<?php

// TODO sécurité, vérifications, ...
$b = ($_GET['myCallbackFunction']);
$c = floatval($_GET['_']);
$name = array_key_exists('name', $_GET) ? $_GET['name'] : '';
$key = array_key_exists('key', $_GET) ? $_GET['key'] : '';
$score =  array_key_exists('score', $_GET) ? $_GET['score'] : '';
$temps = array_key_exists('temps', $_GET) ? $_GET['temps'] : '';
$classe = array_key_exists('classe', $_GET) ? $_GET['classe'] : '';
$comment = array_key_exists('comment', $_GET) ? $_GET['comment'] : '';
$urlproxy = $_GET['url'];
$nblig= (isset($_GET['nblig'])?'nblig='.$_GET['nblig']:'');
$day= (isset($_GET['day'])?'day':'');
$days= (isset($_GET['days'])?'days='.$_GET['days']:'');
$qu_id = (isset($_GET['qu_id'])?'qu_id='.$_GET['qu_id']:'');
$plato_show_form_result = (isset($_GET['plato_show_form_result'])?'plato_show_form_result='.$_GET['plato_show_form_result']:'');
$mode= (isset($_GET['mode'])?'mode='.$_GET['mode']:'');
$timeId= (isset($_GET['timeId'])?'timeId='.$_GET['timeId']:'');
$questionnaireId= (isset($_GET['questionnaireId'])?'questionnaireId='.$_GET['questionnaireId']:'');
$userId= (isset($_GET['userId'])?'userId='.$_GET['userId']:'');

// URL vers laquelle émettre la "vraie" requête :
//$url = 'https://script.google.com/macros/s/AKfycbyD05-rdvxbsXZG8MH7potWHFgW4Y45aCPBA3o5FEfJRnK9kwU/exec?'
 $url = $urlproxy
       . '?'
       . '&myCallbackFunction=' . urlencode($b)
	   . '&_=' . urlencode($c);
   

if (!empty($timeId)) {   
 $url .= '&' . ($timeId);
}
if (!empty($questionnaireId)) {   
 $url .= '&' . ($questionnaireId);
} 
if (!empty($userId)) {   
 $url .= '&' . ($userId);
} 
if (!empty($mode)) {   
 $url .= '&' . ($mode);
} 
if (!empty($name)) {   
 $url .= '&name=' . urlencode($name);
}
if (!empty($key)) {   
 $url .= '&key=' . urlencode($key);
} 
if (!empty($score)) {   
 $url .= '&score=' . urlencode($score);
}
if (!empty($temps)) {   
  $url .= '&temps=' . urlencode($temps);
}
if (!empty($classe)) {   
  $url .= '&classe=' . urlencode($classe);
}
if (!empty($comment)) {   
 $url .= '&comment=' . urlencode($comment);
}
if (!empty($day)) {   
 $url .= '&' . ($day);
}
if (!empty($nblig)) {   
 $url .= '&' . ($nblig);
}
if (!empty($days)) {   
 $url .= '&' . ($days);
}
if (!empty($qu_id)) {   
 $url .= '&' . ($qu_id);
}
if (!empty($plato_show_form_result)) {   
 $url .= '&' . ($plato_show_form_result);
}
// echo $url;
// Requête HTTP vers le "vrai" serveur
$ch = curl_init($url);
//curl_setopt($ch, CURLOPT_HEADER, true);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_CRLF, true);
curl_setopt($ch, CURLOPT_TIMEOUT, 10);
curl_setopt($ch, CURLOPT_HEADER      ,0);  // DO NOT RETURN HTTP HEADERS
$retourDistant = curl_exec($ch);

if ($retourDistant === FALSE) {
   die(curl_error($ch));
}

curl_close($ch);
//curl_setopt($ch, CURLOPT_POST, true);
//curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
echo $retourDistant;
//echo $url

// Récupération du résultat (en-tête X-JSON)
// $headers = explode("\r\n", $retourDistant);
// foreach ($headers as $header) {
    // $matches = array();
    // if (preg_match('/^X-JSON: (.*)$/', $header, $matches)) {
        // // Et sortie, telle qu'attendue par prototype.js
        // header('X-JSON: ' . $matches[1]);
        // die;
    // }
// }
 ?>