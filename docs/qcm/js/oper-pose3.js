   
function afficheroptions() {
    if ($('#zonparam').css("display") == 'block') {
        $('#zonparam').slideUp('slow', function() {
            //actualiserobjets();
            $('#zonparam').css('display', 'none')
        });
        $('#imgoptions').attr('src', 'images/icoplus.gif')
    } else {
        $('#zonparam').slideDown('slow', function() {
            //actualiserobjets();
            $('#zonparam').css('display', 'block')
        });
        $('#imgoptions').attr('src', 'images/icomoins.gif')
    }
}

function verifestchiffre(nb) {
    nb = parseFloat(nb.replace(',', '.'));
    if (isNaN(nb))
        return (-1);
    else
        return (nb)
}
function validerparametresope(lien, nompage, rubno, rubajaxpage, ope) {
    var existe = document.getElementById('zonparam') || 0;
    if (existe != 0) {
        ok = 1;
        strerreur = "";
        str = "";
        nb1 = $('#txtnb1').val();
        nb2 = $('#txtnb2').val();
        if (!(nb1 == "" && nb2 == "")) {
            if (nb2 == "") {
                strerreur = strerreur + "Il faut saisir le deuxième nombre.";
                ok = 0
            }
            if (nb1 == "") {
                strerreur = strerreur + "Il faut saisir le premier nombre.";
                ok = 0
            }
            if (ok == 1) {
                nb1 = verifestchiffre(nb1);
                if (nb1 == -1) {
                    strerreur = strerreur + "Le premier nombre contient des caractères incorrects.";
                    ok = 0
                }
                nb2 = verifestchiffre(nb2);
                if (nb2 == -1) {
                    strerreur = strerreur + "Le deuxième nombre contient des caractères incorrects.";
                    ok = 0
                }
            }
            if (ok == 1) {
                strnb1 = nb1.toString();
                strnb2 = nb2.toString();
                if (strnb1.length > 9) {
                    strerreur = strerreur + "Le premier nombre est trop grand.";
                    ok = 0
                }
                if (nb1 == 0 || nb2 == 0) {
                    strerreur = strerreur + "Les nombres ne peuvent valoir 0.";
                    ok = 0
                }
                if ((ope == 2) && nb2 > nb1) {
                    strerreur = strerreur + "Le deuxième nombre ne peut être supérieur au premier.";
                    ok = 0
                }
                if (ope == 3) {
                    if (strnb1.length + strnb2.length > 14) {
                        strerreur = strerreur + "Les nombres sont trop grands.";
                        ok = 0
                    }
                }
            }
            if (ok == 1) {
                str = str + "&nb1=" + strnb1;
                str = str + "&nb2=" + strnb2
            }
        }
        if (ope == 1) {
            nb3 = $('#txtnb3').val();
            if (nb3 != "") {
                nb3 = verifestchiffre(nb3);
                strnb3 = nb3.toString();
                if (nb3 == -1) {
                    strerreur = strerreur + "Le troisième nombre contient des caractères incorrects.";
                    ok = 0
                }
                if (strnb3.length > 9) {
                    strerreur = strerreur + "Le troisième nombre est trop grand.";
                    ok = 0
                }
                if (ok == 1)
                    str = str + "&nb3=" + strnb3
            }
        }
        nbchiffres = verifestchiffre($('#txtnbchiffres').val());
        if (nbchiffres == -1) {
            strerreur = strerreur + "Le nombre de chiffres contient des caractères incorrects.";
            ok = 0
        } else {
            if (nbchiffres > 9 || nbchiffres < 1) {
                strerreur = strerreur + "Le nombre de chiffres doit être compris entre 1 et 9.";
                ok = 0
            } else {
                str = str + "&nbchiffres=" + nbchiffres
            }
        }
        if (ope == 3 || ope == 4) {
            nbchiffres2 = verifestchiffre($('#txtnbchiffres2').val());
            if (nbchiffres2 == -1) {
                strerreur = strerreur + "Le nombre de chiffres du deuxième nombre contient des caractères incorrects.";
                ok = 0
            } else {
                if (nbchiffres2 <= 0) {
                    strerreur = strerreur + "Le nombre de chiffres du deuxième nombre ne peut être égal à 0.";
                    ok = 0
                } else {
                    if (ope == 3 && (nbchiffres2 + nbchiffres > 14)) {
                        strerreur = strerreur + "Le nombre de chiffres total est trop grand.";
                        ok = 0
                    } else {
                        str = str + "&nbchiffres2=" + nbchiffres2
                    }
                }
            }
        }
        if (ope == 3) {
            if ($('#txttables').val() != "") {
                tables = verifestchiffre($('#txttables').val());
                if (tables == -1) {
                    strerreur = strerreur + "Le nombre des tables contient des caractères incorrects.";
                    ok = 0
                } else {
                    if (tables < 2) {
                        tables = 2
                    }
                    str = str + "&tables=" + tables
                }
            }
        }
        if (ok == 1) {
            if (ope == 4) {
                if (document.getElementById('chksoustraction').checked)
                    str = str + "&affichersoustraction=0";
                if (document.getElementById('chkassistance').checked)
                    str = str + "&assistance=0";
                str = str + "&nbresvirgule=" + $("#txtnbresvirgule").val()
            }
			var chkvirgule = false;
            if (ope == 3) {
                if (document.getElementById('chkretenuedessus').checked)
                    str = str + "&retenuedessus=1"
				if (document.getElementById('chkvirgule').checked)
                    chkvirgule = true;
            }
            if (document.getElementById("zonparam").style.display != "none")
                str = str + "&zonparam=2";
            else
                str = str + "&zonparam=1";
            if (document.getElementById("txtexono"))
                str = str + "&exono=" + document.getElementById("txtexono").value;
            strajax = nompage + "?rubno=" + rubno + str;
            strlien = lien + str;
			document.getElementById("zoninfosaisie").innerHTML ='';
			if (ope == 1) {
				updateExercice(nb1, nb2, 'centre', nbchiffres);
			}else if  (ope == 2) {
				updateExercice2(nb1, nb2, 'centre', nbchiffres);
			}else if  (ope == 3) {
				updateExercice3(nb1, nb2,'centre', nbchiffres, nbchiffres2 , 9,chkvirgule);
			}else if  (ope == 4) {
				updateExercice4(nb1, nb2, 'centre', nbchiffres,nbchiffres2,$("#txtnbresvirgule").val());
			}
        } else
            document.getElementById("zoninfosaisie").innerHTML = strerreur;
    }
}

function getlength(number) {
    return number.toString().length;
}
function removeTrailingZeros(value) {
       value = value.toString();

    // if not containing a dot, we do not need to do anything
    if (value.indexOf('.') > -1 || value.indexOf(',') > -1) {
        // as long as the last character is a 0 or a dot, remove it
		while(value.slice(-1) === '0') {			
			value = value.substr(0, value.length - 1);
		}
    }
	if (value.slice(-1) === '.' || value.slice(-1) === ','){
		value = value.substr(0, value.length - 1);
	}
	
	
	while(value.slice(0,1) === '0' && !((value.slice(1,2) === ',') ||(value.slice(1,2) === '.') )) {
		value = value.slice(1);
	}
    
    return value;
}

function changeInput(this_el,k,retenu,sol){
	if(document.getElementById('elefalc_1_'+k+'_2')){
		$('#elefalc_1_'+k+'_2').val($('#eleb_1_'+k+'_1').val());
	}else {
		$('#elefalc_1_'+retenu+'_2').val($('#eleb_1_'+k+'_1').val());
	}
}

function divisionEucl(nb,d){
	nb = parseInt(nb);
	d = parseInt(d);
	var resu =[];
	resu[0] = Math.floor((nb-nb%d)/d);
	resu[1] = nb%d;
	return resu;
}

function division(nb1str,nb2str,nbchfvirg){
	var div={};
	div.ele={};
	div.addele={};
	div.eles={};
	div.sol={};
	div.digitadd=0;
	div.virgule = -1;
	
	var pos = 0;
	var fini = false;
	var nb1strT = removeTrailingZeros(nb1str);
	var i=0;
	while (!fini && i<20){
		var k=1;
		var found= false;
		while(!found && k<10){
			var resu = divisionEucl(nb1strT.slice(0,0+k),nb2str);
			if (resu[0]>0 || nb1strT.length==k || nb1strT.charAt(0+k)==','){
				found = true;
				div.sol[i] = resu[0].toString();
				div.ele[i] = resu[1].toLocaleString(undefined, { minimumIntegerDigits: k });
				div.eles[i] = (resu[0] *  parseInt(nb2str)).toLocaleString(undefined, { minimumIntegerDigits: k });
				var addv = nb1str.slice(pos+k,pos+k+1);
				if (addv==="" && (parseInt(div.ele[i])===0)){
					fini = true;
				}else if(addv==="" && ((div.digitadd + nb1str.length-nb1str.indexOf(',')-1) <nbchfvirg)){
					addv = '0';
					div.digitadd++;
				}else if(addv==="" && ((div.digitadd + nb1str.length-nb1str.indexOf(',')-1) >=nbchfvirg)){
					fini = true;
				}
				nb1strT = removeTrailingZeros(div.ele[i]) + addv;				
				if (nb1strT.slice(-1)===','){
					nb1strT = removeTrailingZeros(div.ele[i]) + nb1str.slice(pos+k,pos+k+2);
					pos+=1;
					nb1strT= nb1strT.replace(',','');
					div.virgule = i;
				}
				div.addele[i] = nb1strT.slice(-1);
				pos+=k-(nb1strT.length-1);
				
			}
			k++;				
		}
		i++;
	}
	return div;
}

function  updateExercice4(nb1, nb2, html_el, nbchiffres1,nbchiffres2,nbchfvirg){
	$('#zoncorrige').show();
	$("#zonsolution0").css("visibility", "hidden");
	$("#zonsolution0").hide();	
	$("#zoneclavierexercice").show();
	$('#spancorriger').html('Corriger');
	$("#btncorriger").show();
	
	var virgule = -1;
	if (nb1 === undefined || nb1===null || nb1.length===0){
		var i=0;
		var tmp = 0;
		var nbdigits1 = nbchiffres1;
		while(i<nbdigits1){
			tmp *=10;	
			tmp += Math.floor(Math.random()*9)+1;
			i++;
		}
		nb1 = tmp;
		virgule = Math.floor(Math.random()*(nbchiffres1-1))+1;		
	}
	
	if (nb2 === undefined || nb2===null || nb2.length===0){
		var i=0;
		var tmp = 0;
		while(i<nbchiffres2){
			tmp *=10;	
			tmp += Math.floor(Math.random()*8)+2;			
			i++;
		}
		nb2 = tmp;
	}

	var nb1str;
	if (nb1.toString().indexOf('.')>0){
		nb1str = nb1.toString().replace('.',',');
	}else if (virgule>-1){
		nb1str = nb1.toString().substr(0, nb1.toString().length - virgule) +','+nb1.toString().substr(nb1.toString().length - virgule); 
	}else{
		nb1str = nb1.toString();
	}
	var nb2str;
	if (nb2.toString().indexOf('.')>0){
		nb2str = nb2.toString().replace('.',',');
	}else{
		nb2str = nb2.toString(); 
	}
	if (nb1str[0]===','){
		nb1str = '0' + nb1str;
	}
	if (nb2str[0]===','){
		nb2str = '0' + nb2str;
	}
		
	var div = division(nb1str,nb2str,nbchfvirg);

	var tblsolution=new Array();
	var temp ='';
	i=0;
	while (i<nb1str.length+div.digitadd){
		if (i+1 === nb1str.length+div.digitadd){
			if (nb1str.charAt(i)===''){
				temp+='0';
			}else{
				temp += nb1str.charAt(i);
			}
		}else if (nb1str.charAt(i)===''){
			temp +="0_";
		}else if (nb1str.slice(i,i+1)!==','){
			temp += nb1str.slice(i,i+1) + (nb1str.slice(i+1,i+2)===','?'':'_');
		}else{
			temp += nb1str.slice(i,i+1);
		}
		i++;
	}
	for( i=0;i<temp.length;i++){
		tblsolution['elefalc.1.'+ (i+1)] = (temp.charAt(i)==='_'?"":temp.charAt(i));
	}
	var nb1strWithoutComma = nb1str.replace(',','');
	var index = (nb1strWithoutComma.length+div.digitadd)*2;
	
	for (i=0;i< Object.keys(div.ele).length;i++){
		for (var p=0;p<div.eles[i].length;p++){
			tblsolution['ele_1_'+ (index)] = (div.eles[i].charAt(p));	
			index++;			
		}
		if (i==0){
			for (var pro in div.sol) {
				tblsolution['ele_1_'+ (index)] = (div.sol[pro]);
				index++;
			}
			for (var p=0;p<Object.keys(div.sol).length-1;p++) {
				if (div.virgule===p){
					tblsolution['ele_1_'+ (p+200)] = ",";
				}else{
					tblsolution['ele_1_'+ (p+200)] = "";
				}
			}
						
		}
		for (var p=0;p<div.ele[i].length;p++){
			tblsolution['ele_1_'+ (index)] = (div.ele[i].charAt(p));
			index++;			
		}
		if (i< Object.keys(div.ele).length-1){
			tblsolution['ele_1_'+ (index)] = div.addele[i];
			index++;
		}
		
	}
	
	$('#zonprincipal > span.mbleu > nobr').html(' '+removeTrailingZeros(nb1str)+' : '+removeTrailingZeros(nb2str))
	$('#zonprincipal > div:nth-child(5) > table > tbody').html('');
	$('#zoncorrige > table > tbody').html('');
	$("#btnrecommencer").attr("onclick","document.getElementById('txtnb1').value='"+removeTrailingZeros(nb1str)+"';document.getElementById('txtnb2').value='"+removeTrailingZeros(nb2str)+"'; validerparametresope('exercice-mathematiques-calculs-poser-operation-multiplication.html','modele04_multiplication.php', '2543', '21','1')");
	
	var readonly='';
	alert(navigator.userAgent);
	if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)  || (navigator.platform === 'MacIntel' && navigator.maxTouchPoints > 1)) {				
		readonly=' readonly="readonly" ';
	}
	alert(readonly);
	var str = '<tr><td>&nbsp;</td>';
	var str2 = str;
	var i=0;
	while (i<temp.length){		
		if (i==0){			
			str+= '<td align="Center"><input type="tel" name="txtelefalc[]" '+readonly+'id="elefalc.1.'+(i+1)+'" value="'+temp.charAt(i)+'" class="saisiechiffrenormal" style="text-align:Center;margin:1px;" maxlength="1" onfocus="factivation(this.id);saisieinit(this.id);" onkeydown="testeffacer(event.keyCode,this)" onkeyup="testtouchepasserfocus(event.keyCode,this.id,&quot;&quot;,this);"></td>';			
			str2+= '<td align="Center"><input type="tel" name="txtsolfalc[]" id="solfalc.1.'+(i+1)+'" value="'+temp.charAt(i)+'" class="saisiechiffrenormal" style="text-align:Center;margin:1px;" maxlength="1" onfocus="factivation(this.id);saisieinit(this.id);" onkeydown="testeffacer(event.keyCode,this)" onkeyup="testtouchepasserfocus(event.keyCode,this.id,&quot;&quot;,this);"></td>';			
		}else{
			str+= '<td align="Center"><div style="position:relative"><div style="position:absolute;top:17px;left:-10px;">';
			str+='<input type="tel" name="txtelefalc[]" '+readonly+'id="elefalc.1.'+(i+1)+'" class="saisieretenuefalc" value="'+(temp.charAt(i)===','?',':'')+'" style="text-align:Center;margin:1px;font-size: 18px;color:black;    width: 11px!important;height: 24px!important;padding-top: 0px;font-family: Verdana,Arial; font-weight: bold;" maxlength="1" onfocus="factivation(this.id);saisieinit(this.id);" onkeydown="testeffacer(event.keyCode,this)" onkeyup="testtouchepasserfocus(event.keyCode,this.id,&quot;&quot;,this);">';
			str+='</div></div>';
			str+='<input type="tel" name="txtelefalc[]" '+readonly+'id="elefalc.1.'+(i+2)+'" value="'+temp.charAt(i+1)+'" class="saisiechiffrenormal" style="text-align:Center;margin:1px;" maxlength="1" onfocus="factivation(this.id);saisieinit(this.id);" onkeydown="testeffacer(event.keyCode,this)" onkeyup="testtouchepasserfocus(event.keyCode,this.id,&quot;&quot;,this);">';
			str+='</td>';
			
			str2+= '<td align="Center"><div style="position:relative"><div style="position:absolute;top:17px;left:-10px;">';
			str2+='<input type="tel" name="txtsolfalc[]" id="solfalc.1.'+(i+1)+'" class="saisieretenuefalc" value="'+(temp.charAt(i)===','?',':'')+'" style="text-align:Center;margin:1px;font-size: 18px;color:black;    width: 11px!important;height: 24px!important;padding-top: 0px;font-family: Verdana,Arial; font-weight: bold;" maxlength="1" onfocus="factivation(this.id);saisieinit(this.id);" onkeydown="testeffacer(event.keyCode,this)" onkeyup="testtouchepasserfocus(event.keyCode,this.id,&quot;&quot;,this);">';
			str2+='</div></div>';
			str2+='<input type="tel" name="txtsolfalc[]" id="solfalc.1.'+(i+2)+'" value="'+temp.charAt(i+1)+'" class="saisiechiffrenormal" style="text-align:Center;margin:1px;" maxlength="1" onfocus="factivation(this.id);saisieinit(this.id);" onkeydown="testeffacer(event.keyCode,this)" onkeyup="testtouchepasserfocus(event.keyCode,this.id,&quot;&quot;,this);">';
			str2+='</td>';
			i++;
		}
		i++;
	}
	str+='<td class="traitopevertic" rowspan="'+(Object.keys(div.eles).length*3+2)+'">&nbsp;</td>';
	str2+='<td class="traitopevertic" rowspan="'+(Object.keys(div.eles).length*3+2)+'">&nbsp;</td>';
	i=0;
	while (i<Object.keys(div.sol).length){
		if (i<nb2str.length){
			str+= '<td align="Center" class="casechiffre">'+nb2str.charAt(i)+'</td>';
			str2+= '<td align="Center" class="casechiffre">'+nb2str.charAt(i)+'</td>';
		}else{
			str+= '<td></td>';
			str2+= '<td></td>';
		}	
		i++;
	}
	str+='</tr>';
	str2+='</tr>';
	$('#zonprincipal > div:nth-child(5) > table > tbody').append(str);
	$('#zoncorrige > table > tbody').append(str2);
	
	
	str='<tr><td colspan="'+(1+nb1strWithoutComma.length+div.digitadd)+'"></td>';
	str+='<td class="traitopehorizbold" colspan="'+Object.keys(div.sol).length+'">&nbsp;</td></tr>';
	$('#zonprincipal > div:nth-child(5) > table > tbody').append(str);
	$('#zoncorrige > table > tbody').append(str);
	
	var col = 0;
	index = (nb1strWithoutComma.length+div.digitadd)*2;
	for (i=0;i< Object.keys(div.ele).length;i++){
		str='<tr><td>-</td>';
		str2='<tr><td>-</td>';
		for (var p=0;p<col;p++){
			str+='<td></td>';
			str2+='<td></td>';
		}
		for (var p=0;p<div.eles[i].length;p++){
		    var tabindex= (9-col-p)+10+(i*40);
			str+='<td align="Center"><input type="tel" name="txtele[]"'+readonly+' id="ele_1_'+index+'" class="saisiechiffrenormal" style="text-align:Center;" maxlength="1" onfocus="factivation(this.id);saisieinit(this.id)" tabindex="'+tabindex+'" onkeydown="testeffacer(event.keyCode,this)" onkeyup="testtouchepasserfocus(event.keyCode,this.id,&quot;&quot;,this)"></td>';
			str2+='<td align="Center"><input type="tel" name="txtsol[]" id="sol_1_'+index+'" class="saisiechiffrenormal" style="text-align:Center;" maxlength="1" onfocus="factivation(this.id);saisieinit(this.id)" tabindex="'+tabindex+'" onkeydown="testeffacer(event.keyCode,this)" onkeyup="testtouchepasserfocus(event.keyCode,this.id,&quot;&quot;,this)"></td>';
			index++;			
		}
		for (var p=0;p<nb1strWithoutComma.length+div.digitadd-div.eles[i].length-col;p++){
			str+='<td></td>';
			str2+='<td></td>';
		}

		
		if (i===0){
			for (var p=0;p<Object.keys(div.sol).length;p++) {
				var tabindex= 1+(p*40);
				if (p==0){			
					str+= '<td align="Center"><input type="tel" name="txtele[]" '+readonly+'id="ele_1_'+index+'" class="saisiechiffrenormal" style="text-align: center; background-color: white;" maxlength="1" onfocus="factivation(this.id);saisieinit(this.id)" tabindex="'+tabindex+'" onkeydown="testeffacer(event.keyCode,this)" onkeyup="testtouchepasserfocus(event.keyCode,this.id,&quot;&quot;,this)"></td>';								
					str2+= '<td align="Center"><input type="tel" name="txtsol[]" id="sol_1_'+index+'" class="saisiechiffrenormal" style="text-align: center; background-color: white;" maxlength="1" onfocus="factivation(this.id);saisieinit(this.id)" tabindex="'+tabindex+'" onkeydown="testeffacer(event.keyCode,this)" onkeyup="testtouchepasserfocus(event.keyCode,this.id,&quot;&quot;,this)"></td>';								
				}else{
					str+='<td align="Center"><div style="position:relative"><div style="position:absolute;top:17px;left:-10px;">';
					str+='<input type="tel" name="txtele[]" '+readonly+'id="ele_1_'+(p+200-1)+'" class="saisieretenuefalc" style="text-align:Center;margin:1px;font-size: 18px;color:black;    width: 11px!important;height: 24px!important;padding-top: 0px;font-family: Verdana,Arial; font-weight: bold;" maxlength="1" onfocus="factivation(this.id);saisieinit(this.id);" onkeydown="testeffacer(event.keyCode,this)" onkeyup="testtouchepasserfocus(event.keyCode,this.id,&quot;&quot;,this);">';
					str+='</div></div>';
					str+='<input type="tel" name="txtele[]" '+readonly+'id="ele_1_'+index+'" class="saisiechiffrenormal" style="text-align:Center" maxlength="1" onfocus="factivation(this.id);saisieinit(this.id)" tabindex="'+tabindex+'" onkeydown="testeffacer(event.keyCode,this)" onkeyup="testtouchepasserfocus(event.keyCode,this.id,&quot;&quot;,this)">';
					str+='</td>';
					
					str2+='<td align="Center"><div style="position:relative"><div style="position:absolute;top:17px;left:-10px;">';
					str2+='<input type="tel" name="txtsol[]" id="sol_1_'+(p+200-1)+'" class="saisieretenuefalc" style="text-align:Center;margin:1px;font-size: 18px;color:black;    width: 11px!important;height: 24px!important;padding-top: 0px;font-family: Verdana,Arial; font-weight: bold;" maxlength="1" onfocus="factivation(this.id);saisieinit(this.id);" onkeydown="testeffacer(event.keyCode,this)" onkeyup="testtouchepasserfocus(event.keyCode,this.id,&quot;&quot;,this);">';
					str2+='</div></div>';
					str2+='<input type="tel" name="txtsol[]" id="sol_1_'+index+'" class="saisiechiffrenormal" style="text-align:Center" maxlength="1" onfocus="factivation(this.id);saisieinit(this.id)" tabindex="'+tabindex+'" onkeydown="testeffacer(event.keyCode,this)" onkeyup="testtouchepasserfocus(event.keyCode,this.id,&quot;&quot;,this)">';
					str2+='</td>';
				}
				index++;
			}
		}
		str+='</tr>';
		str2+='</tr>';
		str+='<tr><td class="traitopehorizthin" colspan="'+(1+nb1strWithoutComma.length+div.digitadd)+'">&nbsp;</td><td colspan="'+Object.keys(div.sol).length+'"></td></tr>'
		str2+='<tr><td class="traitopehorizthin" colspan="'+(1+nb1strWithoutComma.length+div.digitadd)+'">&nbsp;</td><td colspan="'+Object.keys(div.sol).length+'"></td></tr>'
		$('#zonprincipal > div:nth-child(5) > table > tbody').append(str);
		$('#zoncorrige > table > tbody').append(str2);
	
		str='<tr><td></td>';
		str2='<tr><td></td>';
		for (var p=0;p<col;p++){
			str+='<td></td>';
			str2+='<td></td>';
		}
		for (var p=0;p<div.ele[i].length;p++){
		    var tabindex= (9-col-p)+30+(i*40);
			str+='<td align="Center"><input type="tel" name="txtele[]" '+readonly+' id="ele_1_'+index+'" class="saisiechiffrenormal" style="text-align:Center;" maxlength="1" onfocus="factivation(this.id);saisieinit(this.id)" tabindex="'+tabindex+'" onkeydown="testeffacer(event.keyCode,this)" onkeyup="testtouchepasserfocus(event.keyCode,this.id,&quot;&quot;,this)"></td>';		
			str2+='<td align="Center"><input type="tel" name="txtsol[]" id="sol_1_'+index+'" class="saisiechiffrenormal" style="text-align:Center;" maxlength="1" onfocus="factivation(this.id);saisieinit(this.id)" tabindex="'+tabindex+'" onkeydown="testeffacer(event.keyCode,this)" onkeyup="testtouchepasserfocus(event.keyCode,this.id,&quot;&quot;,this)"></td>';		
			index++;			
		}
		if (i+1<Object.keys(div.ele).length){
			var tabindex= (i+1)*40;
			str+='<td align="Center"><input type="tel" name="txtele[]" '+readonly+' id="ele_1_'+index+'" class="saisiechiffrenormal" style="text-align:Center;" maxlength="1" onfocus="factivation(this.id);saisieinit(this.id)" tabindex="'+tabindex+'" onkeydown="testeffacer(event.keyCode,this)" onkeyup="testtouchepasserfocus(event.keyCode,this.id,&quot;&quot;,this)"></td>';
			str2+='<td align="Center"><input type="tel" name="txtsol[]" id="sol_1_'+index+'" class="saisiechiffrenormal" style="text-align:Center;" maxlength="1" onfocus="factivation(this.id);saisieinit(this.id)" tabindex="'+tabindex+'" onkeydown="testeffacer(event.keyCode,this)" onkeyup="testtouchepasserfocus(event.keyCode,this.id,&quot;&quot;,this)"></td>';
			index++;
		}
		
		for (var p=0;p<nb1strWithoutComma.length+div.digitadd-div.ele[i].length-col;p++){
			str+='<td></td>';
			str2+='<td></td>';
		}
		
		str+='<td colspan="'+Object.keys(div.sol).length+'"></td>';
		str+='</tr>';
		str2+='<td colspan="'+Object.keys(div.sol).length+'"></td>';
		str2+='</tr>';
		$('#zonprincipal > div:nth-child(5) > table > tbody').append(str);
		$('#zoncorrige > table > tbody').append(str2);
		col =  col + div.ele[i].length -  (i+1<Object.keys(div.ele).length?div.ele[i+1].length:0) + 1;
		console.log('col:'+col);
		
	}		
	window.tblsolution = tblsolution;
}


function  updateExercice3(nb1, nb2, html_el, nbchiffres1, nbchiffres2 , tables,chkvirgule){
	$('#zonsolution0').hide();
	$("#zoneclavierexercice").show();
	$('#spancorriger').html('Corriger');
	$("#btncorriger").show();
	
	var virgule1 = -1;
	if (nb1 === undefined || nb1===null || nb1.length===0){
		var i=0;
		var tmp = 0;
		while(i<nbchiffres1){
			tmp *=10;	
			tmp += Math.floor(Math.random()*9)+1;
			i++;
		}
		nb1 = tmp;
		if (chkvirgule){
			virgule1 = Math.floor(Math.random()*(nbchiffres1-1))+1;
		}else{
			virgule1 = 0;
		}
	}
	
	var virgule2 = -1;
	if (nb2 === undefined || nb2===null || nb2.length===0){
		var i=0;
		var tmp = 0;
		while(i<nbchiffres2){
			tmp *=10;	
			tmp += Math.floor(Math.random()*(tables))+1;			
			i++;
		}
		nb2 = tmp;
		if (chkvirgule){
			virgule2 = Math.floor(Math.random()*(nbchiffres2-1))+1;
		}else{
			virgule2 = 0;
		}
	}
	
	var nb1str;
	if (virgule1>=0){
		nb1str = nb1.toString().substr(0, nb1.toString().length - virgule1) +','+nb1.toString().substr(nb1.toString().length - virgule1); 
	}else {
		nb1str = nb1.toString().replace('.',',');
	}
	if (nb1str[0]===','){
		nb1str = '0' + nb1str;
	}	
	var nb1strWithoutComa = nb1str.replace(',','');
	
	var nb2str;
	if (virgule2>=0){
		nb2str = nb2.toString().substr(0, nb2.toString().length - virgule2) +','+nb2.toString().substr(nb2.toString().length - virgule2); 
	}else {
		nb2str = nb2.toString().replace('.',',');
	}
	if (nb2str[0]===','){
		nb2str = '0' + nb2str;
	}
	var nb2strWithoutComa = nb2str.replace(',','');
	
	$('#zonprincipal > span.mbleu > nobr').html(' '+removeTrailingZeros(nb1str)+'  X '+removeTrailingZeros(nb2str))
	$('div.colgche > table > tbody').html('');
	$('#zonsolution0 > table > tbody').html('');
	$("#btnrecommencer").attr("onclick","document.getElementById('txtnb1').value="+nb1+";document.getElementById('txtnb2').value="+nb1str+"; validerparametresope('','', '2543', '21','3')");
	
	
	var k = nb1str.indexOf(',')>=0?nb1str.indexOf(',')+1:nb1str.length;
	var decimal1 = nb1str.length - k;
	
	k = nb2str.indexOf(',')>=0?nb2str.indexOf(',')+1:nb2str.length;
	var decimal2 = nb2str.length - k;
	
	var resu = parseInt(nb1strWithoutComa)*parseInt(nb2strWithoutComa);
	resu = resu.toString();
	var resustr  = resu.slice(0,resu.length-decimal1-decimal2) +','+resu.slice(resu.length-decimal1-decimal2); 
	if (resustr[0]===','){
		resustr = '0' + resustr;
	}
	
	var resustrWithoutComa = resustr.replace(',','');
	var resudigits = resustrWithoutComa.length;
	
	var nb1digits = getlength(nb1strWithoutComa);	
	var str = '<tr style="height:2em;"><td colspan="'+ (resudigits+1-nb1digits) + '"></td>';
	var i=0;
	while (i<nb1digits){
		str+= '<td align="Center" class="casechiffre">'+nb1strWithoutComa.charAt(i)+'</td>';
		i++;
	}
	str+='</tr>';
	$('div.colgche > table > tbody').append(str);
	$('#zonsolution0 > table > tbody').append(str);
	
	
	/*virgule*/
	var str ='<tr><td height="1px" style="font-size:1px;" colspan="'+ (resudigits+1-nb1digits) + '">&nbsp;</td>';
	i=0;
	while (i<nb1digits){
		if (nb1str.charAt(i)!==','){
			str+='<td height="1px" style="font-size:1px;">&nbsp;</td>';
		}else{
			str+='<td height="1px" class="casechiffre"><div style="position:relative"><div style="position:absolute;top:-38px;left:-5px;">,</div></div></td>'
		}
		i++;
	}
	str+='</tr>';
	$('div.colgche > table > tbody').append(str);
	$('#zonsolution0 > table > tbody').append(str);
	
	var nb2digits = getlength(nb2strWithoutComa);
	str='<tr style="height:2em;"><td align="Center" class="txtope">x</td>';	
	if (resudigits-nb2digits>0){
		str+='<td colspan="'+(resudigits-nb2digits) +'"></td>';
	}
	i=0;
	while (i<nb2digits){
		str+= '<td align="Center" class="casechiffre">'+nb2strWithoutComa.charAt(i)+'</td>';
		i++;
	}
	str+='<td nowrap="nowrap">';
	
	
	i=nb1digits-1;
	var indN = 50-2*nb1digits+1;
	while (i>0){	    
		str+='<input type="tel" id="retA.'+i+'" class="saisiechiffremini saisiefacultatif" style="text-align: center; color: red; margin: 1px; background-color: rgb(227, 227, 227);" maxlength="1" tabindex="'+indN+'" onfocus="';
			str+=		'for(j='+i+' ;j>=1;j--) {';
			str+=		'		document.getElementById(&quot;retA.&quot;+j).value=&quot;&quot;;';
			str+=		'		document.getElementById(&quot;retA.&quot;+j).style.backgroundColor=&quot;#E3E3E3&quot;;';
			str+=		'}';
			str+=		'k='+i+'+1;';
			str+=		'if(document.getElementById(&quot;retA.&quot;+k))';
			str+=		'	document.getElementById(&quot;retA.&quot;+k).style.backgroundColor=&quot;grey&quot;;';
			str+=		'factivation(this.id);';
			str+=		' " onkeydown="testeffacer(event.keyCode,this)" onkeyup="testtouchepasserfocus(event.keyCode,this.id,&quot;&quot;,this)">';
		i--;
		indN=indN+2;
	}
						
	str+='</td></tr>';
	
	$('div.colgche > table > tbody').append(str);
	$('#zonsolution0 > table > tbody').append(str);
	
	/*virgule*/
	var str ='<tr><td height="1px" style="font-size:1px;" colspan="'+ (resudigits+1-nb2digits) + '">&nbsp;</td>';
	i=0;
	while (i<nb2digits){
		if (nb2str.charAt(i)!==','){
			str+='<td height="1px" style="font-size:1px;">&nbsp;</td>';
		}else{
			str+='<td height="1px" class="casechiffre"><div style="position:relative"><div style="position:absolute;top:-38px;left:-5px;">,</div></div></td>'
		}
		i++;
	}
	str+='</tr>';
	$('div.colgche > table > tbody').append(str);
	$('#zonsolution0 > table > tbody').append(str);
	
	
	str='';
	str+='<tr><td class="traitopehorizbold" colspan="'+(resudigits+1)+'">&nbsp;</td></tr>';
	$('div.colgche > table > tbody').append(str);
	$('#zonsolution0 > table > tbody').append(str);
	
	str='';
	str+='<tr align="center" valign="bottom" style="font-size:0.5em;"><td width="30px">&nbsp;</td>';
	var str2 = str;
	var j=0;ind=1;
	var indN =(nb2digits+1)*50+28;
	while(j<resudigits-1){
		str+='<td align="Center"><input type="tel" name="txtelefalc[]" id="elefalc_'+ind+'" class="saisieretenuefalc saisieretenuefalcsoustr1" style="	 text-align:Center" maxlength="1" tabindex="'+indN+'" onfocus="factivation(this.id);" onkeydown="testeffacer(event.keyCode,this)" onkeyup="testtouchepasserfocus(event.keyCode,this.id,&quot;&quot;,this)"></td>';
		str2+='<td align="Center"><input type="tel" name="txtsolfalc[]" id="solfalc_'+ind+'" class="saisieretenuefalc saisieretenuefalcsoustr1" style="	 text-align:Center" maxlength="1" tabindex="'+indN+'" onfocus="factivation(this.id);" onkeydown="testeffacer(event.keyCode,this)" onkeyup="testtouchepasserfocus(event.keyCode,this.id,&quot;&quot;,this)" readonly=""></td>';
		indN=indN-2;
		ind++;
		j++;
	}
	str+='<td></td></tr>';
	str2+='<td></td></tr>';
	$('div.colgche > table > tbody').append(str);
	$('#zonsolution0 > table > tbody').append(str2);
	
	
	var j=0;ind=1;
	while(j<nb2digits){
		var resuT = parseInt(nb1strWithoutComa)*parseInt(nb2strWithoutComa.charAt(nb2digits-1-j));		
		i=0;
		while(i<j){
			resuT = resuT *10;
			i++;
		}
		var resuTdigits = Math.max(getlength(resuT),(nb1digits+i));
		str='<tr>';
		str+=(j==0&& j+1!=nb2digits)?'<td align="Center">&nbsp;</td>':'<td align="Center">+</td>';
		str+=resudigits-resuTdigits>0?'<td colspan="'+(resudigits-resuTdigits)+'"></td>':'';
		str2=str;
		i=0;
		var indN = (resuTdigits>(nb1digits+j)?(j+1)*50: (j+1)*50-2);
		while (i<resuTdigits){
			str+='<td align="Center"><input type="tel" name="txtele[]" id="ele_1_'+ind+'" class="saisiechiffrenormal" style="text-align: center; margin: 2px; background-color: white;" maxlength="1" onfocus="factivation(this.id);saisieinit(this.id);majtabindex(this);" tabindex="'+indN+'" onkeydown="testeffacer(event.keyCode,this)" onkeyup="testtouchepasserfocus(event.keyCode,this.id,&quot;&quot;,this)"></td>';
			str2+='<td align="Center"><input type="tel" name="txtsol[]" id="sol_1_'+ind+'" class="saisiechiffrenormal" style="text-align: center; margin: 2px; color: rgb(51, 153, 0);" maxlength="1" onfocus="factivation(this.id);saisieinit(this.id);majtabindex(this);" tabindex="'+indN+'" onkeydown="testeffacer(event.keyCode,this)" onkeyup="testtouchepasserfocus(event.keyCode,this.id,&quot;&quot;,this)" readonly=""></td>';
			ind++;
			indN = indN -2;
			i++;
		}
		str+='</tr>';
		str2+='</tr>';
		$('div.colgche > table > tbody').append(str);
		$('#zonsolution0 > table > tbody').append(str2);
		j++;
	}
	
	str='<tr style="height:0.6em;">';
	str+='<td class="traitopehorizbold" colspan="'+(resudigits+1) +'">&nbsp;</td>';
	str+='</tr>';
	$('div.colgche > table > tbody').append(str);
	$('#zonsolution0 > table > tbody').append(str);
	j++;
	
	str='<tr>';
	str+='<td align="Center" class="txtope">=</td>';
	str2=str;
	var indN =(nb2digits+1)*50+29;
	i=0;
	while(i<resudigits){
		str+='<td align="Center"><input type="tel" name="txtele[]" id="ele_1_'+ind+'" class="saisiechiffrenormal" style="text-align: center; margin: 1px; background-color: white;" maxlength="1" onfocus="factivation(this.id);saisieinit(this.id);" tabindex="'+indN+'" onkeydown="testeffacer(event.keyCode,this)" onkeyup="testtouchepasserfocus(event.keyCode,this.id,&quot;&quot;,this)"></td>';
		str2+='<td align="Center"><input type="tel" name="txtsol[]" id="sol_1_'+ind+'" class="saisiechiffrenormal" style="text-align: center; margin: 1px; color: rgb(51, 153, 0);" maxlength="1" onfocus="factivation(this.id);saisieinit(this.id);" tabindex="'+indN+'" onkeydown="testeffacer(event.keyCode,this)" onkeyup="testtouchepasserfocus(event.keyCode,this.id,&quot;&quot;,this)" readonly=""></td>';
		i++;
		ind++;
		indN = indN -2;
	}
	str+='</tr>';
	str2+='</tr>';
	$('div.colgche > table > tbody').append(str);
	$('#zonsolution0 > table > tbody').append(str2);
	
	/*virgule resu*/
	if (decimal1+decimal2>0){
		str='<tr><td height="1px" style="font-size:1px;" colspan="2">&nbsp;</td>';
		str2=str;
		indN = 200;
		i=0;
		while(i<resudigits-1){
			str+='<td height="1px" class="casechiffre"><div style="position:relative"><div style="position:absolute;top:-20px;left:-8px;">';
			str2+='<td height="1px" class="casechiffre"><div style="position:relative"><div style="position:absolute;top:-20px;left:-8px;">';
			str+='<input type="tel" name="txtele[]" id="ele_1_'+ind+'" class="saisieretenuefalc" style="text-align: center; margin: 1px; font-size: 18px; color: black; padding-top: 0px; font-family: Verdana, Arial; font-weight: bold; width: 11px !important; height: 24px !important; background-color: white;" maxlength="1" onfocus="factivation(this.id);saisieinit(this.id);" tabindex="'+indN+'"  onkeydown="testeffacer(event.keyCode,this)" onkeyup="testtouchepasserfocus(event.keyCode,this.id,&quot;&quot;,this)"></div></div></td>';
			str2+='<input type="tel" name="txtsol[]" id="sol_1_'+ind+'" class="saisieretenuefalc" style="text-align: center; margin: 1px; font-size: 18px; color: black; padding-top: 0px; font-family: Verdana, Arial; font-weight: bold; width: 11px !important; height: 24px !important; background-color: white;" maxlength="1" onfocus="factivation(this.id);saisieinit(this.id);" tabindex="'+indN+'"  onkeydown="testeffacer(event.keyCode,this)" onkeyup="testtouchepasserfocus(event.keyCode,this.id,&quot;&quot;,this)"></div></div></td>';
			ind++;
			indN++;
			i++;
		}
		str+='</tr>';
		str2+='</tr>';
		$('div.colgche > table > tbody').append(str);
		$('#zonsolution0 > table > tbody').append(str2);
	}
	
	var tblsolution=new Array();
	tblsolution['elefalc_1']='1';tblsolution['elefalc_2']='';tblsolution['elefalc_3']='';tblsolution['elefalc_4']='';
	j=0;ind=1;
	while(j<nb2digits){
		var resuT = parseInt(nb1strWithoutComa)*parseInt(nb2strWithoutComa.charAt(nb2digits-1-j));
		i=0;
		while(i<j){
			resuT = resuT *10;
			i++;
		}
		var resuTdigits = Math.max(getlength(resuT),(nb1digits+i));
		k=0;
		var len = resuT.toString().length;
		while(k<resuTdigits){
			if (getlength(resuT)>=nb1digits+i){
				tblsolution['ele_1_'+ind+'']=resuT.toString().charAt(k);
			}else{
				tblsolution['ele_1_'+ind+'']='0';
			}		
			ind++;
			k++;		
		}		
		tblsolution[j] = resuTdigits;
		tblsolution['nbcol']=Math.max(resuTdigits,j>0?tblsolution[j-1]:0);
		j++;
	}
	tblsolution['nblig']=nb2digits;
	k=0;
	while(k<resustrWithoutComa.length){
		tblsolution['ele_1_'+ind+'']=resustrWithoutComa.charAt(k);
		ind++;
		k++;
	}
	
	k=0;
	while(k<resuTdigits-1){
		if (k===resuTdigits-decimal1-decimal2-1){
			tblsolution['ele_1_'+ind+'']=',';
		}else{
			tblsolution['ele_1_'+ind+'']='';
		}		
		ind++;
		k++;		
	}
		
	j=0;	
	var retenu =0;
	while(j<tblsolution['nbcol']){
		i=0;
		var tmp=retenu;
		ind=0;		
		while(i<tblsolution['nblig']){
			ind += tblsolution[i];
			var res=0;
			if (i>0 && (ind-j)<=ind-tblsolution[i]){
				res=0;
			}else{
				res = parseInt(tblsolution['ele_1_'+ (ind-j)]);
			}			
			tmp += (isNaN(res)?0:res);
			i++;
		}
		console.log('tmp:'+tmp)	;
		retenu = Math.floor(tmp/10);//retenu
		tblsolution['elefalc_'+(tblsolution['nbcol']-1-j)] = Math.floor(tmp/10)	;
		j++;
	}
	window.tblsolution = tblsolution;
}

function  updateExercice2(nb1, nb2, html_el, nbchiffres1){
	$('#zonsolution0').hide();
	$("#zoneclavierexercice").show();
	$('#spancorriger').html('Corriger');
	$("#btncorriger").show();
	
	var virgule = -1;
	if (nb1 === undefined || nb1===null || nb1.length===0){
		var i=0;
		var tmp = 0;
		var nbdigits1 = nbchiffres1 + Math.floor(Math.random()*2);
		while(i<nbdigits1){
			tmp *=10;	
			tmp += Math.floor(Math.random()*9)+1;
			i++;
		}
		nb1 = tmp;
		virgule = Math.floor(Math.random()*nbchiffres1)+1;
	}
	
	if (nb2 === undefined || nb2===null || nb2.length===0){
		var i=0;
		var tmp = 0;
		while(i<nbchiffres1){
			tmp *=10;	
			tmp += Math.floor(Math.random()*9)+1;			
			i++;
		}
		nb2 = tmp;
	}
	if (nb1<nb2){
		var tmp = nb1;
		nb1 = nb2;
		nb2 = tmp;
	}
	var nb1str;
	if (nb1.toString().indexOf('.')<0 && nb1.toString().indexOf(',')<0 && virgule<0){
		nb1str = nb1.toString() + ',0';
	}else if (nb1.toString().indexOf('.')>0){
		nb1str = nb1.toString().replace('.',',');
	}else{
		nb1str = nb1.toString().substr(0, nb1.toString().length - virgule) +','+nb1.toString().substr(nb1.toString().length - virgule); 
	}
	var nb2str;
	if (nb2.toString().indexOf('.')<0 && nb2.toString().indexOf(',')<0 &&  virgule<0){
		nb2str = nb2.toString() + ',0';
	}else if (nb2.toString().indexOf('.')>0){
		nb2str = nb2.toString().replace('.',',');
	}else{
		nb2str = nb2.toString().substr(0, nb2.toString().length - virgule) +','+nb2.toString().substr(nb2.toString().length - virgule); 
	}
	
	if (nb1str[0]===','){
		nb1str = '0' + nb1str;
	}
	if (nb2str[0]===','){
		nb2str = '0' + nb2str;
	}

	var k = nb1str.indexOf(',')>=0?nb1str.indexOf(',')+1:nb1str.length;
	var decimal1 = nb1str.length - k;
	
	k = nb2str.indexOf(',')>=0?nb2str.indexOf(',')+1:nb2str.length;
	var decimal2 = nb2str.length - k;
	
	while (decimal2>decimal1){
		nb1str = nb1str+'0';
		decimal1++;
	}
	while (decimal2<decimal1){
		nb2str = nb2str+'0';
		decimal2++;
	}
	
	var resu = parseInt(nb1str.replace(',','')) -parseInt(nb2str.replace(',',''));
	resu = resu.toString();
	while(resu.length<nb1str.replace(',','').length){
		resu ='0'+resu; 
	}
	var resustr = resu.toString().substr(0, nb1str.indexOf(',')) +','+resu.toString().substr(nb1str.indexOf(',')); 
	
	while(nb2str.length<nb1str.length){
		nb2str ='0'+nb2str; 
	}
	
	$('#zonprincipal > span.mbleu > nobr').html(' '+removeTrailingZeros(nb1str)+' - '+removeTrailingZeros(nb2str))
	$('div.colgche > table > tbody').html('');
	$('#zonsolution0 > table > tbody').html('');
	$("#btnrecommencer").attr("onclick","document.getElementById('txtnb1').value="+nb1str+";document.getElementById('txtnb2').value="+nb2str+"; validerparametresope('exercice-mathematiques-calculs-poser-operation-multiplication.html','modele04_multiplication.php', '2543', '21','3')");
	
	var nb1digits = getlength(nb1str);
	var str = '<tr style="height:2em;" valign="top"><td align="Center" valign="middle" class="txtope">&nbsp;</td>';
	var str2 = str;
	var i=0;
	var k = 0;
	while (i<nb1digits){		
		if (i==0){
			str+= '<td align="Center" class="casechiffre"><input type="tel" class="saisieretenuefalc saisieretenuefalcsoustr2" style="text-align:Center;visibility:hidden;" maxlength="1">'+nb1str.charAt(i)+'</td>';
			str2 += '<td align="Center" class="casechiffre"><input type="tel" class="saisieretenuefalc saisieretenuefalcsoustr2" style="text-align:Center;visibility:hidden;" maxlength="1">'+nb1str.charAt(i)+'</td>';
		}else if (nb1str.charAt(i)===',') {
			k++;
			str+= '<td align="left" class="casechiffre"><input type="tel" class="saisieretenuefalc saisieretenuefalcsoustr2" style="text-align:Center;visibility:hidden;" maxlength="1">,</td>'
			str2+= '<td align="left" class="casechiffre"><input type="tel" class="saisieretenuefalc saisieretenuefalcsoustr2" style="text-align:Center;visibility:hidden;" maxlength="1">,</td>'
		}else{
			k++;
			var retenu = k-1;
			/*if (nb1str.charAt(i-1)===','){
				retenu = k-2;
			}*/
			str+= '<td align="left" class="casechiffre"><input type="tel" name="txtele[]" id="eleb_1_'+k+'_1" tabindex="'+(100+(nb1digits-i)*2)+'" class="saisieretenuefalc saisieretenuefalcsoustr2" style="text-align:Center" maxlength="1" onfocus="factivation(this.id);saisieinit(this.id)" onchange="changeInput(this,'+k+','+retenu+',0)" onkeydown="testeffacer(event.keyCode,this)" onkeyup=" testtouchepasserfocus(event.keyCode,this.id,&quot;&quot;,this);">'+nb1str.charAt(i)+'</td>';
			str2+= '<td align="left" class="casechiffre"><input type="tel" name="txtsol[]" id="solb_1_'+k+'_1" tabindex="'+(100+(nb1digits-i)*2)+'" class="saisieretenuefalc saisieretenuefalcsoustr2" style="text-align: center; color: rgb(51, 153, 0);" maxlength="1" onfocus="factivation(this.id);saisieinit(this.id)" onkeydown="testeffacer(event.keyCode,this)" onkeyup="testtouchepasserfocus(event.keyCode,this.id,&quot;&quot;,this);" readonly="">'+nb1str.charAt(i)+'</td>';
			
		}
		i++;
	}
	str+='</tr>';
	str2+='</tr>'
	$('div.colgche > table > tbody').append(str);
	$('#zonsolution0 > table > tbody').append(str2);
	
	var nb2digits = getlength(nb2str);
	str='';
	str+='<tr valign="top"><td align="Center" valign="middle" class="txtope">-</td>';
	i=0;
	var stop = true;
	while (i<nb2digits){
		if (stop && nb2str.charAt(i)==='0' && nb2str.charAt(i+1)!==','){
			str+= '<td align="left" class="casechiffre"><input type="tel" class="saisieretenuefalc saisieretenuefalcsoustr2" style="text-align:Center;visibility:hidden;" maxlength="1"></td>';
		}else{
			stop = false;
			str+= '<td align="left" class="casechiffre"><input type="tel" class="saisieretenuefalc saisieretenuefalcsoustr2" style="text-align:Center;visibility:hidden;" maxlength="1">'+nb2str.charAt(i)+'</td>';
		}
		
		i++;
	}
	str+='</tr>';
	$('div.colgche > table > tbody').append(str);
	$('#zonsolution0 > table > tbody').append(str);
	
	str = '<tr align="center" valign="top" style="font-size:0.5em;"><td class="caseretenue" style="padding:0px;" width="30px">&nbsp;</td>';
	str2 =str;
	var i=0;
	var k = 0;
	while (i<nb1digits){		
		if (i==0){
			k++;
			str+= '<td valign="top" align="center"><input type="tel" disabled="disabled" name="txtelefalc[]" id="elefalc_1_'+k+'_2" tabindex="" class="saisieretenuefalc saisieretenuefalcsoustr1" style="text-align:Center;background-color:white;" maxlength="1" onfocus="factivation(this.id);saisieinit(this.id)" onkeydown="testeffacer(event.keyCode,this)"></td>';
			str2+= '<td valign="top" align="center"><input type="tel" disabled="disabled" name="txtsolfalc[]" id="solfalc_1_'+k+'_2" tabindex="" class="saisieretenuefalc saisieretenuefalcsoustr1" style="text-align:Center;background-color:white;" maxlength="1" onfocus="factivation(this.id);saisieinit(this.id)" onkeydown="testeffacer(event.keyCode,this)"></td>';
		}else if (nb1str.charAt(i)===',' || i+1>=nb1digits) {
			k++;
			str+= '<td>&nbsp;</td>';
			str2+=  '<td>&nbsp;</td>';
		}else{
			k++;
			str+= '<td valign="top" align="center"><input type="tel" disabled="disabled" name="txtelefalc[]" id="elefalc_1_'+k+'_2" tabindex="" class="saisieretenuefalc saisieretenuefalcsoustr1" style="text-align:Center;background-color:white;" maxlength="1" onfocus="factivation(this.id);saisieinit(this.id)" onkeydown="testeffacer(event.keyCode,this)"></td>';
			str2+= '<td valign="top" align="center"><input type="tel" disabled="disabled" name="txtsolfalc[]" id="solfalc_1_'+k+'_2" tabindex="" class="saisieretenuefalc saisieretenuefalcsoustr1" style="text-align:Center;background-color:white;" maxlength="1" onfocus="factivation(this.id);saisieinit(this.id)" onkeydown="testeffacer(event.keyCode,this)" readonly=""></td>';
		}
		i++;
	}
	str+='</tr>';
	str2+='</tr>';
	$('div.colgche > table > tbody').append(str);	
	$('#zonsolution0 > table > tbody').append(str2);
	
	str='';
	str+='<tr><td height="10px" class="traitopehorizbold" colspan="'+(nb1digits+1)+'">&nbsp;</td></tr>';
	$('div.colgche > table > tbody').append(str);
	$('#zonsolution0 > table > tbody').append(str);
	
	str='';
	str+='<tr><td align="Center" class="txtope">=</td>';
	str2=str;
	var i=0;
	var k = 0;
	while (i<nb1digits){
		k++;
		str+='<td align="right"><input type="tel" name="txtele[]" id="elea_1_'+(nb1digits+k)+'" tabindex="'+(100+1+(nb1digits-i)*2)+'" class="saisiechiffrenormal" style="text-align:Center;" maxlength="1" onfocus="factivation(this.id);saisieinit(this.id)" onkeydown="testeffacer(event.keyCode,this)" onkeyup="testtouchepasserfocus(event.keyCode,this.id,&quot;&quot;,this)"></td>';		
		str2+='<td align="right"><input type="tel" name="txtsol[]" id="sola_1_'+(nb1digits+k)+'" tabindex="'+(100+1+(nb1digits-i)*2)+'" class="saisiechiffrenormal" style="text-align: center; color: rgb(51, 153, 0);" maxlength="1" onfocus="factivation(this.id);saisieinit(this.id)" onkeydown="testeffacer(event.keyCode,this)" onkeyup="testtouchepasserfocus(event.keyCode,this.id,&quot;&quot;,this)" readonly=""></td>';	
		i++;
	}
	str+='</tr>';
	str2+='</tr>';
	$('div.colgche > table > tbody').append(str);
	$('#zonsolution0 > table > tbody').append(str2);
	
	var tblsolution=new Array();
	var i=0;
	var k = 0;
	var virg = nb1str.indexOf(',');
	while (i<nb1digits){			
		tblsolution['elea_1_'+ (nb1digits+i+1)] = resustr.charAt(i);
		if (i!==virg){			
			tblsolution['eleb_1_'+ (i)+ '_1'] = (parseInt(resustr.charAt(i)) + parseInt(nb2str.charAt(i))>parseInt(nb1str.charAt(i))?"%1":"%");
			if (i==virg+1){
				tblsolution['elefalc_1_'+ (i-1)+ '_2'] = (parseInt(resustr.charAt(i)) + parseInt(nb2str.charAt(i))>parseInt(nb1str.charAt(i))?"1":"");
			}else{
				tblsolution['elefalc_1_'+ (i)+ '_2'] = (parseInt(resustr.charAt(i)) + parseInt(nb2str.charAt(i))>parseInt(nb1str.charAt(i))?"1":"");
			}
			
		}
		i++;
	}
	window.tblsolution = tblsolution;
}

function  updateExercice(nb1, nb2, html_el, nbchiffres1){
	$('#zonsolution0').hide();
	$('#zonresultats').hide();
	idtxtcourant = "";
	$("#zoneclavierexercice").show();
	$('#spancorriger').html('Corriger');
	$("#btncorriger").show();
	
	var virgule = -1;
	if (nb1 === undefined || nb1===null || nb1.length===0){
		var i=0;
		var tmp = 0;
		var nbdigits1 = nbchiffres1 + Math.floor(Math.random()*4);
		while(i<nbdigits1){
			tmp *=10;	
			tmp += Math.floor(Math.random()*9)+1;
			i++;
		}
		nb1 = tmp;
		virgule = Math.floor(Math.random()*nbchiffres1)+1;
	}
	
	if (nb2 === undefined || nb2===null || nb2.length===0){
		var i=0;
		var tmp = 0;
		while(i<nbchiffres1){
			tmp *=10;	
			tmp += Math.floor(Math.random()*9)+1;			
			i++;
		}
		nb2 = tmp;
	}
	if (nb1<nb2){
		var tmp = nb1;
		nb1 = nb2;
		nb2 = tmp;
	}
	var nb1str;
	if (nb1.toString().indexOf('.')<0 && nb1.toString().indexOf(',')<0 && virgule<0){
		nb1str = nb1.toString() + ',0';
	}else if (nb1.toString().indexOf('.')>0){
		nb1str = nb1.toString().replace('.',',');
	}else{
		nb1str = nb1.toString().substr(0, nb1.toString().length - virgule) +','+nb1.toString().substr(nb1.toString().length - virgule); 
	}
	var nb2str;
	if (nb2.toString().indexOf('.')<0 && nb2.toString().indexOf(',')<0 &&  virgule<0){
		nb2str = nb2.toString() + ',0';
	}else if (nb2.toString().indexOf('.')>0){
		nb2str = nb2.toString().replace('.',',');
	}else{
		nb2str = nb2.toString().substr(0, nb2.toString().length - virgule) +','+nb2.toString().substr(nb2.toString().length - virgule); 
	}
	if (nb1str[0]===','){
		nb1str = '0' + nb1str;
	}
	if (nb2str[0]===','){
		nb2str = '0' + nb2str;
	}

	var k = nb1str.indexOf(',')>=0?nb1str.indexOf(',')+1:nb1str.length;
	var decimal1 = nb1str.length - k;
	
	k = nb2str.indexOf(',')>=0?nb2str.indexOf(',')+1:nb2str.length;
	var decimal2 = nb2str.length - k;
	
	while (decimal2>decimal1){
		nb1str = nb1str+'0';
		decimal1++;
	}
	while (decimal2<decimal1){
		nb2str = nb2str+'0';
		decimal2++;
	}
	
	var resu = parseInt(nb1str.replace(',','')) +parseInt(nb2str.replace(',',''));
	resu = resu.toString();	
	var resustr = resu.toString().substr(0, resu.length - (nb1str.length-nb1str.indexOf(',')-1)) +','+resu.toString().substr(resu.length - (nb1str.length-nb1str.indexOf(',')-1)); 
	if (resustr[0]===','){
		resustr = '0' + resustr;
	}
	
	while(nb2str.length<nb1str.length){
		nb2str ='0'+nb2str; 
	}
	var decimal = Math.max(nb1str.length - nb1str.indexOf(',')-1,nb2str.length - nb2str.indexOf(',')-1);

	$('#zonprincipal > span.mbleu > nobr').html(' '+removeTrailingZeros(nb1str)+' + '+removeTrailingZeros(nb2str))
	$('div.colgche > table > tbody').html('');
	$('#zonsolution0 > table > tbody').html('');
	$("#btnrecommencer").attr("onclick","document.getElementById('txtnb1').value='"+removeTrailingZeros(nb1str)+"';document.getElementById('txtnb2').value='"+removeTrailingZeros(nb2str)+"'; validerparametresope('exercice-mathematiques-calculs-poser-operation-multiplication.html','modele04_multiplication.php', '2543', '21','1')");
	


	var nbdigits = getlength(resustr);
	var str = '<tr><td class="caseretenue">&nbsp;</td>';
	var str2 = str;
	var i=0;
	while (i<nbdigits-1){		
		if (resustr.charAt(i)===',') {
			str+= '<td>&nbsp;</td>'
			str2+= '<td>&nbsp;</td>'
		}else{			
			str+= '<td align="Center"><input type="tel" name="txtele[]" id="eleb_1_'+(i+1)+'" tabindex="'+(100+(nbdigits-i)*2)+'" class="saisieretenuefalc" style="text-align:Center" maxlength="1" onfocus="factivation(this.id);saisieinit(this.id);" onkeydown="testeffacer(event.keyCode,this)" onkeyup="testtouchepasserfocus(event.keyCode,this.id,&quot;&quot;,this)"></td>';
			str2+= '<td align="Center"><input type="tel" name="txtsol[]" id="solb_1_'+(i+1)+'" tabindex="'+(100+(nbdigits-i)*2)+'" class="saisieretenuefalc" style="text-align:Center" maxlength="1" onfocus="factivation(this.id);saisieinit(this.id);" onkeydown="testeffacer(event.keyCode,this)" onkeyup="testtouchepasserfocus(event.keyCode,this.id,&quot;&quot;,this)"></td>';			
		}
		i++;
	}
	str+='</tr>';
	str2+='</tr>'
	$('div.colgche > table > tbody').append(str);
	$('#zonsolution0 > table > tbody').append(str2);
	

	var nb1digits = getlength(nb1str);
	str='';
	str+='<tr style="height:2em;" valign="top"><td align="Center" valign="middle" class="txtope">&nbsp;</td>';
	i=0;
	while (i<nbdigits){
		if (nbdigits-i>nb1digits){
			str+= '<td align="Center" class="casechiffre"></td>';
		}else{
			console.log('pos1:'+(nb1digits - (nbdigits-i)));
			str+= '<td align="Center" class="casechiffre">'+nb1str.charAt(nb1digits - (nbdigits-i))+'</td>';
		}	
		i++;
	}
	str+='</tr>';
	$('div.colgche > table > tbody').append(str);
	$('#zonsolution0 > table > tbody').append(str);
	
	var nb2digits = getlength(nb2str);
	
	
	str='';	
	str = '<tr style="height:2em;" valign="top">	<td align="Center" valign="middle" class="txtope">+</td>';
	i=0;
	while (i<nbdigits){
		var pos = nb2digits - (nbdigits-i);
		if (nbdigits-i>nb2digits){
			str+= '<td align="Center" class="casechiffre"></td>';
		}else if (nb2str.slice(pos,pos+1) === '0' && !((nb2str.slice(pos+1,pos+2) === ',') ||(nb2str.slice(pos+1,pos+2) === '.') ||(nb2str.slice(pos+1,pos+2) === '') )){
			str+= '<td align="Center" class="casechiffre"></td>';			
		}else{
			console.log('pos2:'+(nb2digits - (nbdigits-i)));
			str+= '<td align="Center" class="casechiffre">'+nb2str.charAt(nb2digits - (nbdigits-i))+'</td>';
		}	
		i++;
	}
	str+='</tr>';
	$('div.colgche > table > tbody').append(str);	
	$('#zonsolution0 > table > tbody').append(str);
	
	str='';
	str+='<tr><td height="10px" class="traitopehorizbold" colspan="'+(nbdigits+1)+'">&nbsp;</td></tr>';
	$('div.colgche > table > tbody').append(str);
	$('#zonsolution0 > table > tbody').append(str);
	
	str='';
	str+='<tr><td align="Center" class="txtope">=</td>';
	str2=str;
	i=0;
	while (i<nbdigits){
		str+='<td align="right"><input type="tel" name="txtele[]" id="elea_1_'+(nbdigits+i+1)+'" tabindex="'+(100+1+(nbdigits-i)*2)+'" class="saisiechiffrenormal" style="text-align:Center;" maxlength="1" onfocus="factivation(this.id);saisieinit(this.id)" onkeydown="testeffacer(event.keyCode,this)" onkeyup="testtouchepasserfocus(event.keyCode,this.id,&quot;&quot;,this)"></td>';		
		str2+='<td align="right"><input type="tel" name="txtsol[]" id="sola_1_'+(nbdigits+i+1)+'" tabindex="'+(100+1+(nbdigits-i)*2)+'" class="saisiechiffrenormal" style="text-align: center; color: rgb(51, 153, 0);" maxlength="1" onfocus="factivation(this.id);saisieinit(this.id)" onkeydown="testeffacer(event.keyCode,this)" onkeyup="testtouchepasserfocus(event.keyCode,this.id,&quot;&quot;,this)" readonly=""></td>';	
		i++;
	}
	str+='</tr>';
	str2+='</tr>';
	$('div.colgche > table > tbody').append(str);
	$('#zonsolution0 > table > tbody').append(str2);
	
	var tblsolution=new Array();
	i=0;
	while (i<nbdigits){			
		tblsolution['elea_1_'+ (nbdigits+i+1)] = resustr.charAt(i);
		i++;
	}	
	i=nb1digits-1;
	var retenu = 0;
	while (i>=0){
		if (nb1str.charAt(i)!==',') {
			var value= parseInt(nb1str.charAt(i)) + parseInt(nb2str.charAt(i))  + retenu; 
			var retenu =  Math.floor(value/10);	
			console.log(retenu);
			if (i>=0 && nb1str.charAt(i-1)!==',') {
				tblsolution['eleb_1_'+ (nbdigits-1-(nb1digits-1-i))] = (retenu>0?"%"+retenu:"%");
			}else{
				tblsolution['eleb_1_'+ (nbdigits-2-(nb1digits-1-i))] = (retenu>0?"%"+retenu:"%");
			}
		}
		//tblsolution['elea_1_'+ (nbdigits+i)] = resustr.charAt(i);
		i--;
	}
	window.tblsolution = tblsolution;
}

function majtabindex(objet) {
    var existe = document.getElementById("retA.1") || 0;
    if (existe != 0) {
        console.log('existe.tabIndex:'+existe.tabIndex)
		console.log('objet.tabIndex:'+objet.tabIndex)
        var coefinit = Math.floor((existe.tabIndex - 2) / 50);
        var coefdest = Math.floor((objet.tabIndex - 6) / 50);
		console.log('coefinit:'+coefinit)
		console.log('coefdest:'+coefdest)
        var diff = (coefdest - coefinit) * 50;
        if (diff != 0) {
            var i = 1;
            existe = document.getElementById("retA." + i) || 0;
            while (existe != 0) {
                existe.tabIndex = existe.tabIndex + diff;
                i++;
                existe = document.getElementById("retA." + i) || 0
            }
        }
    }
}

var consttxtboutoncorr2 = "Corriger 2ème chance";
var cumulpts = 0;
var connecte = 0;
var tabnavigationnote = new Array();
var nbliens = 0;

function restaurecaract(strsolution) {
    strsolution = strsolution.replace(/§{/g, "'");
    strsolution = strsolution.replace(/§~/g, "\\\'");
    strsolution = strsolution.replace(/§#/g, '"');
    return (strsolution)
}

function removeExtraSpace(str) {
    str = str.replace(/[\s]{2,}/g, " ");
    str = str.replace(/^[\s]/, "");
    str = str.replace(/[\s]$/, "");
    return str
}


var tapprec = 
[
   null,
   [
      null,
      [
         null,
         "Très fort !<br />Comment fais-tu ?",
         "Excellent !<br />Tu progresses<br />vraiment bien !",
         "Bien joué !<br />Continue comme ça !",
         "Superbe ! <br />Tu iras loin !",
         "C'est<br />vraiment bien !",
         "Félicitations ! <br />Tu as réussi !",
         "Je n'aurais pas<br />pu faire mieux que toi ! <br />Bravo !",
         "<br />Épatant !",
         "Parfait,<br />on forme une équipe<br />qui gagne !",
         "<br />Brillantissime !",
         "C'est bien<br />agréable d'avoir une<br />bonne note !"
      ],
      [
         null,
         "Bravo !<br />Tu t'y connais mieux<br />que moi !",
         "Hey ! <br>Ta note est<br />excellente ! ",
         "Quelle belle<br />performance ! <br />Bravo !",
         "Tu as<br />tout compris !",
         "Fabuleux !<br />Continue ainsi !",
         "Tu t'es<br />surpassé(e) <br /> &nbsp; aujourd'hui !",
         "C'est<br />exactement ça ! <br /> &nbsp; On continue ?",
         "Bravo !<br />Tu as dû beaucoup<br />t'exercer !",
         "<br />Beau score !",
         "<br />Parfait !",
         "Super !<br />tu vas pouvoir<br />m'aider !",
         "Ta note est <br />bien meilleure que<br />la mienne !"
      ],
      [
         null,
         "Excellent !<br />Tu m'expliqueras<br />la leçon ?",
         "Extra ! <br />Tu es vraiment<br />doué(e) !",
         "<br />Sensationnel !",
         "<br />Magnifique !",
         "Tu as fait<br />un excellent travail !",
         "Continue à bien<br />travailler.",
         "Youpi, <br />comme toi, <br />j'ai réussi !",
         "Ça,<br />c'est du travail<br />bien fait !",
         "Bravo à toi !<br />Moi, j'ai rien<br />compris !",
         "C'est super,<br />moi aussi j'ai<br />eu 10/10 !",
         "Tout juste !<br />C'est génial !"
      ],
      [
         null,
         "Génial !<br />Hé, ça surchauffe<br />les neurones !",
         "WOUAH !<br />Tu t'es surpassé(e) !",
         "<br />Extraordinaire !",
         "<br />C'est pas beau la vie ?",
         "Rien ne<br />peut t'arrêter<br />maintenant !",
         "Waouh !<br />C'est beau !",
         "Hé !<br />Mais t'es vraiment<br />trop balaise !!!",
         "Hé,<br />mais t'es un(e)<br /> &nbsp; champion(ne) !",
         "Hé !<br />Tu exploses le score !",
         "On est<br />trop doués.<br />Non ?",
         "On sort<br />le champagne ?",
         "Mais t'as<br />vraiment la patate<br />aujourd'hui !",
         "<br />Trop cool !",
         "<br />Chapeau bas !"
      ]
   ],
   [
      null,
      [
         null,
         "Une bonne<br />note. Voilà qui fait<br />plaisir !",
         "Bon travail !<br />La prochaine fois<br />tu auras 10/10 !",
         "Encore un peu<br />d'efforts pour avoir<br />tout juste !",
         "C'est pas mal,<br />mais corrige bien<br />tes erreurs !",
         "Encore une<br />bonne note. On fait<br />une pause ?",
         "Bravo<br />pour ce bon<br />résultat !"
      ],
      [
         null,
         "Oui, bien !<br />J'ai eu la même note<br />que toi.",
         "Tu y es<br />presque. Continue !",
         "C'est pas<br />encore parfait, mais<br /> ça va venir !",
         "<br />Bonne prestation !",
         "Bravo à toi.<br />Moi je n'ai rien<br />compris !",
         "Comprends<br />bien tes erreurs avant<br /> de continuer !"
      ],
      [
         null,
         "C'est bien !<br />On progresse<br />ensemble.",
         "Tu es sur<br />la bonne voie !",
         "Oui, bien !<br />Tu n'es pas loin d'avoir<br />tout juste.",
         "Presque &nbsp; <br />tout juste !<br />C'est bien !",
         "Un petit effort <br />et tu seras<br />champion(ne).",
         "C'est bien !<br />On continue ?"
      ],
      [
         null,
         "Encore un peu et<br />on va atteindre<br />la perfection !",
         "Bon, c'était<br />presque juste, et c'est<br />pas si mal !",
         "Bien joué,<br />mais moi j'ai eu<br />10/10 !",
         "Lâche pas, <br />tu t'améliores !",
         "Ha, Ha, c'est pas<br />mal, mais c'est pas<br />tout juste !",
         "Allez,<br />on essaye d'avoir<br />tout juste ?"
      ]
   ],
   [
      null,
      [
         null,
         "Moi aussi, j'ai<br />encore quelques erreurs<br />à corriger…",
         "Lâche pas ! <br />On va y arriver !",
         "Encore une fois<br />et tu auras 10/10 !",
         "Regarde<br />bien la correction pour<br />progresser !",
         "J'ai eu les<br />mêmes erreurs que toi.<br />On les corrige ?",
         "On reprend<br/>la leçon ensemble ?"
      ],
      [
         null,
         "Je crois <br />qu'on peut encore<br />progresser...",
         "Tu es sur le <br />point de réussir. <br />Continue !",
         "Il y a des<br />erreurs, mais<br />ça va venir !",
         "Zut !<br />Je pensais avoir<br />tout juste !",
         "On peut<br />faire mieux. Qu'en<br />penses-tu ?",
         "Ce ne sont pas<br />quelques erreurs qui<br />&nbsp; vont nous arrêter."
      ],
      [
         null,
         "Pas si mal, <br />mais on peut encore<br />s'améliorer.",
         "Allez, on continue.<br />C'est pas parfait<br />tout ça !",
         "Je commence<br />à comprendre.<br />Et toi ?",
         "J'ai encore<br />un peu de mal à tout <br />comprendre !",
         "J'ai eu 9/10.<br />Essaye de faire<br />pareil !",
         "Encore<br />un effort et on va<br />y arriver."
      ],
      [
         null,
         "D'accord, ça va à<br />peu près, mais on peut<br />&nbsp;encore s'entrainer.",
         "C'est<br />bien moyen tout ça.<br />Recommençons !",
         "Je pensais<br />que ce serait mieux<br />que ça...",
         "Allez,<br />ça va finir par<br />rentrer !",
         "T'inquiète,<br /> on va y arriver !",
         "Bosse bien,<br /> et tu auras tout<br />juste !"
      ]
   ],
   [
      null,
      [
         null,
         "Pas facile<br />cet exercice, mais courage,<br />on va y arriver !",
         "C'est un peu<br />juste... Il faut encore<br />travailler...",
         "Tu peux<br />y arriver, toi aussi.<br />Continue !",
         "Persévère,<br />Persévère !",
         "J'aimerais<br />tellement avoir<br />la moyenne !",
         "Une mauvaise<br />note, ce n'est pas<br />agréable !"
      ],
      [
         null,
         "D'habitude, moi<br />aussi je fais mieux. Je vais<br />revoir la leçon !",
         "Je n'ai pas eu<br />mieux que toi... Je vais<br />retravailler.",
         "Tu peux<br />mieux faire...<br />C'est sûr.",
         "Relis la leçon<br />et regarde bien<br />la correction !",
         "Pas facile<br />cet exercice ! Mais on va<br />s'exercer.",
         "Un peu difficile,<br />mais on va<br />y arriver !"
      ],
      [
         null,
         "Zut ! moi non<br />plus, je n'ai pas<br />très bien réussi.",
         "Bon, on revoit<br />la leçon et on<br />recommence ?",
         "Décevant<br />notre résultat. On<br />recommence ?",
         "Zut, c'est<br />un peu compliqué<br />tout ça !",
         "Comme toi, il faut <br />que je retravaille cet <br />exercice !",
         "Et si on<br />comprenait nos<br />erreurs ?"
      ],
      [
         null,
         "Ho, Ho !<br />Quelqu'un peut-il<br />nous aider ?",
         "Pour te redonner<br />du courage, lis donc la<br />blague du jour !",
         "Allez, du nerf.<br />Ne te décourage<br />pas !",
         "Encore un<br />effort, on sera bientôt<br />au top !",
         "Pas de panique !<br/>Ca va venir... J'en<br />suis sûr.",
         "D'accord,<br />ce n'est pas fameux.<br />Continuons !"
      ]
   ],
   [
      null,
      [
         null,
         "Pfffff... <br />Il me casse la tête<br />cet exercice !",
         "Bon, je me<br />concentre et j'essaye<br />de comprendre...",
         "Je vais recommencer<br />dix fois s'il le faut,<br />mais j'y arriverai !",
         "Difficile<br />cet exercice... Je vais<br />batailler.",
         "Allez, <br />on tient bon ! <br />On continue !",
         "Surtout,<br />regarde bien<br />la correction !",
         "Et si on<br />reprenait la leçon<br />ensemble ?",
         "J'en ai marre,<br />mais je vais y arriver,<br /> je vais y arriver !"
      ],
      [
         null,
         "Aïe, aïe, aïe, <br />Mais qui est  l'inventeur<br />de cet exercice ?",
         "Pas de panique !<br />Je sais que tu vas<br />y arriver !",
         "Hum, Hum...<br />et si on essayait de<br />comprendre ?",
         "Quand tu auras<br />compris, tu pourras<br />m'expliquer ?",
         "Bon, surtout,<br />ne pas se décourager !<br />On va réussir.",
         "Je n'ai rien<br />compris. Je recommence<br />plein de fois !",
         "Regardons<br />la correction ensemble<br />et recommençons !",
         "Aïe Aïe Ouille...<br />Comme moi ! On va<br />retravailler."
      ],
      [
         null,
         "Oups... <br />Je commence à avoir<br />une petite fatigue.",
         "Je n'ai rien<br />compris moi non plus.<br />On recommence ?",
         "De la patience,<br />de la motivation...<br />On va y arriver !",
         "N'y a t'il pas<br />d'exercice plus<br />facile ?",
         "Bon, c'est pas<br />terrible. Mais on peut<br />réessayer ?",
         "Pas terrible…<br />As-tu bien lu la<br />consigne ?",
         "Aïe, aïe, aïe, <br />Moi non plus, j'aime<br />pas les 0 !",
         "Bon, c'est<br />pas notre journée, mais<br />restons positifs !"
      ],
      [
         null,
         "Là, j'ai pris<br />un petit coup<br />au moral...",
         "Hou ! J'ai<br />la tête dans les<br />chaussettes !",
         "Cet exercice<br />me hérisse les poils !<br />On s'accroche.",
         "C'est pas<br />mon truc cet exercice<br />là !",
         "J'aime pas<br />les mauvaises notes...<br />Tenons le choc.",
         "Mouais !<br />On retravaille ensemble ?<br />D'accord ?",
         "Aujourd'hui,<br />je n'y suis pas<br />du tout !",
         "On ne<br />baisse pas les bras, <br />on continue !"
      ]
   ]
];

function listemajnote(note) {
    if (document.getElementById("txtexono"))
        if (document.getElementById("txtexono").value != "" && document.getElementById("txtexono").value != "0") {
            var tres = document.getElementById("txtexono").value.split(".");
            if (document.getElementById('txtliste_no'))
                if (document.getElementById('txtliste_no').value == tres[0]) {
                    var exono = tres[1];
                    var sommenotes = parseFloat(document.getElementById("liste_som_" + exono).value) + parseFloat(note);
                    var nbnotes = parseFloat(document.getElementById("liste_nb_" + exono).innerHTML) + 1;
                    document.getElementById("liste_som_" + exono).value = sommenotes;
                    document.getElementById("liste_nb_" + exono).innerHTML = nbnotes;
                    document.getElementById("liste_moy_" + exono).innerHTML = (sommenotes / nbnotes).toFixed(1);
                    document.getElementById("zon_liste_moy_" + exono).style.display = "block";
                    var sommenotes = parseFloat(document.getElementById("txtliste_somnotes").value) + parseFloat(note);
                    var nbnotes = parseFloat(document.getElementById("listenbnotes").innerHTML) + 1;
                    document.getElementById("txtliste_somnotes").value = sommenotes;
                    if (nbnotes == 1)
                        document.getElementById("listenbnotes").innerHTML = nbnotes + " note";
                    else
                        document.getElementById("listenbnotes").innerHTML = nbnotes + " notes";
                    var moy = (sommenotes / nbnotes).toFixed(1);
                    document.getElementById("listemoyenne").innerHTML = moy + "/10"
                }
        }
}

function traiterresultats(correction, note, nbjustes, nbelem, points, nbftes, message, image, txtresultat) {
    if (document.getElementById('zonefenetrecontrole'))
        controle_retour_correction(note, nbjustes, nbelem, 1);
    else {
        var strdefi = "";
        if (document.getElementById('zondefi')) {
            if ($('#zondefi').css("display") == "block" && correction == 1) {
                if (defi_gagne == 1) {
                    defi_stop(nbftes);
                    if (nbftes == 0) {
                        var pointssup = Math.ceil(points * defivitesse / 5);
                        if (pointssup >= 1)
                            strdefi += " <b>DEFI REUSSI !</b> + " + pointssup + " points !";
                        else
                            strdefi += " <b>DEFI REUSSI !</b> + " + pointssup + " point !";
                        points = points + pointssup
                    } else {
                        strdefi += "<b>DEFI NON REUSSI !</b>"
                    }
                } else {
                    defi_correction();
                    strdefi += "<b>DEFI NON REUSSI !</b> Manque de rapidité"
                }
            } else
                $('#zondefi').css("display", "none")
        }
        //document.getElementById('imgresultat').src = image;
        $('#divresultat').html(txtresultat);
        if (correction == 1)
            $('#zonspancorrection').html("");
        else
            $('#zonspancorrection').html(", à la 2<sup>ème</sup> correction");
        $('#zonspannote').html(note);
        $('#zonspanjustes').html(nbjustes);
        $('#zonspannbptstot').html(nbelem);
        $('#zonspanpoints').html(points);
        var cumulptsancien = cumulpts;
        cumulpts += points;
        $('#zonspancagnotte').html(cumulpts);
        if (connecte > 0) {
            var reste = constnbpointscadeau - parseInt(cumulpts % constnbpointscadeau);
            $('#zonspanrestecadeau').html("Prochaine récompense : <b>" + reste + " points</b>")
        } else
            $('#zonspanrestecadeau').html("");
        $('#zonspanmessage').html(message);
        if (nbftes > 1) {
            plurielftes = "s";
            messageftes = " les " + nbftes + " erreurs"
        } else {
            plurielftes = "";
            if (nbftes == 1)
                messageftes = " l'erreur"
        }
        if ($('#spancorriger').html() == consttxtboutoncorr2 && $("#btncorriger").css("display") != "none") {
            $('#zonspanmessagecor2').html("Tu peux corriger " + messageftes + " puis clique sur <b>Corriger 2ème chance</b> pour voir le corrigé.")
        } else if (nbftes >= 1)
            $('#zonspanmessagecor2').html("Il reste " + nbftes + " erreur" + plurielftes);
        else
            $('#zonspanmessagecor2').html("Aucune erreur.");
        $('#zonspanmessage').html($('#zonspanmessage').html() + strdefi);
        //$('#zonresultats').css("display", "block");
        if (correction == 1) {
            tabnavigationnote[nbliens] = note;
            listemajnote(note)
        }
        if (connecte > 0) {
            var rubno = document.forms['frmresultats'].txtrubno.value;
            if (document.getElementById("n" + rubno) && correction == 1) {
                var nb = Number(document.getElementById("nnb" + rubno).innerHTML);
                var moy = (Number(document.getElementById("nm" + rubno).innerHTML) * nb + note) / (nb + 1);
                $("#nm" + rubno).html(Math.round(moy * 10) / 10);
                $("#nnb" + rubno).html(nb + 1);
                $("#n" + rubno).css("display", "inline")
            }
            str = "g/note/enregistrer.php?rubno=" + rubno + " &note=" + note + " &points=" + points + " &difficulte=" + document.forms['frmresultats'].txtdifficulte.value + " &correction=" + correction + "  &info=" + escape(document.forms['frmresultats'].txtinfo.value) + " &parametres=" + escape(document.forms['frmresultats'].txtparametres.value) + " &cumulpts=" + cumulptsancien + "&listeexono=" + escape(document.forms['frmresultats'].txtexono.value);
            eaxG(str, 14, '')
        }
    }
}

function getmessage(note, modele, chance) {
    if (note == 10) {
        niv = "1"
    } else if (note >= 7) {
        niv = "2"
    } else if (note >= 5) {
        niv = "3"
    } else if (note >= 2) {
        niv = "4"
    } else {
        niv = "5"
    }
    var numimg = 1 + Math.floor(Math.random() * 4);
    var numappreciation = 1 + Math.floor(Math.random() * (tapprec[niv][numimg].length - 1));
    var tabretour = new Array();
    tabretour[0] = "images/resultat_" + niv + numimg + ".png";
    tabretour[1] = "";
    tabretour[2] = tapprec[niv][numimg][numappreciation];
    return (tabretour)
}
function afficherresultatsdoublecorrection(nbelem, nbftes) {
    note = 0;
    if ((nbelem - nbftes) > 0)
        note = (Math.round((nbelem - nbftes) * 10 / nbelem));
    nbjustes = 0;
    if ((nbelem - nbftes) > 0)
        nbjustes = (nbelem - nbftes);
    if ($('#spancorriger').html() == consttxtboutoncorr2) {
        correction = 2;
        $('#btncorriger').css('display', 'none');
        var tabresultat = new Array();
        tabresultat = getmessage(note, "classique", 0);
        var image = tabresultat[0];
        var message = tabresultat[1];
        var txtresultat = tabresultat[2];
        nbftesprec = parseFloat(document.forms['frmresultats'].txtnbftes.value);
        points = Math.round((nbftesprec - nbftes) / 2);
        if (points < 0)
            points = 0;
        $("#zoneclavierexercice").css("display", "none")
    } else {
        correction = 1;
        var tabresultat = new Array();
        tabresultat = getmessage(note, "classique", 1);
        var image = tabresultat[0];
        var message = tabresultat[1];
        var txtresultat = tabresultat[2];
        points = 0;
        if ((nbelem - nbftes) > 0)
            points = (nbelem - nbftes);
        document.forms['frmresultats'].txtnbftes.value = nbftes;
        if (nbftes == 0) {
            $('#btncorriger').css('display', 'none');
            message = "Excellent ! Félicitations !";
            $("#zoneclavierexercice").css("display", "none")
        } else {
            $('#spancorriger').html(consttxtboutoncorr2)
        }
    }
    traiterresultats(correction, note, nbjustes, nbelem, points, nbftes, message, image, txtresultat)
}

function corriger02(strsolution, codeajax) {
    var i = 0;
    var nbjustes = 0;
    var nbelem = 0;
    var nbfaux = 0;
    var idele = "";
    var idsol = "";
    var strunesolution = "";
    var strsuitesolution = "";
    var juste = 0;
    if (!codeajax)
        codeajax = 0;
    //eval(strsolution);
    for (i = 0; i < document.forms['frmope'].elements.length; i++) {
        if (document.forms['frmope'].elements[i].name == "txtele[]") {
            idele = document.forms['frmope'].elements[i].id;
            idsol = "sol" + idele.substr(3, idele.length);
            var idzonejuste = "zonjuste" + idele.substr(3, idele.length);
            var idzonefaux = "zonfaux" + idele.substr(3, idele.length);
            strsuitesolution = tblsolution[idele];
			console.log(idele);
            repeleve = removeExtraSpace(document.forms['frmope'].elements[i].value.toUpperCase());
            var reg = new RegExp("(/)","g");
            repeleve = repeleve.replace(reg, ":");
            if (strsuitesolution.charAt(0) == "%") {
                strsuitesolution = strsuitesolution.substring(1, strsuitesolution.length);
                tblsolution[idele] = strsuitesolution;
                repeleve = repeleve.replace(",", ".");
                repeleve = repeleve.replace(/ /g, "");
                strsuitesolution = strsuitesolution.replace(",", ".");
                if (strsuitesolution.length > 3)
                    strsuitesolution = strsuitesolution.replace(/ /g, "");
                if (repeleve != '') {
                    repeleve = parseFloat(repeleve)
                }
            }
            if (repeleve == removeExtraSpace(strsuitesolution.toUpperCase())) {
                juste = 1
            } else {
                juste = 0;
                while (strsuitesolution != "" && juste == 0) {
                    if (strsuitesolution.indexOf("/") > -1) {
                        strunesolution = strsuitesolution.substr(0, strsuitesolution.indexOf("/"));
                        strsuitesolution = strsuitesolution.substr(strsuitesolution.indexOf("/") + 1, strsuitesolution.length)
                    } else {
                        strunesolution = strsuitesolution;
                        strsuitesolution = ""
                    }
                    if (repeleve == removeExtraSpace(strunesolution.toUpperCase()))
                        juste = 1
                }
            }
            if (juste == 1) {
                document.forms['frmope'].elements[i].readOnly = true;
                document.forms['frmope'].elements[i].style.color = colorTrue;
                document.forms['frmope'].elements[i].style.backgroundColor = backgroundColorTrue;
                if (document.getElementById(idzonejuste)) {
                    document.getElementById(idzonejuste).style.display = "block";
                    if (document.getElementById(idzonefaux)) {
                        document.getElementById(idzonefaux).style.display = "none"
                    }
                }
                document.getElementById(idsol).style.color = colorCorrigeJuste;
                nbjustes++
            } else {
                nbfaux++;
                document.getElementById(idsol).style.color = colorCorrigeFaux;
                document.forms['frmope'].elements[i].style.color = colorFalse;
                document.forms['frmope'].elements[i].style.backgroundColor = backgroundColorFalse;
                if (document.forms['frmope'].elements[i].value == "")
                    document.forms['frmope'].elements[i].value = "?";
                if ($('#spancorriger').html() == consttxtboutoncorr2)
                    document.forms['frmope'].elements[i].readOnly = true;
                if (document.getElementById(idzonefaux)) {
                    document.getElementById(idzonefaux).style.display = "block";
                    if (document.getElementById(idzonejuste)) {
                        document.getElementById(idzonejuste).style.display = "none"
                    }
                }
            }
            if (tblsolution[idele] == "/0" || tblsolution[idele] == "0/")
                document.getElementById(idsol).value = "0";
            else
                document.getElementById(idsol).value = tblsolution[idele];
            document.getElementById(idsol).readOnly = true;
            if (tblsolution[idele] != "")
                nbelem = nbelem + 1
        } else {
            if (document.forms['frmope'].elements[i].name == "txtelefalc[]") {
                idele = document.forms['frmope'].elements[i].id;
                idsol = "solfalc" + idele.substr(7, idele.length);
				console.log('idsol:'+idsol)
                document.getElementById(idsol).value = tblsolution[idele] ;
                document.getElementById(idsol).readOnly = true
            }
        }
    }
    if ($('#spancorriger').html() == consttxtboutoncorr2 || nbfaux == 0) {
        i = 0;
        var existe = document.getElementById('zonsolution' + i) || 0;
        while (existe != 0) {
            if ($('#zonsolution' + i).is("div.coldte"))
                existe.style.display = "inline-block";
            else
                existe.style.display = "block";
            existe.style.visibility = "visible";
            i++;
            var existe = document.getElementById('zonsolution' + i) || 0
        }
    }
	if ($('#spancorriger').html() !== consttxtboutoncorr2){		
		gwd.checkAndIncrement2(nbelem, nbfaux,1);
	}else{
		gwd.checkAndIncrement2(nbelem, nbfaux,2);
	}
    afficherresultatsdoublecorrection(nbelem, nbfaux)
}

function lancercorriger02(strsolution) {
    //strsolution = restaurecaract(strsolution);
    corriger02(strsolution)
}

var idtxtcourant = "";
function factivation(id) {
    idtxtcourant = id
}

var colorNormal = 'black';
var colorTrue = 'black';
var colorCorrige = 'red';
var colorCorrigeJuste = 'black';
var colorCorrigeFaux = "#390";
var colorFalse = 'black';
var backgroundColorNormal = 'white';
var backgroundColorTrue = '#69d469';
var backgroundColorFalse = '#ff9900';
function saisieinit(idele) {
    var existe = document.getElementById(idele) || 0;
    if (existe != 0) {
        if (document.getElementById(idele).readOnly == false) {
            document.getElementById(idele).style.backgroundColor = backgroundColorNormal;
            if (document.getElementById(idele).value == "?") {
                document.getElementById(idele).style.color = colorNormal;
                document.getElementById(idele).value = ""
            }
        }
    }
}

function remplacercaracteres(lachaine) {
    lachaine = lachaine.replace(/§qot/g, "'");
    var repfin = '';
    if (idtxtcourant != "")
        if (document.getElementById(idtxtcourant)) {
            if (document.getElementById(idtxtcourant).readOnly == false) {
                var letxt = document.getElementById(idtxtcourant);
                if (letxt.disabled != true && letxt.readOnly == false) {
                    letxt.value = lachaine;
					var event = new Event('change');
					letxt.dispatchEvent(event);
                }				
            }
        }
}

function passerfocus(idcurrent, idform) {
    if (idcurrent == "") {
        indexcible = 1;
        existe = document.getElementById(idform) || 0
    } else {
        existe = 1;
        indexcible = document.getElementById(idcurrent).tabIndex + 1;
        idform = document.getElementById(idcurrent).form.id
    }
    if (existe != 0) {
        meilleurindex = 1000;
        i = 0;
        trouve = false;
        idcour = "";
        document.forms[idform].elements.length;
        while (i < document.forms[idform].elements.length && trouve == false) {
            id = document.forms[idform].elements[i].id;
            if (id != "") {
                if (document.getElementById(id).id.substr(0, 3) != "sol") {
                    if (document.getElementById(id).tabIndex == indexcible) {
                        trouve = true;
                        idcour = id
                    } else {
                        if (document.getElementById(id).tabIndex > indexcible && document.getElementById(id).tabIndex < meilleurindex) {
                            meilleurindex = document.getElementById(id).tabIndex;
                            idcour = id
                        }
                    }
                }
            }
            i++
        }
        if (idcour != "") {
            if (document.getElementById(idcour)) {
                document.getElementById(idcour).select();
                document.getElementById(idcour).focus();
                document.getElementById(idcour).focus()
            }
        }
    }
}

function testeffacer(codetouche, objet) {
    if (objet.readOnly == false && ((codetouche >= 48 && codetouche <= 57) || (codetouche >= 96 && codetouche <= 105)))
        objet.value = ""
}

function testtouchepasserfocus(codetouche, idcurrent, idform, objet) {
    if (codetouche == 110 || codetouche == 190)
        objet.value = ",";
    if ((codetouche >= 48 && codetouche <= 57) || (codetouche >= 96 && codetouche <= 105) || codetouche == 13 || codetouche == 9 || codetouche == 110 || codetouche == 188)
        passerfocus(idcurrent, idform)
}

function testtoucheentrer(codetouche, idsuivant, idvalider) {
    if (codetouche == 13)
        if (idvalider != '') {
            document.getElementById(idvalider).click()
        } else
            document.getElementById(idsuivant).focus()
}

