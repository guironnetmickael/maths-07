// function utils

function shuffle (array) {
  let currentIndex = array.length; let temporaryValue; let randomIndex

  // While there remain elements to shuffle...
  const arrayBis = array.slice()
  while (currentIndex !== 0) {
    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex)
    currentIndex -= 1

    // And swap it with the current element.
    temporaryValue = arrayBis[currentIndex]
    arrayBis[currentIndex] = arrayBis[randomIndex]
    arrayBis[randomIndex] = temporaryValue
  }

  return arrayBis
}

function combinaisonListes (liste, tailleMinimale) {
  if (liste.length === 0) return []
  let l = shuffle(liste)
  while (l.length < tailleMinimale) {
    l = l.concat(shuffle(liste))
  }
  return l
}

function randint (min, max, listeAEviter = []) {
  // Source : https://gist.github.com/pc035860/6546661
  const range = max - min
  let rand = Math.floor(Math.random() * (range + 1))
  if (typeof listeAEviter === 'string') {
    listeAEviter = listeAEviter.split('')
  }
  if (Number.isInteger(listeAEviter)) {
    listeAEviter = [listeAEviter]
  }
  listeAEviter = listeAEviter.map(Number)
  if (listeAEviter.length > 0) {
    while (listeAEviter.indexOf(min + rand) !== -1) {
      rand = Math.floor(Math.random() * (range + 1))
    }
  }
  return min + rand
}

function questionJamaisPosee(listeArguments, ...args) {
    let argsConcatenes = ''
    for (const arg of args) {
      if (arg !== undefined) argsConcatenes += arg.toString()
    }
    if (listeArguments.indexOf(argsConcatenes) > -1) {
      return false
    } else {
      listeArguments.push(argsConcatenes)
      return true
    }
 }
 
function ecritureParentheseSiNegatif (a) {
  let result = ''
  if (a >= 0) {
     result = a.toLocaleString("fr-FR", { maximumFractionDigits: 10 })
  } else {
     result = '(' + a.toLocaleString("fr-FR", { maximumFractionDigits: 10 }) + ')'
  }
  return result
}

function ecritureNombreRelatif (a) {
  let result = ''
  if (a > 0) {
    result = '(+' + a.toLocaleString("fr-FR", { maximumFractionDigits: 10 }) + ')'
  } else if (a < 0) {
    result = '(' + a.toLocaleString("fr-FR", { maximumFractionDigits: 10 }) + ')'
  } else { // ne pas mettre de parenthèses pour 0
    result = '0'
  }
  return result
}

function ecritureNombreRelatifWithSign (a) {
  let result = ''
  if (a > 0) {
    result = '+' + a.toLocaleString("fr-FR", { maximumFractionDigits: 10 }) 
  } else if (a < 0) {
    result =  + a.toLocaleString("fr-FR", { maximumFractionDigits: 10 })
  } else { // ne pas mettre de parenthèses pour 0
    result = '+0'
  }
  return result
}

function egal (a, b, tolerance = 0.00001) {
  return (Math.abs(a - b) < tolerance)
}

function enleveElement (array, item) {
  for (let i = array.length - 1; i >= 0; i--) {
    if (typeof item === 'number') {
      if (egal(array[i], item)) {
        array.splice(i, 1)
      }
    } else {
      if (array[i] === item) {
        array.splice(i, 1)
      }
    }
  }
}

function rangeMinMax (min, max, listeAEviter = [], step = 1) {
  // Créer un tableau avec toutes les valeurs de 0 à max sauf celle de la liste à éviter
  const liste = []
  for (let i = min; i <= max; i = i + step) {
    liste.push(i)
  }
  for (let i = 0; i < listeAEviter.length; i++) {
    enleveElement(liste, listeAEviter[i])
  }
  return liste
}

function choice (liste, listeAEviter = []) {
  // copie la liste pour ne pas y toucher (ce n'est pas le but de choice)
  if (!Array.isArray(listeAEviter)) {
    listeAEviter = [listeAEviter]
  }
  const listebis = liste.slice()
  // Supprime les éléments de liste à éviter
  for (let i = 0; i < listeAEviter.length; i++) {
    enleveElement(listebis, listeAEviter[i])
  }
  const index = Math.floor(Math.random() * listebis.length)
  return listebis[index]
}

function ecritureAlgebrique (a, {parentheses = false, signeplus = true, precision = 10} ={}) {
  if (a >= 0) {
    return (parentheses? '(' : '') + (signeplus? '+' : '') + a.toLocaleString("fr-FR", { maximumFractionDigits: precision }) + (parentheses? ')' : '')
  } else {
	return (parentheses? '(' : '') + a.toLocaleString("fr-FR", { maximumFractionDigits: precision }) + (parentheses? ')' : '')
  }
}

function listeDesDiviseurs (n) {
  let k = 2
  const liste = [1]
  while (k <= n) {
    if (n % k === 0) {
      liste.push(k)
    }
    k++
  }
  return liste
}

String.prototype.replaceAll = function(target, replacement) {
	return this.split(target).join(replacement);
};