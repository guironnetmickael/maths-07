(function() {
	var
	// Obtain a reference to the canvas element
	// using its id.
	htmlCanvas = document.getElementById('c'),
  	// Obtain a graphics context on the
  	// canvas element for drawing.
  	context = htmlCanvas.getContext('2d'),
	container = $(c).parent();
	

window.canvasp = window.canvasp || {};
canvasp.gdivision = 5.0;
canvasp.gselected = 7.0;
canvasp.xunite = 5;
canvasp.xselected = 4;
canvasp.index = 2;

// zoom options
canvasp.cameraZoom = 1
canvasp.MAX_ZOOM = 200
canvasp.MIN_ZOOM = 1
canvasp.SCROLL_SENSITIVITY = 0.5
 
// Start listening to resize events and
// draw canvas.
initialize();

function adjustZoom(zoomAmount, zoomFactor){
	console.log('zoomAmount' + zoomAmount)
	console.log('zoomFactor' + zoomFactor)
    // if (!isDragging) {
        if (zoomAmount){
            canvasp.cameraZoom += zoomAmount
        } else if (zoomFactor) {
            console.log(zoomFactor)
            canvasp.cameraZoom = zoomFactor*lastZoom
        }        
        canvasp.cameraZoom = Math.min( canvasp.cameraZoom, canvasp.MAX_ZOOM )
        canvasp.cameraZoom = Math.max( canvasp.cameraZoom, canvasp.MIN_ZOOM )        
        console.log(canvasp.cameraZoom)
		resizeCanvas();
    // }
}
 
function initialize() {
	// Register an event listener to
	// call the resizeCanvas() function each time
	// the window is resized.
	window.addEventListener('resize', resizeCanvas, false);
	
	htmlCanvas.addEventListener( 'wheel', (e) => adjustZoom(e.deltaY*canvasp.SCROLL_SENSITIVITY))
	
	// Draw canvas border for the first time.
	resizeCanvas();
}
// Display custom canvas.
// In this case it's a blue, 5 pixel border that
// resizes along with the browser window.
function redraw(value) {
	// context.scale(canvasp.cameraZoom, canvasp.cameraZoom)
	context.strokeStyle = 'blue';
	context.lineWidth = '5';
	//context.strokeRect(0, 0, window.innerWidth, window.innerHeight);
	context.strokeRect(0, 0, htmlCanvas.width,htmlCanvas.height);	
	var centerX = htmlCanvas.width / 2;
	var centerY = htmlCanvas.height / 2;
	context.lineWidth = 5;
	switch (value) {
		case 1:
			var radius = Math.min(htmlCanvas.width,htmlCanvas.height) / 3;
			gateau(context,centerX,centerY,radius,canvasp.gselected,canvasp.gdivision);	
			break; 
		case 2:			
			var width = htmlCanvas.width / 2  + canvasp.cameraZoom - 1 ;
			var height = htmlCanvas.height / 3;
			var xleft = centerX-width/2;
			var yleft = centerY-height/2;
			axe(context,xleft,yleft,width ,height,canvasp.xselected,canvasp.xunite);
			break;		
		default: 
			text = "Looking forward to the Weekend";
	}
}

// Runs each time the DOM window resize event fires.
// Resets the canvas dimensions to match window,
// then draws the new borders accordingly.
function resizeCanvas() {
	htmlCanvas.width = $(container).width();
	if (htmlCanvas.width/3<200){
		htmlCanvas.height = 200 ; 
	}else{
		htmlCanvas.height = $(container).width()/3;
	}
	redraw(canvasp.index);
}


function gateau(context,centerX,centerY,radius,selected,division){
	var multiply = Math.floor(Math.random() * 2) + 1 ;
	if (selected>division){
		cercle(context,centerX-radius-5,centerY,radius,division*multiply,division*multiply);
		cercle(context,centerX+radius+5,centerY,radius,selected*multiply-division*multiply,division*multiply);
	}else{
		cercle(context,centerX,centerY,radius,selected*multiply,division*multiply);
	}
}

function cercle(context,centerX,centerY,radius,selected,division)	{
	context.beginPath();
	context.lineWidth = 2;			
	context.strokeStyle = '#003300';
	//var division = 5.0;
	//var selected = 3.0;
	var rotated = Math.floor(Math.random() * division) + 1 ;
	context.arc(centerX, centerY, radius, (0+rotated) * Math.PI*2/division, (selected+rotated)* Math.PI*2/division, false);
	context.moveTo(centerX,centerY);
	context.lineTo(centerX+radius*Math.cos((0+rotated)* Math.PI*2/division),centerY+radius*Math.sin((0+rotated)* Math.PI*2/division));
	context.lineTo(centerX+radius*Math.cos((selected+rotated)* Math.PI*2/division),centerY+radius*Math.sin((selected+rotated)* Math.PI*2/division));
	context.lineTo(centerX,centerY);
	context.fillStyle = 'green';
	context.fill();
	
	context.beginPath();
	for (var i = 0; i < division && division>1; i++) {	
		context.moveTo(centerX+radius*Math.cos((i+1)* Math.PI*2/division),centerY+radius*Math.sin((i+1)* Math.PI*2/division));
		context.lineTo(centerX,centerY);	
	}
	context.arc(centerX, centerY, radius, 0,  Math.PI*2, false);
	context.stroke();
	
	//context.lineWidth = 1;
	//canvas_arrow(context, centerX-radius, centerY, centerX+radius, centerY,textR);
	//context.stroke();	
}


function axe (context,xleft,yleft,width,height,selected,division){
	//var selected = 4;
	//var division = 5;
	var unite = width/20;
	var hauteur = 10 ;
	
	context.beginPath();
	context.strokeStyle = '#003300';
	context.lineWidth = 3;
	//axe
	context.moveTo(xleft, yleft);
	context.lineTo(xleft+unite*20, yleft);
	context.stroke();
	canvas_arrow(context, xleft+unite*20, yleft, xleft+unite*20, yleft,null,undefined);
	
	
	context.fillStyle = 'green';
	context.fill();	
	//codage 1er
	for (var i=0;i<20;i++){
		context.moveTo(xleft+unite*i, yleft);
		context.lineTo(xleft+unite*i, yleft+hauteur);
		context.lineTo(xleft+unite*i, yleft-hauteur);
	}
	context.stroke();
	context.font = "30px serif";
	context.fillStyle = "#ff0000"; // red
		 
	// set alignment of text at writing point (left-align)
	context.textAlign = "center";
	context.fillText("0", xleft, yleft+6*hauteur );	
	context.fillText("1", xleft+unite*division, yleft+6*hauteur);
	context.fillText("A", xleft+unite*selected, yleft+6*hauteur);
}

function canvas_arrow(context, fromx, fromy, tox, toy,text,arrow_start){
    var headlen = 10;   // length of head in pixels
	
    var angle = Math.atan2(toy-fromy,tox-fromx);
	context.lineWidth = 1;
    context.moveTo(fromx, fromy);
    context.lineTo(tox, toy);
    context.lineTo(tox-headlen*Math.cos(angle-Math.PI/6),toy-headlen*Math.sin(angle-Math.PI/6));
    context.moveTo(tox, toy);
    context.lineTo(tox-headlen*Math.cos(angle+Math.PI/6),toy-headlen*Math.sin(angle+Math.PI/6));
	context.lineTo(tox, toy);
	
	if (arrow_start!=undefined){
		context.moveTo(fromx, fromy);
		context.lineTo(fromx+headlen*Math.cos(angle-Math.PI/6),fromy+headlen*Math.sin(angle-Math.PI/6));
		context.moveTo(fromx, fromy);
		context.lineTo(fromx+headlen*Math.cos(angle+Math.PI/6),fromy+headlen*Math.sin(angle+Math.PI/6));
		context.stroke();
	}
	
	if (text!=null){
	
		// start by saving the current context (current orientation, origin)
		context.save();
		context.lineWidth = 1; 
		// when we rotate we will be pinching the
		// top-left hand corner with our thumb and finger
		context.translate( (fromx+tox)/2,(fromy+toy)/2 );
		 
		// now rotate the canvas anti-clockwise by 90 degrees
		// holding onto the translate point
		if (angle>=Math.PI/2){
			context.rotate( -1*angle );
		}else{
			context.rotate( angle );
		}
		 
		// specify the font and colour of the text
		context.font = "30px serif";
		context.fillStyle = "#ff0000"; // red
		 
		// set alignment of text at writing point (left-align)
		context.textAlign = "center";
		 
		// write the text
		context.fillText(text, 0, 0 );
		 
		// now restore the canvas flipping it back to its original orientation		
		context.restore();
	}
}


canvasp.resizeCanvas = resizeCanvas;
canvasp.gateau = gateau;
canvasp.axe = axe;

})();
