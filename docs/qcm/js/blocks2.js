
'use strict';

//goog.require('Blockly');
//goog.require('Blockly.JavaScript');

Blockly.BlockSvg.START_HAT = true;
 
Blockly.Blocks['start2'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("Démonstration");
    this.setInputsInline(true);
    this.setNextStatement(true, null);
    this.setColour(50);
    this.setTooltip('Démonstration');
    this.setHelpUrl('');
  }
};

Blockly.JavaScript['start2'] = function(block) {
  // TODO: Assemble JavaScript into code variable.
  var code = '...;\n';
  return code;
};

Blockly.Blocks['droites'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(new Blockly.FieldDropdown([["(d1)","D1"], ["(d2)","D2"], ["(d3)","D3"]]), "droite");
    this.setInputsInline(true);
    this.setOutput(true, null);
    this.setColour(195);
    this.setTooltip('Droites');
    this.setHelpUrl('');
  }
};

Blockly.JavaScript['droites'] = function(block) {
  var dropdown_droite = block.getFieldValue('droite');
  // TODO: Assemble JavaScript into code variable.
  var code = '...';
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.JavaScript.ORDER_NONE];
};

Blockly.Blocks['perp'] = {
  init: function() {
    this.appendValueInput("D1")
        .setCheck(null);
    this.appendValueInput("D2")
        .setCheck(null)
        .appendField("⊥");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(190);
    this.setTooltip('perpendiculaire');
    this.setHelpUrl('');
	this.contextMenu = false;
  }
};

Blockly.JavaScript['perp'] = function(block) {
  var value_d1 = Blockly.JavaScript.valueToCode(block, 'D1', Blockly.JavaScript.ORDER_ATOMIC);
  var value_d2 = Blockly.JavaScript.valueToCode(block, 'D2', Blockly.JavaScript.ORDER_ATOMIC);
  // TODO: Assemble JavaScript into code variable.
  var code = '...;\n';
  return code;
};



Blockly.Blocks['parallel'] = {
  init: function() {
    this.appendValueInput("D1")
        .setCheck(null);
    this.appendValueInput("D2")
        .setCheck(null)
        .appendField("//");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(190);
    this.setTooltip('parallèle');
    this.setHelpUrl('');
	this.contextMenu = false;
  }/*,
  customContextMenu: function(options) {
    console.log(options);
    var option={};
    option.enabled = true;
    option.text = 'Create Function';
    option.callback = popupFunction;
    options.push(option);
    console.log(options);
  }*/
};

function popupFunction() {
  console.log('create function clicked!');
}


Blockly.JavaScript['parallel'] = function(block) {
  var value_d1 = Blockly.JavaScript.valueToCode(block, 'D1', Blockly.JavaScript.ORDER_ATOMIC);
  var value_d2 = Blockly.JavaScript.valueToCode(block, 'D2', Blockly.JavaScript.ORDER_ATOMIC);
  // TODO: Assemble JavaScript into code variable.
  var code = '...;\n';
  return code;
};

Blockly.Blocks['prop1'] = {
  init: function() {
    //this.appendDummyInput()
    //    .appendField("Si deux droites sont perpendiculaires à une même droite alors elles sont parallèles entre elles.");
	this.appendDummyInput()
        .appendField("Si deux droites sont perpendiculaires");
	this.appendDummyInput()
        .appendField("à une même droite");
    this.appendDummyInput()
        .appendField("alors elles sont parallèles entre elles");
    this.setInputsInline(false);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(30);
    this.setTooltip('Si deux droites sont perpendiculaires à une même droite alors elles sont parallèles entre elles.');
    this.setHelpUrl('');
	this.contextMenu = false;
  }
};

Blockly.JavaScript['prop1'] = function(block) {
  // TODO: Assemble JavaScript into code variable.
  var code = '...;\n';
  return code;
};

Blockly.Blocks['prop2'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("Si deux droites sont parallèles");
	this.appendDummyInput()
        .appendField("à une même droite");
	this.appendDummyInput()
        .appendField("alors elles sont parallèles entre elles");
    this.setInputsInline(false);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(30);
    this.setTooltip('Si deux droites sont parallèles à une même droite alors elles sont parallèles entre elles.');
    this.setHelpUrl('');
	this.contextMenu = false;
  }
};

Blockly.JavaScript['prop2'] = function(block) {
  // TODO: Assemble JavaScript into code variable.
  var code = '...;\n';
  return code;
};

Blockly.Blocks['prop3'] = {
  init: function() {
    this.appendDummyInput()
        .appendField("Si deux droites sont parallèles et qu'");
	this.appendDummyInput()
        .appendField("une troisième est perpendiculaire");
	this.appendDummyInput()
        .appendField(" à l'une alors cette troisième");
	this.appendDummyInput()
        .appendField("est perpendiculaire à l'autre.");
    this.setInputsInline(false);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(30);
    this.setTooltip('Si deux droites sont parallèles et qu\'une troisième est perpendiculaire à l\'une alors cette troisième droite est perpendiculaire à l\'autre.');
    this.setHelpUrl('');
	this.contextMenu = false;
  }
};

Blockly.JavaScript['prop3'] = function(block) {
  // TODO: Assemble JavaScript into code variable.
  var code = '...;\n';
  return code;
};

Blockly.Blocks['know'] = {
  init: function() {
    this.appendStatementInput("sait")
        .setCheck(null)
        .appendField("On sait que");
    this.appendStatementInput("prop")
        .setCheck(null)
        .appendField("Propriété");
    this.appendStatementInput("concl")
        .setCheck(null)
        .appendField("Conclusion:");
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(150);
    this.setTooltip('On sait que');
    this.setHelpUrl('');
  }
};

Blockly.JavaScript['know'] = function(block) {
  var statements_sait = Blockly.JavaScript.statementToCode(block, 'sait');
  var statements_prop = Blockly.JavaScript.statementToCode(block, 'prop');
  var statements_concl = Blockly.JavaScript.statementToCode(block, 'concl');
  // TODO: Assemble JavaScript into code variable.
  var code = '...;\n';
  return code;
};

Blockly.Blocks['and'] = {
  init: function() {
    this.appendStatementInput("P1")
        .setCheck(null);
    this.appendDummyInput()
        .appendField("ET");
    this.appendStatementInput("P2")
        .setCheck(null);
    this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(150);
    this.setTooltip('ET logique');
    this.setHelpUrl('');
  }
};

Blockly.JavaScript['and'] = function(block) {
  var statements_p1 = Blockly.JavaScript.statementToCode(block, 'P1');
  var statements_p2 = Blockly.JavaScript.statementToCode(block, 'P2');
  // TODO: Assemble JavaScript into code variable.
  var code = '...;\n';
  return code;
};

function checkStart(block){	
	var child = block.getNextBlock();
	var error = 0;
	if (child===null){
		console.log("error");
		$('.modal-body>p').html('Manque la structure de la démonstration');
	    $('#myModal').modal('show');
		error++;
	}else{		
		if (child.type==="know"){			
			var sait= child.getInputTargetBlock('sait');
			var prop= child.getInputTargetBlock('prop');
			var concl= child.getInputTargetBlock('concl');
			if (sait===null || prop===null || concl===null ){
				console.log("Schema incomplet");
				$('.modal-body>p').html('Schéma incomplet ');
				$('#myModal').modal('show');
				error++;
			}else{
				if (prop.getNextBlock()===null && prop.type==="prop1"){
					console.log("prop OK");
				}else{					
					console.log("error prop");
					$('.modal-body>p').html('Mauvaise propriété');
					$('#myModal').modal('show');
					error++;
					return error;
				}
				var D1 = concl.getInputTargetBlock('D1');
				var D2 = concl.getInputTargetBlock('D2');
				if (concl.getNextBlock()===null && concl.type==="parallel" && D1!==null && D2!==null ){
					if ((D1.getFieldValue('droite')==='D2' && D2.getFieldValue('droite') === 'D3') || (D1.getFieldValue('droite')==='D3' && D2.getFieldValue('droite') === 'D2')){
						console.log("concl OK");
					}else{
						console.log("error concl droite");
						$('.modal-body>p').html('Propriété correcte <br/> Conclusion incorrecte');
					    $('#myModal').modal('show');
						error++;
						return error;
					}
				}else{
					$('.modal-body>p').html('Propriété correcte <br/> Conclusion incomplète ou incorrecte');
					$('#myModal').modal('show');
					error++;
					return error;
				}
				var P1 = sait.getInputTargetBlock('P1')
				var P2 = sait.getInputTargetBlock('P2')
				if (sait.getNextBlock()===null &&  sait.type==="and" && P1!==null && P2!==null && P1.type==="perp" && P2.type === "perp"  && P1.getNextBlock()===null &&P2.getNextBlock()===null){
					var P1D1 = P1.getInputTargetBlock('D1');
					var P1D2 = P1.getInputTargetBlock('D2');
					var P2D1 = P2.getInputTargetBlock('D1');
					var P2D2 = P2.getInputTargetBlock('D2');
					if (P2D2===null || P2D1===null || P1D2===null || P1D1===null){
						console.log("error condition droite");
						error++;
						$('.modal-body>p').html('Propriété correcte <br/> Conclusion correcte  <br/> Condition erronée : droites manquantes');
						$('#myModal').modal('show');						
						return error;
					}else if (
					   ((P1D1.getFieldValue('droite')==='D2' && P1D2.getFieldValue('droite') === 'D1') || (P1D1.getFieldValue('droite')==='D1' && P1D2.getFieldValue('droite') === 'D2'))&&
					   ((P2D1.getFieldValue('droite')==='D3' && P2D2.getFieldValue('droite') === 'D1') || (P2D1.getFieldValue('droite')==='D1' && P2D2.getFieldValue('droite') === 'D3'))
					   ){
						console.log("condition OK");
					}else if(
					   ((P1D1.getFieldValue('droite')==='D3' && P1D2.getFieldValue('droite') === 'D1') || (P1D1.getFieldValue('droite')==='D1' && P1D2.getFieldValue('droite') === 'D3'))&&
					   ((P2D1.getFieldValue('droite')==='D2' && P2D2.getFieldValue('droite') === 'D1') || (P2D1.getFieldValue('droite')==='D1' && P2D2.getFieldValue('droite') === 'D2'))
					   ){
						console.log("condition OK");
					}else{
						console.log("error condition droite");
						error++;
						$('.modal-body>p').html('Propriété correcte <br/> Conclusion correcte  <br/> Condition erronée : droites à modifier');
						$('#myModal').modal('show');
						return error;	
					}
				}else{
					console.log("error condition");
					error++;
					if (sait.type!=="and"){
						$('.modal-body>p').html('Propriété correcte <br/> Conclusion correcte  <br/> Condition erronée : bloc ET manquant (il faut deux conditions)');
						$('#myModal').modal('show');
						return error;
					}else if (sait.getNextBlock()!==null){
						$('.modal-body>p').html('Propriété correcte <br/> Conclusion correcte  <br/> Condition erronée : bloc non nécessaire');
						$('#myModal').modal('show');
						return error;
					}else if (P1===null || P2===null){
						$('.modal-body>p').html('Propriété correcte <br/> Conclusion correcte  <br/> Condition erronée : blocs manquants');
						$('#myModal').modal('show');
						return error;
					}else if (P1.type!=="perp" || P2.type !== "perp"){
						$('.modal-body>p').html('Propriété correcte <br/> Conclusion correcte  <br/> Condition erronée : Mauvaise condition');
						$('#myModal').modal('show');
						return error;						
					}					
				}
			}
		}else{
			$('.modal-body>p').html('Manque la structure de la démonstration: Cond Prop Concl');
	        $('#myModal').modal('show');
			console.log("error2");
			error++;
		}		
	}
	return error;
}

function checkStart2(block){	
	var child = block.getNextBlock();
	var error = 0;
	if (child===null){
		console.log("error");
		$('.modal-body>p').html('Manque la structure de la démonstration');
	    $('#myModal').modal('show');
		error++;
	}else{		
		if (child.type==="know"){			
			var sait= child.getInputTargetBlock('sait');
			var prop= child.getInputTargetBlock('prop');
			var concl= child.getInputTargetBlock('concl');
			if (sait===null || prop===null || concl===null ){
				console.log("Schema incomplet");
				$('.modal-body>p').html('Schéma incomplet ');
				$('#myModal').modal('show');
				error++;
			}else{
				if (prop.getNextBlock()===null && prop.type==="prop3"){
					console.log("prop OK");
				}else{					
					console.log("error prop");
					$('.modal-body>p').html('Mauvaise propriété');
					$('#myModal').modal('show');
					error++;
					return error;
				}
				var D1 = concl.getInputTargetBlock('D1');
				var D2 = concl.getInputTargetBlock('D2');
				if (concl.getNextBlock()===null && concl.type==="perp" && D1!==null && D2!==null ){
					if ((D1.getFieldValue('droite')==='D1' && D2.getFieldValue('droite') === 'D3') || (D1.getFieldValue('droite')==='D3' && D2.getFieldValue('droite') === 'D1')){
						console.log("concl OK");
					}else{
						console.log("error concl droite");
						$('.modal-body>p').html('Propriété correcte <br/> Conclusion incorrecte');
					    $('#myModal').modal('show');
						error++;
						return error;
					}
				}else{
					$('.modal-body>p').html('Propriété correcte <br/> Conclusion incomplète ou incorrecte');
					$('#myModal').modal('show');
					error++;
					return error;
				}
				var P1 = sait.getInputTargetBlock('P1')
				var P2 = sait.getInputTargetBlock('P2')
				if (sait.getNextBlock()===null &&  sait.type==="and" && P1!==null && P2!==null && ((P1.type==="perp" && P2.type === "parallel")||((P2.type==="perp" && P1.type === "parallel")))  && P1.getNextBlock()===null &&P2.getNextBlock()===null){
					var P1D1 = P1.getInputTargetBlock('D1');
					var P1D2 = P1.getInputTargetBlock('D2');
					var P2D1 = P2.getInputTargetBlock('D1');
					var P2D2 = P2.getInputTargetBlock('D2');
					if (P2D2===null || P2D1===null || P1D2===null || P1D1===null){
						console.log("error condition droite");
						error++;
						$('.modal-body>p').html('Propriété correcte <br/> Conclusion correcte  <br/> Condition erronée : droites manquantes');
						$('#myModal').modal('show');						
						return error;
					}else if (
					   ((P1.type==="perp" && P1D1.getFieldValue('droite')==='D1' && P1D2.getFieldValue('droite') === 'D2') || ( P1.type==="perp" && P1D1.getFieldValue('droite')==='D2' && P1D2.getFieldValue('droite') === 'D1'))&&
					   ((P2.type==="parallel" && P2D1.getFieldValue('droite')==='D3' && P2D2.getFieldValue('droite') === 'D2') || (P2.type==="parallel" && P2D1.getFieldValue('droite')==='D2' && P2D2.getFieldValue('droite') === 'D3'))
					   ){
						console.log("condition OK");
					}else if(
					   ((P1.type==="parallel" && P1D1.getFieldValue('droite')==='D3' && P1D2.getFieldValue('droite') === 'D2') || (P1.type==="parallel" && P1D1.getFieldValue('droite')==='D2' && P1D2.getFieldValue('droite') === 'D3'))&&
					   ((P2.type==="perp" && P2D1.getFieldValue('droite')==='D2' && P2D2.getFieldValue('droite') === 'D1') || (P2.type==="perp" && P2D1.getFieldValue('droite')==='D1' && P2D2.getFieldValue('droite') === 'D2'))
					   ){
						console.log("condition OK");
					}else{
						console.log("error condition droite");
						error++;
						$('.modal-body>p').html('Propriété correcte <br/> Conclusion correcte  <br/> Condition erronée : droites à modifier');
						$('#myModal').modal('show');
						return error;	
					}
				}else{
					console.log("error condition");
					error++;
					if (sait.type!=="and"){
						$('.modal-body>p').html('Propriété correcte <br/> Conclusion correcte  <br/> Condition erronée : bloc ET manquant (il faut deux conditions)');
						$('#myModal').modal('show');
						return error;
					}else if (sait.getNextBlock()!==null){
						$('.modal-body>p').html('Propriété correcte <br/> Conclusion correcte  <br/> Condition erronée : bloc non nécessaire');
						$('#myModal').modal('show');
						return error;
					}else if (P1===null || P2===null){
						$('.modal-body>p').html('Propriété correcte <br/> Conclusion correcte  <br/> Condition erronée : blocs manquants');
						$('#myModal').modal('show');
						return error;
					}else if (P1.type!=="perp" || P2.type !== "perp"){
						$('.modal-body>p').html('Propriété correcte <br/> Conclusion correcte  <br/> Condition erronée : Mauvaise condition');
						$('#myModal').modal('show');
						return error;						
					}					
				}
			}
		}else{
			$('.modal-body>p').html('Manque la structure de la démonstration: Cond Prop Concl');
	        $('#myModal').modal('show');
			console.log("error2");
			error++;
		}		
	}
	return error;
}

function checkStart3(block){	
	var child = block.getNextBlock();
	var error = 0;
	if (child===null){
		console.log("error");
		$('.modal-body>p').html('Manque la structure de la démonstration');
	    $('#myModal').modal('show');
		error++;
	}else{		
		if (child.type==="know"){			
			var sait= child.getInputTargetBlock('sait');
			var prop= child.getInputTargetBlock('prop');
			var concl= child.getInputTargetBlock('concl');
			if (sait===null || prop===null || concl===null ){
				console.log("Schema incomplet");
				$('.modal-body>p').html('Schéma incomplet ');
				$('#myModal').modal('show');
				error++;
			}else{
				if (prop.getNextBlock()===null && prop.type==="prop2"){
					console.log("prop OK");
				}else{					
					console.log("error prop");
					$('.modal-body>p').html('Mauvaise propriété');
					$('#myModal').modal('show');
					error++;
					return error;
				}
				var D1 = concl.getInputTargetBlock('D1');
				var D2 = concl.getInputTargetBlock('D2');
				if (concl.getNextBlock()===null && concl.type==="parallel" && D1!==null && D2!==null ){
					if ((D1.getFieldValue('droite')==='D1' && D2.getFieldValue('droite') === 'D3') || (D1.getFieldValue('droite')==='D3' && D2.getFieldValue('droite') === 'D1')){
						console.log("concl OK");
					}else{
						console.log("error concl droite");
						$('.modal-body>p').html('Propriété correcte <br/> Conclusion incorrecte');
					    $('#myModal').modal('show');
						error++;
						return error;
					}
				}else{
					$('.modal-body>p').html('Propriété correcte <br/> Conclusion incomplète ou incorrecte');
					$('#myModal').modal('show');
					error++;
					return error;
				}
				var P1 = sait.getInputTargetBlock('P1')
				var P2 = sait.getInputTargetBlock('P2')
				if (sait.getNextBlock()===null &&  sait.type==="and" && P1!==null && P2!==null && P2.type === "parallel" && P1.type === "parallel"  && P1.getNextBlock()===null &&P2.getNextBlock()===null){
					var P1D1 = P1.getInputTargetBlock('D1');
					var P1D2 = P1.getInputTargetBlock('D2');
					var P2D1 = P2.getInputTargetBlock('D1');
					var P2D2 = P2.getInputTargetBlock('D2');
					if (P2D2===null || P2D1===null || P1D2===null || P1D1===null){
						console.log("error condition droite");
						error++;
						$('.modal-body>p').html('Propriété correcte <br/> Conclusion correcte  <br/> Condition erronée : droites manquantes');
						$('#myModal').modal('show');						
						return error;
					}else if (
					   ((P1.type==="parallel" && P1D1.getFieldValue('droite')==='D1' && P1D2.getFieldValue('droite') === 'D2') || ( P1.type==="parallel" && P1D1.getFieldValue('droite')==='D2' && P1D2.getFieldValue('droite') === 'D1'))&&
					   ((P2.type==="parallel" && P2D1.getFieldValue('droite')==='D3' && P2D2.getFieldValue('droite') === 'D2') || (P2.type==="parallel" && P2D1.getFieldValue('droite')==='D2' && P2D2.getFieldValue('droite') === 'D3'))
					   ){
						console.log("condition OK");
					}else if(
					   ((P1.type==="parallel" && P1D1.getFieldValue('droite')==='D3' && P1D2.getFieldValue('droite') === 'D2') || (P1.type==="parallel" && P1D1.getFieldValue('droite')==='D2' && P1D2.getFieldValue('droite') === 'D3'))&&
					   ((P2.type==="parallel" && P2D1.getFieldValue('droite')==='D2' && P2D2.getFieldValue('droite') === 'D1') || (P2.type==="parallel" && P2D1.getFieldValue('droite')==='D1' && P2D2.getFieldValue('droite') === 'D2'))
					   ){
						console.log("condition OK");
					}else{
						console.log("error condition droite");
						error++;
						$('.modal-body>p').html('Propriété correcte <br/> Conclusion correcte  <br/> Condition erronée : droites à modifier');
						$('#myModal').modal('show');
						return error;	
					}
				}else{
					console.log("error condition");
					error++;
					if (sait.type!=="and"){
						$('.modal-body>p').html('Propriété correcte <br/> Conclusion correcte  <br/> Condition erronée : bloc ET manquant (il faut deux conditions)');
						$('#myModal').modal('show');
						return error;
					}else if (sait.getNextBlock()!==null){
						$('.modal-body>p').html('Propriété correcte <br/> Conclusion correcte  <br/> Condition erronée : bloc non nécessaire');
						$('#myModal').modal('show');
						return error;
					}else if (P1===null || P2===null){
						$('.modal-body>p').html('Propriété correcte <br/> Conclusion correcte  <br/> Condition erronée : blocs manquants');
						$('#myModal').modal('show');
						return error;
					}else if (P1.type!=="perp" || P2.type !== "perp"){
						$('.modal-body>p').html('Propriété correcte <br/> Conclusion correcte  <br/> Condition erronée : Mauvaise condition');
						$('#myModal').modal('show');
						return error;						
					}					
				}
			}
		}else{
			$('.modal-body>p').html('Manque la structure de la démonstration: Cond Prop Concl');
	        $('#myModal').modal('show');
			console.log("error2");
			error++;
		}		
	}
	return error;
}

Blockly.checkAnswers3 = function(workspace) {
	
  var xml = Blockly.Xml.workspaceToDom(workspace);
  var xml_text = Blockly.Xml.domToPrettyText(xml);
  var blocks = workspace.getAllBlocks();
  var error = 1;
  var found = 0;
  for (var b = 0, block; block = blocks[b]; b++) {
	console.log(block.type);
	if (block.type==="start2"){
		found = 1;
		error = checkStart3(block);
		break;
	}
  }
  if (found === 0){
	  $('.modal-body>p').html('Manque le bloc : démonstration');
	  $('#myModal').modal('show');
  }
  var resu = document.getElementById('result');
  if (error===0){
	 resu.innerHTML = "&nbsp;&nbsp;BRAVO!&nbsp;&nbsp;"+"<img src='images/awards.png' alt='félicitations' style='width:64px;'/>"; 
  }else{
	 resu.innerHTML = "nombre d'erreurs:"+error;  
  }
  
  
};

Blockly.checkAnswers2 = function(workspace) {
	
  var xml = Blockly.Xml.workspaceToDom(workspace);
  var xml_text = Blockly.Xml.domToPrettyText(xml);
  var blocks = workspace.getAllBlocks();
  var error = 1;
  var found = 0;
  for (var b = 0, block; block = blocks[b]; b++) {
	console.log(block.type);
	if (block.type==="start2"){
		found = 1;
		error = checkStart2(block);
		break;
	}
  }
  if (found === 0){
	  $('.modal-body>p').html('Manque le bloc : démonstration');
	  $('#myModal').modal('show');
  }
  var resu = document.getElementById('result');
  if (error===0){
	 resu.innerHTML = "&nbsp;&nbsp;BRAVO!&nbsp;&nbsp;"+"<img src='images/awards.png' alt='félicitations' style='width:64px;'/>"; 
  }else{
	 resu.innerHTML = "nombre d'erreurs:"+error;  
  }
  
  
};

Blockly.checkAnswers = function(workspace) {
	
  var xml = Blockly.Xml.workspaceToDom(workspace);
  var xml_text = Blockly.Xml.domToPrettyText(xml);
  var blocks = workspace.getAllBlocks();
  var error = 1;
  var found = 0;
  for (var b = 0, block; block = blocks[b]; b++) {
	console.log(block.type);
	if (block.type==="start2"){
		found = 1;
		error = checkStart(block);
		break;
	}
  }
  if (found === 0){
	  $('.modal-body>p').html('Manque le bloc : démonstration');
	  $('#myModal').modal('show');
  }
  var resu = document.getElementById('result');
  if (error===0){
	 resu.innerHTML = "&nbsp;&nbsp;BRAVO!&nbsp;&nbsp;"+"<img src='images/awards.png' alt='félicitations' style='width:64px;'/>"; 
  }else{
	 resu.innerHTML = "nombre d'erreurs:"+error;  
  }
  
  
};
