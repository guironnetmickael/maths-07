$(document).ready(function() {
  $('body').append('<div class="modal fade" id="modal"></div>');
  $("a.secure").click(function () { $(this).connexion(); return false;});
//  $("form#secure #_submit").click(function () { $(this).formEnvoi(); return false;});
//  $('form .checkbox  input[type="checkbox"]').click(function () { $(this).good_result(); return false;});
  $.fn.message_cookies_detect();
  $('.alert-cookies .close-cookies').click(function () { $(this).message_cookies_close(); return false;});

  $(".dropdown-fullwidth").click( function () { $(this).dropdownFullWidth()});

$("a.share-popup").click(function(){
  var C="";
  var B=this.href;
  var D=B.split("/")[2];
  switch(D){case"www.facebook.com":C="width=585,height=368";break;
            case"www.twitter.com":C="width=585,height=261";break;
            case"plus.google.com":C="width=517,height=511";break;
            default:C="width=585,height=511"}
  window.open(B,"","menubar=no,toolbar=no,resizable=yes,scrollbars=yes,"+C);
  return false
});


// contactez-nous 
$('.feedback_new').click(function () { $(this).toModal(); return false;});

});

// taille du menu 
$( window ).resize(function() { 
  setTimeout(function () { $(".dropdown-fullwidth.open").dropdownFullWidth()}, 400);
});

!function ($) {
$.fn.dropdownFullWidthAll = function() {
  fullwidth = $( document ).width();
  $(".dropdown-fullwidth.open").each( function() {
    $(this).dropdownFullWidth();
  }); 

}

$.fn.dropdownFullWidth = function() {
    fullwidth = $( document ).width();
    if(fullwidth < 435){
	  $(this).children('.dropdown-menu').css("width", fullwidth-10);
      p=$(this).position();
	  if (p){
		$(this).children('.dropdown-menu').css("margin-left", -(p.left-20));
	  }
    }
    else
    {
      $(".dropdown-fullwidth").each( function() {
        $(this).children('.dropdown-menu').removeAttr('style');
      });
    }
}


// contactez-nous 
$.fn.toModal = function() {
  $.ajax({
    type        : 'GET',
    url         : $(this).attr('href'),
    success     : function(data) {
      $('#modal').html(data).modal({backdrop:"static"}).modal('show');
    }
  });
}




$.fn.DoughnutChart = function (canvas, moduleData) { ///
    var helpers = Chart.helpers;

		var moduleDoughnut = new Chart(canvas.getContext('2d')).Doughnut(moduleData, { tooltipTemplate : "<%= value %>%", animation: true, responsive: true, showTooltips: true  });
		var legendHolder = document.createElement('div');
		legendHolder.innerHTML = moduleDoughnut.generateLegend();
		// Include a html legend template after the module doughnut itself
		helpers.each(legendHolder.firstChild.childNodes, function(legendNode, index){
			helpers.addEvent(legendNode, 'mouseover', function(){
				var activeSegment = moduleDoughnut.segments[index];
				activeSegment.save();
				activeSegment.fillColor = activeSegment.highlightColor;
        moduleDoughnut.showTooltip([activeSegment]);
				activeSegment.restore();
			});
		});
		helpers.addEvent(legendHolder.firstChild, 'mouseout', function(){
			moduleDoughnut.draw();
		});
		canvas.parentNode.parentNode.appendChild(legendHolder.firstChild);

}


$.fn.set_cookie = function (sName, sValue) {
        var today = new Date(), expires = new Date();
        expires.setTime(today.getTime() + (365*24*60*60*1000));
        document.cookie = sName + "=" + encodeURIComponent(sValue) + ";expires=" + expires.toGMTString() + "; path=/";
    
}
$.fn.get_cookie = function (sName) {
        var oRegex = new RegExp("(?:; )?" + sName + "=([^;]*);?");
 
        if (oRegex.test(document.cookie)) {
                return decodeURIComponent(RegExp["$1"]);
        } else {
                return null;
        }
}

$.fn.message_cookies_close = function () {
  $.fn.set_cookie("QYSMCC",1); 
  $(".alert-cookies").alert('close');
}

$.fn.message_cookies_detect = function () {
  if ($.fn.get_cookie("QYSMCC") === null)
  {
    $(".alert-cookies").show();
  }
  return 
}





//formulaire de question

$.fn.good_result = function (control_group, type) { ///
  clicked = $(this); 
  var openMixSwitch = 'on';
  if(type=='single')
  {  
    $('.' + control_group + ' .collection-items .collection-item').each(function(){
      $(this).removeClass('goodresult');
      kids = $(this).children('div').children('div').children('div').children('div').children('input');
      if(clicked.attr('id') == kids.attr('id'))
      {
        if(kids.prop('checked') )
        {
          $(this).addClass('goodresult');
          openMixSwitch = 'off';
        }
      }
      else
      {
        if(clicked.prop('checked') )
        {
          $(this).children('div').children('div').children('div').children('div').children('input').removeAttr('checked');
          openMixSwitch = 'off';
        }
      }    
    });
  }
  if(type=='multi')
  {  
    $('.' + control_group + ' .collection-items .collection-item').each(function(){

      $(this).removeClass('goodresult');
      kids = $(this).children('div').children('div').children('div').children('div').children('input');
      if(kids.prop('checked') )
      {
        $(this).addClass('goodresult');
        openMixSwitch = 'off';
      }
        
    });
  }
  open_mix_initialize(openMixSwitch);
} 

$.fn.good_result_initialize = function (control_group) { ///
  var openMixSwitch = 'on';
  $('.' + control_group + ' .collection-items .collection-item').each(function(){

    $(this).removeClass('goodresult');
    kids = $(this).children('div').children('div').children('div').children('div').children('input');
    if(kids.prop('checked') )
    {
        $(this).addClass('goodresult');
        openMixSwitch = 'off';
    }
        
  });
  open_mix_initialize(openMixSwitch);
}


/*
$.fn.open_reponse = function () { ///
  $('#question_open_reponse_switch').val(0); 
  $.fn.open_reponse_action();
} 

$.fn.open_reponse_after_delete = function () { ///
  clicked = $(this); 
  var nbr_reponse = 0;
  reponse_group = clicked.parent().parent().parent().children('div');
  reponse_group.each(function(){ 
    nbr_reponse++;
  }); 
  if (nbr_reponse == 1)
  {  
    $('#question_open_reponse_switch').val(1); 
    $.fn.open_reponse_action();
  }
} 

$.fn.open_reponse_initialyse = function () { 
  var nbr_reponse = 0;
  $('.reponse_predefinie').children('.collection-item').each(function( index ) {
nbr_reponse = nbr_reponse + 1;
});
if (nbr_reponse == 0)
  {
    $('#question_open_reponse_switch').val(1)	;
  }
  else 
  {
    $('#question_open_reponse_switch').val(0);
  }

    $.fn.open_reponse_action();
}

$.fn.open_reponse_action = function () { 
  if($('#question_open_reponse_switch').val() == 0)
  {
    $('.reponse_predefinie').show();
    $('.alert-goodresult').show();
    $('.reponse_libre').hide();
  }
  else
  {
    $('.reponse_predefinie').hide();
    $('.alert-goodresult').hide();
    $('.reponse_libre').show();
  }
}
*/
} ( jQuery );




    




//Theme test
!function ($) {



$.fn.themeGestionColor = function () { ///
  color = $(this).val();
  $('#'+ $(this).attr('id') + '_show').css('color', color);

  if ($(this).attr('id')=='theme_background_color')
  {
    $('.panel-theme').css('background-color', color);
    $('.rating-smile span.active').css('color', color);
    color_border = $.Color( color );
    r = color_border.red(); g = color_border.green(); b = color_border.blue();
    color_border = color_border.rgba( r - 30, g - 30, b - 30, 255 );
    $('.panel-theme').css('border-color', color_border.toHexString());

  }
  if ($(this).attr('id')=='theme_font_color')
  {
    $('.panel-theme').css('color', color);
  }
  if ($(this).attr('id')=='theme_button_color')
  {
    color_border = $.Color( color );
    r = color_border.red(); g = color_border.green(); b = color_border.blue();
    color_border = color_border.rgba( r - 30, g - 30, b - 30, 255 );
    $('.panel-theme .btn').css('background-color', color); 
    $('.panel-theme .slider-handle').css('background-color', color); 
    $('.panel-theme .btn').css('border-color', color_border.toHexString());
    $('.panel-theme .slider-handle').css('border-color', color_border.toHexString());
  }
  if ($(this).attr('id')=='theme_progress_color')
  {
    color_border = $.Color( color );
    r = color_border.red(); g = color_border.green(); b = color_border.blue();
    color_border = color_border.rgba( r - 30, g - 30, b - 30, 255 );
    $('.panel-theme .quizz-progress-bar').css('background-color', color); 
    $('.panel-theme .quizz-progress-bar').css('border-color', color_border.toHexString());
  }
  if ($(this).attr('id')=='theme_progress_font_color')
  {
    $('.panel-theme .quizz-progress-bar').css('color', color);
  }
  if ($(this).attr('id')=='theme_button_font_color')
  {
    color_opacity = $.Color( color );
    r = color_opacity.red(); g = color_opacity.green(); b = color_opacity.blue();
    color_opacity = color_opacity.rgba( r, g, b, 100 );
    $('.panel-theme .debut.btn').css('color', color); 
    $('.panel-theme .btn span').css('color', color); 
    $('.panel-theme .btn i').css('color', color_opacity.toHexString(true)); 
  }
  if ($(this).attr('id')=='theme_validity_color')
  {
    color_border = $.Color( color );
    r = color_border.red(); g = color_border.green(); b = color_border.blue();
    $('.panel-theme .single .btn.active i.fa-check').css('color', color); 
    if (color_border.toHexString() == '#ffffff')
    {
      $('.panel-theme .single .open_in_mix i.fa-check').css('color', color_border.rgba( 225, 225, 225, 255 )); 
    }
    else
    {
      $('.panel-theme .single .open_in_mix i.fa-check').css('color', color); 
    }
  }
  if ($(this).attr('id')=='theme_smile_color')
  {
    $('.rating span.fa-star, .rating span.fa-star-o').css('color', color);
    $('.rating span.fa-heart-o, .rating span.fa-heart').css('color', color);
    $('.rating-smile span.inactive').css('color', color);
    $('.rating-smile span.active').css('background', color);
  } 
} 
  
} ( jQuery );


/**
* bootstrap-colorpalette.js
* (c) 2013~ Jiung Kang
* Licensed under the Apache License, Version 2.0 (the "License");
*/

(function($) {
  "use strict";
  var aaColor = [
    ['#000000', '#424242', '#636363', '#9C9C94', '#CEC6CE', '#EFEFEF', '#F7F7F7', '#FFFFFF'],
    ['#FF0000', '#FF9C00', '#FFFF00', '#00FF00', '#00FFFF', '#0000FF', '#9C00FF', '#FF00FF'],
    ['#F7C6CE', '#FFE7CE', '#FFEFC6', '#D6EFD6', '#CEDEE7', '#CEE7F7', '#D6D6E7', '#E7D6DE'],
    ['#E79C9C', '#FFC69C', '#FFE79C', '#B5D6A5', '#A5C6CE', '#9CC6EF', '#B5A5D6', '#D6A5BD'],
    ['#E76363', '#F7AD6B', '#FFD663', '#94BD7B', '#73A5AD', '#6BADDE', '#8C7BC6', '#C67BA5'],
    ['#CE0000', '#E79439', '#EFC631', '#6BA54A', '#4A7B8C', '#3984C6', '#634AA5', '#A54A7B'],
    ['#9C0000', '#B56308', '#BD9400', '#397B21', '#104A5A', '#085294', '#311873', '#731842'],
    ['#630000', '#7B3900', '#846300', '#295218', '#083139', '#003163', '#21104A', '#4A1031']
  ];

  var createPaletteElement = function(element, _aaColor) {
    element.addClass('bootstrap-colorpalette');
    var aHTML = [];
    $.each(_aaColor, function(i, aColor){
      aHTML.push('<div>');
      $.each(aColor, function(i, sColor) {
        var sButton = ['<button type="button" class="btn-color" style="background-color:', sColor,
          '" data-value="', sColor,
          '" title="', sColor,
          '"></button>'].join('');
        aHTML.push(sButton);
      });
      aHTML.push('</div>');
    });
    element.html(aHTML.join(''));
  };

  var attachEvent = function(palette) {
    palette.element.on('click', function(e) {
      var welTarget = $(e.target),
          welBtn = welTarget.closest('.btn-color');

      if (!welBtn[0]) { return; }

      var value = welBtn.attr('data-value');
      palette.value = value;
      palette.element.trigger({
        type: 'selectColor',
        color: value,
        element: palette.element
      });
    });
  };

  var Palette = function(element, options) {
    this.element = element;
    createPaletteElement(element, options && options.colors || aaColor);
    attachEvent(this);
  };

  $.fn.extend({
    colorPalette : function(options) {
      this.each(function () {
        var $this = $(this),
            data = $this.data('colorpalette');
        if (!data) {
          $this.data('colorpalette', new Palette($this, options));
        }
      });
      return this;
    }
  });
})(jQuery);


/*!
 * jQuery Color Animations v2.1.2
 * https://github.com/jquery/jquery-color
 *
 * Copyright 2013 jQuery Foundation and other contributors
 * Released under the MIT license.
 * http://jquery.org/license
 *
 * Date: Wed Jan 16 08:47:09 2013 -0600
 */
(function( jQuery, undefined ) {

	var stepHooks = "backgroundColor borderBottomColor borderLeftColor borderRightColor borderTopColor color columnRuleColor outlineColor textDecorationColor textEmphasisColor",

	// plusequals test for += 100 -= 100
	rplusequals = /^([\-+])=\s*(\d+\.?\d*)/,
	// a set of RE's that can match strings and generate color tuples.
	stringParsers = [{
			re: /rgba?\(\s*(\d{1,3})\s*,\s*(\d{1,3})\s*,\s*(\d{1,3})\s*(?:,\s*(\d?(?:\.\d+)?)\s*)?\)/,
			parse: function( execResult ) {
				return [
					execResult[ 1 ],
					execResult[ 2 ],
					execResult[ 3 ],
					execResult[ 4 ]
				];
			}
		}, {
			re: /rgba?\(\s*(\d+(?:\.\d+)?)\%\s*,\s*(\d+(?:\.\d+)?)\%\s*,\s*(\d+(?:\.\d+)?)\%\s*(?:,\s*(\d?(?:\.\d+)?)\s*)?\)/,
			parse: function( execResult ) {
				return [
					execResult[ 1 ] * 2.55,
					execResult[ 2 ] * 2.55,
					execResult[ 3 ] * 2.55,
					execResult[ 4 ]
				];
			}
		}, {
			// this regex ignores A-F because it's compared against an already lowercased string
			re: /#([a-f0-9]{2})([a-f0-9]{2})([a-f0-9]{2})/,
			parse: function( execResult ) {
				return [
					parseInt( execResult[ 1 ], 16 ),
					parseInt( execResult[ 2 ], 16 ),
					parseInt( execResult[ 3 ], 16 )
				];
			}
		}, {
			// this regex ignores A-F because it's compared against an already lowercased string
			re: /#([a-f0-9])([a-f0-9])([a-f0-9])/,
			parse: function( execResult ) {
				return [
					parseInt( execResult[ 1 ] + execResult[ 1 ], 16 ),
					parseInt( execResult[ 2 ] + execResult[ 2 ], 16 ),
					parseInt( execResult[ 3 ] + execResult[ 3 ], 16 )
				];
			}
		}, {
			re: /hsla?\(\s*(\d+(?:\.\d+)?)\s*,\s*(\d+(?:\.\d+)?)\%\s*,\s*(\d+(?:\.\d+)?)\%\s*(?:,\s*(\d?(?:\.\d+)?)\s*)?\)/,
			space: "hsla",
			parse: function( execResult ) {
				return [
					execResult[ 1 ],
					execResult[ 2 ] / 100,
					execResult[ 3 ] / 100,
					execResult[ 4 ]
				];
			}
		}],

	// jQuery.Color( )
	color = jQuery.Color = function( color, green, blue, alpha ) {
		return new jQuery.Color.fn.parse( color, green, blue, alpha );
	},
	spaces = {
		rgba: {
			props: {
				red: {
					idx: 0,
					type: "byte"
				},
				green: {
					idx: 1,
					type: "byte"
				},
				blue: {
					idx: 2,
					type: "byte"
				}
			}
		},

		hsla: {
			props: {
				hue: {
					idx: 0,
					type: "degrees"
				},
				saturation: {
					idx: 1,
					type: "percent"
				},
				lightness: {
					idx: 2,
					type: "percent"
				}
			}
		}
	},
	propTypes = {
		"byte": {
			floor: true,
			max: 255
		},
		"percent": {
			max: 1
		},
		"degrees": {
			mod: 360,
			floor: true
		}
	},
	support = color.support = {},

	// element for support tests
	supportElem = jQuery( "<p>" )[ 0 ],

	// colors = jQuery.Color.names
	colors,

	// local aliases of functions called often
	each = jQuery.each;

// determine rgba support immediately
supportElem.style.cssText = "background-color:rgba(1,1,1,.5)";
support.rgba = supportElem.style.backgroundColor.indexOf( "rgba" ) > -1;

// define cache name and alpha properties
// for rgba and hsla spaces
each( spaces, function( spaceName, space ) {
	space.cache = "_" + spaceName;
	space.props.alpha = {
		idx: 3,
		type: "percent",
		def: 1
	};
});

function clamp( value, prop, allowEmpty ) {
	var type = propTypes[ prop.type ] || {};

	if ( value == null ) {
		return (allowEmpty || !prop.def) ? null : prop.def;
	}

	// ~~ is an short way of doing floor for positive numbers
	value = type.floor ? ~~value : parseFloat( value );

	// IE will pass in empty strings as value for alpha,
	// which will hit this case
	if ( isNaN( value ) ) {
		return prop.def;
	}

	if ( type.mod ) {
		// we add mod before modding to make sure that negatives values
		// get converted properly: -10 -> 350
		return (value + type.mod) % type.mod;
	}

	// for now all property types without mod have min and max
	return 0 > value ? 0 : type.max < value ? type.max : value;
}

function stringParse( string ) {
	var inst = color(),
		rgba = inst._rgba = [];

	string = string.toLowerCase();

	each( stringParsers, function( i, parser ) {
		var parsed,
			match = parser.re.exec( string ),
			values = match && parser.parse( match ),
			spaceName = parser.space || "rgba";

		if ( values ) {
			parsed = inst[ spaceName ]( values );

			// if this was an rgba parse the assignment might happen twice
			// oh well....
			inst[ spaces[ spaceName ].cache ] = parsed[ spaces[ spaceName ].cache ];
			rgba = inst._rgba = parsed._rgba;

			// exit each( stringParsers ) here because we matched
			return false;
		}
	});

	// Found a stringParser that handled it
	if ( rgba.length ) {

		// if this came from a parsed string, force "transparent" when alpha is 0
		// chrome, (and maybe others) return "transparent" as rgba(0,0,0,0)
		if ( rgba.join() === "0,0,0,0" ) {
			jQuery.extend( rgba, colors.transparent );
		}
		return inst;
	}

	// named colors
	return colors[ string ];
}

color.fn = jQuery.extend( color.prototype, {
	parse: function( red, green, blue, alpha ) {
		if ( red === undefined ) {
			this._rgba = [ null, null, null, null ];
			return this;
		}
		if ( red.jquery || red.nodeType ) {
			red = jQuery( red ).css( green );
			green = undefined;
		}

		var inst = this,
			type = jQuery.type( red ),
			rgba = this._rgba = [];

		// more than 1 argument specified - assume ( red, green, blue, alpha )
		if ( green !== undefined ) {
			red = [ red, green, blue, alpha ];
			type = "array";
		}

		if ( type === "string" ) {
			return this.parse( stringParse( red ) || colors._default );
		}

		if ( type === "array" ) {
			each( spaces.rgba.props, function( key, prop ) {
				rgba[ prop.idx ] = clamp( red[ prop.idx ], prop );
			});
			return this;
		}

		if ( type === "object" ) {
			if ( red instanceof color ) {
				each( spaces, function( spaceName, space ) {
					if ( red[ space.cache ] ) {
						inst[ space.cache ] = red[ space.cache ].slice();
					}
				});
			} else {
				each( spaces, function( spaceName, space ) {
					var cache = space.cache;
					each( space.props, function( key, prop ) {

						// if the cache doesn't exist, and we know how to convert
						if ( !inst[ cache ] && space.to ) {

							// if the value was null, we don't need to copy it
							// if the key was alpha, we don't need to copy it either
							if ( key === "alpha" || red[ key ] == null ) {
								return;
							}
							inst[ cache ] = space.to( inst._rgba );
						}

						// this is the only case where we allow nulls for ALL properties.
						// call clamp with alwaysAllowEmpty
						inst[ cache ][ prop.idx ] = clamp( red[ key ], prop, true );
					});

					// everything defined but alpha?
					if ( inst[ cache ] && jQuery.inArray( null, inst[ cache ].slice( 0, 3 ) ) < 0 ) {
						// use the default of 1
						inst[ cache ][ 3 ] = 1;
						if ( space.from ) {
							inst._rgba = space.from( inst[ cache ] );
						}
					}
				});
			}
			return this;
		}
	},
	is: function( compare ) {
		var is = color( compare ),
			same = true,
			inst = this;

		each( spaces, function( _, space ) {
			var localCache,
				isCache = is[ space.cache ];
			if (isCache) {
				localCache = inst[ space.cache ] || space.to && space.to( inst._rgba ) || [];
				each( space.props, function( _, prop ) {
					if ( isCache[ prop.idx ] != null ) {
						same = ( isCache[ prop.idx ] === localCache[ prop.idx ] );
						return same;
					}
				});
			}
			return same;
		});
		return same;
	},
	_space: function() {
		var used = [],
			inst = this;
		each( spaces, function( spaceName, space ) {
			if ( inst[ space.cache ] ) {
				used.push( spaceName );
			}
		});
		return used.pop();
	},
	transition: function( other, distance ) {
		var end = color( other ),
			spaceName = end._space(),
			space = spaces[ spaceName ],
			startColor = this.alpha() === 0 ? color( "transparent" ) : this,
			start = startColor[ space.cache ] || space.to( startColor._rgba ),
			result = start.slice();

		end = end[ space.cache ];
		each( space.props, function( key, prop ) {
			var index = prop.idx,
				startValue = start[ index ],
				endValue = end[ index ],
				type = propTypes[ prop.type ] || {};

			// if null, don't override start value
			if ( endValue === null ) {
				return;
			}
			// if null - use end
			if ( startValue === null ) {
				result[ index ] = endValue;
			} else {
				if ( type.mod ) {
					if ( endValue - startValue > type.mod / 2 ) {
						startValue += type.mod;
					} else if ( startValue - endValue > type.mod / 2 ) {
						startValue -= type.mod;
					}
				}
				result[ index ] = clamp( ( endValue - startValue ) * distance + startValue, prop );
			}
		});
		return this[ spaceName ]( result );
	},
	blend: function( opaque ) {
		// if we are already opaque - return ourself
		if ( this._rgba[ 3 ] === 1 ) {
			return this;
		}

		var rgb = this._rgba.slice(),
			a = rgb.pop(),
			blend = color( opaque )._rgba;

		return color( jQuery.map( rgb, function( v, i ) {
			return ( 1 - a ) * blend[ i ] + a * v;
		}));
	},
	toRgbaString: function() {
		var prefix = "rgba(",
			rgba = jQuery.map( this._rgba, function( v, i ) {
				return v == null ? ( i > 2 ? 1 : 0 ) : v;
			});

		if ( rgba[ 3 ] === 1 ) {
			rgba.pop();
			prefix = "rgb(";
		}

		return prefix + rgba.join() + ")";
	},
	toHslaString: function() {
		var prefix = "hsla(",
			hsla = jQuery.map( this.hsla(), function( v, i ) {
				if ( v == null ) {
					v = i > 2 ? 1 : 0;
				}

				// catch 1 and 2
				if ( i && i < 3 ) {
					v = Math.round( v * 100 ) + "%";
				}
				return v;
			});

		if ( hsla[ 3 ] === 1 ) {
			hsla.pop();
			prefix = "hsl(";
		}
		return prefix + hsla.join() + ")";
	},
	toHexString: function( includeAlpha ) {
		var rgba = this._rgba.slice(),
			alpha = rgba.pop();

		if ( includeAlpha ) {
			rgba.push( ~~( alpha * 255 ) );
		}

		return "#" + jQuery.map( rgba, function( v ) {

			// default to 0 when nulls exist
			v = ( v || 0 ).toString( 16 );
			return v.length === 1 ? "0" + v : v;
		}).join("");
	},
	toString: function() {
		return this._rgba[ 3 ] === 0 ? "transparent" : this.toRgbaString();
	}
});
color.fn.parse.prototype = color.fn;

// hsla conversions adapted from:
// https://code.google.com/p/maashaack/source/browse/packages/graphics/trunk/src/graphics/colors/HUE2RGB.as?r=5021

function hue2rgb( p, q, h ) {
	h = ( h + 1 ) % 1;
	if ( h * 6 < 1 ) {
		return p + (q - p) * h * 6;
	}
	if ( h * 2 < 1) {
		return q;
	}
	if ( h * 3 < 2 ) {
		return p + (q - p) * ((2/3) - h) * 6;
	}
	return p;
}

spaces.hsla.to = function ( rgba ) {
	if ( rgba[ 0 ] == null || rgba[ 1 ] == null || rgba[ 2 ] == null ) {
		return [ null, null, null, rgba[ 3 ] ];
	}
	var r = rgba[ 0 ] / 255,
		g = rgba[ 1 ] / 255,
		b = rgba[ 2 ] / 255,
		a = rgba[ 3 ],
		max = Math.max( r, g, b ),
		min = Math.min( r, g, b ),
		diff = max - min,
		add = max + min,
		l = add * 0.5,
		h, s;

	if ( min === max ) {
		h = 0;
	} else if ( r === max ) {
		h = ( 60 * ( g - b ) / diff ) + 360;
	} else if ( g === max ) {
		h = ( 60 * ( b - r ) / diff ) + 120;
	} else {
		h = ( 60 * ( r - g ) / diff ) + 240;
	}

	// chroma (diff) == 0 means greyscale which, by definition, saturation = 0%
	// otherwise, saturation is based on the ratio of chroma (diff) to lightness (add)
	if ( diff === 0 ) {
		s = 0;
	} else if ( l <= 0.5 ) {
		s = diff / add;
	} else {
		s = diff / ( 2 - add );
	}
	return [ Math.round(h) % 360, s, l, a == null ? 1 : a ];
};

spaces.hsla.from = function ( hsla ) {
	if ( hsla[ 0 ] == null || hsla[ 1 ] == null || hsla[ 2 ] == null ) {
		return [ null, null, null, hsla[ 3 ] ];
	}
	var h = hsla[ 0 ] / 360,
		s = hsla[ 1 ],
		l = hsla[ 2 ],
		a = hsla[ 3 ],
		q = l <= 0.5 ? l * ( 1 + s ) : l + s - l * s,
		p = 2 * l - q;

	return [
		Math.round( hue2rgb( p, q, h + ( 1 / 3 ) ) * 255 ),
		Math.round( hue2rgb( p, q, h ) * 255 ),
		Math.round( hue2rgb( p, q, h - ( 1 / 3 ) ) * 255 ),
		a
	];
};


each( spaces, function( spaceName, space ) {
	var props = space.props,
		cache = space.cache,
		to = space.to,
		from = space.from;

	// makes rgba() and hsla()
	color.fn[ spaceName ] = function( value ) {

		// generate a cache for this space if it doesn't exist
		if ( to && !this[ cache ] ) {
			this[ cache ] = to( this._rgba );
		}
		if ( value === undefined ) {
			return this[ cache ].slice();
		}

		var ret,
			type = jQuery.type( value ),
			arr = ( type === "array" || type === "object" ) ? value : arguments,
			local = this[ cache ].slice();

		each( props, function( key, prop ) {
			var val = arr[ type === "object" ? key : prop.idx ];
			if ( val == null ) {
				val = local[ prop.idx ];
			}
			local[ prop.idx ] = clamp( val, prop );
		});

		if ( from ) {
			ret = color( from( local ) );
			ret[ cache ] = local;
			return ret;
		} else {
			return color( local );
		}
	};

	// makes red() green() blue() alpha() hue() saturation() lightness()
	each( props, function( key, prop ) {
		// alpha is included in more than one space
		if ( color.fn[ key ] ) {
			return;
		}
		color.fn[ key ] = function( value ) {
			var vtype = jQuery.type( value ),
				fn = ( key === "alpha" ? ( this._hsla ? "hsla" : "rgba" ) : spaceName ),
				local = this[ fn ](),
				cur = local[ prop.idx ],
				match;

			if ( vtype === "undefined" ) {
				return cur;
			}

			if ( vtype === "function" ) {
				value = value.call( this, cur );
				vtype = jQuery.type( value );
			}
			if ( value == null && prop.empty ) {
				return this;
			}
			if ( vtype === "string" ) {
				match = rplusequals.exec( value );
				if ( match ) {
					value = cur + parseFloat( match[ 2 ] ) * ( match[ 1 ] === "+" ? 1 : -1 );
				}
			}
			local[ prop.idx ] = value;
			return this[ fn ]( local );
		};
	});
});

// add cssHook and .fx.step function for each named hook.
// accept a space separated string of properties
color.hook = function( hook ) {
	var hooks = hook.split( " " );
	each( hooks, function( i, hook ) {
		jQuery.cssHooks[ hook ] = {
			set: function( elem, value ) {
				var parsed, curElem,
					backgroundColor = "";

				if ( value !== "transparent" && ( jQuery.type( value ) !== "string" || ( parsed = stringParse( value ) ) ) ) {
					value = color( parsed || value );
					if ( !support.rgba && value._rgba[ 3 ] !== 1 ) {
						curElem = hook === "backgroundColor" ? elem.parentNode : elem;
						while (
							(backgroundColor === "" || backgroundColor === "transparent") &&
							curElem && curElem.style
						) {
							try {
								backgroundColor = jQuery.css( curElem, "backgroundColor" );
								curElem = curElem.parentNode;
							} catch ( e ) {
							}
						}

						value = value.blend( backgroundColor && backgroundColor !== "transparent" ?
							backgroundColor :
							"_default" );
					}

					value = value.toRgbaString();
				}
				try {
					elem.style[ hook ] = value;
				} catch( e ) {
					// wrapped to prevent IE from throwing errors on "invalid" values like 'auto' or 'inherit'
				}
			}
		};
		jQuery.fx.step[ hook ] = function( fx ) {
			if ( !fx.colorInit ) {
				fx.start = color( fx.elem, hook );
				fx.end = color( fx.end );
				fx.colorInit = true;
			}
			jQuery.cssHooks[ hook ].set( fx.elem, fx.start.transition( fx.end, fx.pos ) );
		};
	});

};

color.hook( stepHooks );

jQuery.cssHooks.borderColor = {
	expand: function( value ) {
		var expanded = {};

		each( [ "Top", "Right", "Bottom", "Left" ], function( i, part ) {
			expanded[ "border" + part + "Color" ] = value;
		});
		return expanded;
	}
};

// Basic color names only.
// Usage of any of the other color names requires adding yourself or including
// jquery.color.svg-names.js.
colors = jQuery.Color.names = {
	// 4.1. Basic color keywords
	aqua: "#00ffff",
	black: "#000000",
	blue: "#0000ff",
	fuchsia: "#ff00ff",
	gray: "#808080",
	green: "#008000",
	lime: "#00ff00",
	maroon: "#800000",
	navy: "#000080",
	olive: "#808000",
	purple: "#800080",
	red: "#ff0000",
	silver: "#c0c0c0",
	teal: "#008080",
	white: "#ffffff",
	yellow: "#ffff00",

	// 4.2.3. "transparent" color keyword
	transparent: [ null, null, null, 0 ],

	_default: "#ffffff"
};

})( jQuery );


(function($) {
  'use strict';
  // PROGRESSBAR CLASS DEFINITION
  // ============================
  var Progressbar = function(element, options) {
    this.$element = $(element);
    this.options = $.extend({}, Progressbar.defaults, options);
  };
  Progressbar.defaults = {
    transition_delay: 300,
    refresh_speed: 50,
    display_text: 'after',
    use_percentage: true,
    percent_format: function(percent) { return percent + '%'; },
    amount_format: function(amount_part, amount_max, amount_min) { return amount_part + ' / ' + amount_max; },
    update: $.noop,
    done: $.noop,
    fail: $.noop
  };

  Progressbar.prototype.transition = function() {
    var $this = this.$element;
    var $parent = $this.parent();
    var $back_text = this.$back_text;
    var $front_text = this.$front_text;
    var options = this.options;
    if (options.display_text == 'after') {
      var data_transitiongoal = parseFloat($this.attr('data-transitiongoal'));
    }else{
      var data_transitiongoal = parseInt($this.attr('data-transitiongoal'));
    }
    var aria_valuemin = parseInt($this.attr('aria-valuemin')) || 0;
    var aria_valuemax = parseInt($this.attr('aria-valuemax')) || 100;
    var is_vertical = $parent.hasClass('vertical');
    var update = options.update && typeof options.update == 'function' ? options.update : Progressbar.defaults.update;
    var done = options.done && typeof options.done == 'function' ? options.done : Progressbar.defaults.done;
    var fail = options.fail && typeof options.fail == 'function' ? options.fail : Progressbar.defaults.fail;
    if (isNaN(data_transitiongoal)) {
      fail('data-transitiongoal not set');
      return;
    }
    if (options.display_text == 'after') {
      var percentage = data_transitiongoal;
    }else{
      var percentage = Math.round(100 * (data_transitiongoal - aria_valuemin) / (aria_valuemax - aria_valuemin));
    }
    if (options.display_text == 'center' && !$back_text && !$front_text) {
      this.$back_text = $back_text = $('<span>').addClass('progressbar-back-text').prependTo($parent);
      this.$front_text = $front_text = $('<span>').addClass('progressbar-front-text').prependTo($this);
      var parent_size;
      if (is_vertical) {
        parent_size = $parent.css('height');
        $back_text.css({height: parent_size, 'line-height': parent_size});
        $front_text.css({height: parent_size, 'line-height': parent_size});
        $(window).resize(function() {
          parent_size = $parent.css('height');
          $back_text.css({height: parent_size, 'line-height': parent_size});
          $front_text.css({height: parent_size, 'line-height': parent_size});
        }); // normal resizing would brick the structure because width is in px
      }
      else {
        parent_size = $parent.css('width');
        $front_text.css({width: parent_size});
        $(window).resize(function() {
          parent_size = $parent.css('width');
          $front_text.css({width: parent_size});
        }); // normal resizing would brick the structure because width is in px
      }
    }
    setTimeout(function() {
      var current_percentage;
      var current_value;
      var this_size;
      var parent_size;
      var text;
      if (is_vertical) {
        $this.css('height', percentage + '%');
      }
      else {
        if (options.display_text == 'after') {
          $this.css('width', percentage*75/100 + '%');
        }
        else{
          $this.css('width', percentage + '%');
        }
      }
      var progress = setInterval(function() {
        if (is_vertical) {
          this_size = $this.height();
          parent_size = $parent.height();
        }
        else {
        if (options.display_text == 'after') {
          this_size = $this.width();
          parent_size = $parent.width()*75/100;
        }
        else{
          this_size = $this.width();
          parent_size = $parent.width();
        }
        }

        if (options.display_text == 'after') {
          current_percentage = Math.round(100/75 * this_size / parent_size * 1000)/10;
        }
        else {
          current_percentage = Math.round(100 * this_size / parent_size);
        }
        current_value = Math.round(aria_valuemin + this_size / parent_size * (aria_valuemax - aria_valuemin));
        if (current_percentage >= percentage) {
          current_percentage = percentage;
            
          current_value = data_transitiongoal;
          done($this);
          clearInterval(progress);
        }
        if (options.display_text != 'none') {
          text = options.use_percentage ? options.percent_format(current_percentage) : options.amount_format(current_value, aria_valuemax, aria_valuemin);
          if (options.display_text == 'fill') {
            $this.text(text);
          }
          else if (options.display_text == 'center') {
            $back_text.text(text);
            $front_text.text(text);
          }
          else if (options.display_text == 'after') {
            $this.parent().children(".progress-bar-text").html('&nbsp;' + text);
          }
        }
        $this.attr('aria-valuenow', current_value);
        update(current_percentage, $this);
      }, options.refresh_speed);
    }, options.transition_delay);
  };
  // PROGRESSBAR PLUGIN DEFINITION
  // =============================
  var old = $.fn.progressbar;
  $.fn.progressbar = function(option) {
    return this.each(function () {
      var $this = $(this);
      var data = $this.data('bs.progressbar');
      var options = typeof option == 'object' && option;
      if (!data) {
        $this.data('bs.progressbar', (data = new Progressbar(this, options)));
      }
      data.transition();
    });
  };
  $.fn.progressbar.Constructor = Progressbar;
  // PROGRESSBAR NO CONFLICT
  // =======================
  $.fn.progressbar.noConflict = function () {
    $.fn.progressbar = old;
    return this;
  };
})(window.jQuery);
