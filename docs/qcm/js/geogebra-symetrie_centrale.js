(function() {

var parameters = {
"id": "ggbApplet",
"width":800,
"height":600,
"showMenuBar":true,
"showAlgebraInput":false,
"showToolBar":true,
"customToolBar":"0 39 | 5 | 18 | 10 | 16",
"showToolBarHelp":false,
"showResetIcon":true,
"enableLabelDrags":false,
"enableShiftDragZoom":true,
"enableRightClick":false,
"errorDialogsActive":false,
"useBrowserForJS":false,
"allowStyleBar":false,
"preventFocus":false,
"showZoomButtons":true,
"capturingThreshold":3,
// add code here to run when the applet starts
"appletOnLoad":function(api){ /* api.evalCommand('Segment((1,2),(3,4))');*/ api.evalCommand('CenterView((0, 0))');},
"showFullscreenButton":false,
"scale":1,
"disableAutoScale":false,
"allowUpscale":true,
"clickToLoad":false,
"appName":"classic",
"showSuggestionButtons":true,
"buttonRounding":0.7,
"buttonShadows":false,
"scaleContainerClass":"containerGgb",
"language":"fr",
// use this instead of ggbBase64 to load a material from geogebra.org
// "material_id":"RHYH3UQ8",
// use this instead of ggbBase64 to load a .ggb file
// "filename":"myfile.ggb",
"ggbBase64":"UEsDBBQACAgIAH2JUFkAAAAAAAAAAAAAAAAXAAAAZ2VvZ2VicmFfZGVmYXVsdHMyZC54bWztml1v4jgUhq9nfoXlq92LQhJIoFXpqDPSait1OtW2Gs2tSUzw1rGzsVNCf/06dkhCIbQNtKBObzDH+CN53uPj44TTL1lEwT1OBOFsBO2OBQFmPg8IC0cwlZOjIfxy9vk0xDzE4wSBCU8iJEfQzVuW/ZTVcW0nr0NxPII+RUIQH4KYIpl3GcGZE0AAMkFOGL9CERYx8vGNP8URuuQ+knqcqZTxSbc7m806ixk7PAm7YSg7mVADqKtlYgSLLydquKVOs55u7liW3f31/dIMf0SYkIj5GAJ1JwGeoJRKob5iiiPMJJDzGI9gzAmTEFA0xnQEr3ML/DFJMP4TgqKTAmTBs8+fTsWUzwAf/4t9VSeTFJf9tNHN26ifv3HKE5CMoOKicNpuD4KxthCNpyj/pptSNMcJuEe0rEGp5L7urWsniAq8aKvm+c4DbH7pF+0ZiTRDICRWAlgdGwIRYxyoaWFxh7bWY66lrY3oc54EAmQjeIWuIJgX5YMpdRPN5oY8FJN69Vo5p0W1nVefdguszwMc4BizQDVaomx/UH4Dyl4ryt5QY86LsSleG/NrQu43QLZeTPkHq7N1Pjx4hx58wf7BobrqOuHeB+EdEl723v5vyVY3MRRF/qnyGB7FFGc7RE8JqzBeaqPE7myTXFhvBX0Ncqs18hyHgSenxL9jWKjszqmNm3/5mwRq7yrQfjrF/7ElkYjSiPhEbgY/SZkvdQgpYH5Lk/s6/V7f2gf/asxd41/L1m1kC7jKyIlUKtmD4WaWAoe5VXK5WdiVK7fL4BzHwHQMTe9duLO32Z15Kmk+1wWT6jiFtZOKlZu5wzi+VZ1/sNsEMZGfqZb9p1mtBM03KeW2Usq1DyLYv1bksd39aLWIUlc/UVIqkKrUfaIuN6jLtk36Y1aX47qVah3lB3teYS+I3WuJbJO0HODu+fKtcQdu5bWLBo7VX4+xMzhgt7pXt8crHj8Ls8oIPvKx5+Rja9JmlEgsCGJPHUPoPKyt6uuFXSowMAqsvSp781W92IXzaJjvaM7gkXL2vpXbTHDpRHFdVlQM7WaGTyj7Tjy/mZ/PWf6UenEiMFZJrt9u/RepUV483mUPITd6IvFpOHOREDMTLwUAmaWnmVt6kgereHGQ2dqe2/rXB9tU6/7qahOSgXPT49w0PHdM0TNF3xRuSaXdQU9rGqsIVEt6HwX2frvTiU52K2Xr29zeY0TLo/ShyfoGKTZLI5zUVv3Vwi7dwzXrXo2X4iUxnxHrFp7QrLugJFBOEhElw5GKDRHK9LEcjQWnqcQ3foIxq96JGceckUBO80QrF8TooT8nJMudwzSa8oQ8cCZLECB38XOqX6Rt7Sztg06zGIiFtFp258aqhDBP0XWj2iO2Qo1VfeporYKs13GGPXvo9qyBPTh2h94zSdvD3ZBeCjKm/llPUa1G19o+yLxIfaeYI/GrZ589q8klrOHA8by+57jHxwPb6w92f/j7q6yoji2H+OROO8xK03Xoe+1OdZT7qageJhurZDJ8Z6cXlGaEEpTMV2fa4UlZ4qxKHG61UXv5f4BIm29FgQ6rS7swVu0du7mZCVHcGIpUBzMJYV+RfxcmPGXB6ua1k1vfe7bUDG3MOcWoCjZfF3bt1e5KerB9uH+19eZPsX835tnS9rU5qhBRrYBLbdReuq5ZAdtsakd7d4V2udCjd4G9eu2jRKVOulv791F38Q+ns/8BUEsHCEsxPloFBQAAhSUAAFBLAwQUAAgICAB9iVBZAAAAAAAAAAAAAAAAFgAAAGdlb2dlYnJhX2phdmFzY3JpcHQuanNlUUFuwjAQvPsVWx+IIyio6jFKJdReKlXiQMXdSTaWUeJEjkOpEA/qO/qx7gbSAlUOjndmZ3bHZe/yYBsHxmQr9+psUDEcBN2WbVthmBeY9UbJX1jGyQXq0dguoF8WxRv/OPRKOvxYZVvMw1jinqMQ5ej1jwCqybbsu9MewmeLkMKficFwor8TMjATYcuhB+5SkKV1VsJkcurkSoemRheui21jL0ppKoO32pkKJTvrCn1QjMRisQCaMm/qVnuEKtKWjqKHsQEwQKWhbTo77ENQTnZMQjBe72z4/hLAy+R1AecR6VRySkNPZZpCbjMSomHWfa0OGzLH/QjHx3jxCER6iWUiAG5fg0TjBK4A3Onqualr7YoTTOjZ9CrIja56XNMijh51CG6gcpx8m1tX4H5VKgqn52Ce4P6B4wG4dOvOQioK2IVo9jCI3DJsxzuqKKNEmmjGigPvKPj7AVBLBwiw2CN9WwEAAHoCAABQSwMEFAAICAgAfYlQWQAAAAAAAAAAAAAAAAwAAABnZW9nZWJyYS54bWztW9tu20Yavk6fYpYL5CqW58xhYqew08MWSOqgToLFFkUxIkcSY4pUScqWu9kH2KfY6zxH3mSfZP+ZIXWwLFmKldROF4jC0xz/7/uPpA++ngwzdG7KKi3yw4B0cIBMHhdJmvcPg3Hd21PB10+/Ouibom+6pUa9ohzq+jAQtuW0H1x1BKH2nh6NDoM401WVxgEaZbq2XQ6DC5oEKE0OgzBhoQlld4+wHtvjIVF7uit7eywxIsIYd0MKHdGkSh/nxY96aKqRjs1pPDBD/byIde1mHNT16PH+/sXFRaddW6co+/v9frczqWAq2FdeHQbNyWMYbqHTBXPNKcZk/+8vnvvh99K8qnUemwDZPY/Tp189OLhI86S4QBdpUg9AQkTJAA1M2h+AFKRSAdq3rUYgipGJ6/TcVNB37tJtuh6OAtdM5/b5A3+Gsul+ApSk52liysMAdxiOOGcCh0IpwWgUoKJMTV43bUkz53472sF5ai78sPbMzcgDVBdF1tV2RCQi9O4dophi9MgeiD9QOEjpH2F/DzN/oP7A/UH4Ntx3574p9224b8MZECKt0m5mDoOeziqQYpr3SkBwel3Vl5lxS2puzARAHsG2qvR3aMww8MiLHe5j/Mj+JPy4fbC/uE81t09iN/EOEbt6d2DIrpu49dsDby6lvwzdgWB/IM1DZf9z8pK33BH7qB2RuVnrcrzlpO2UksvNp6TbbNSvaTblbJcyXJ6SihW7vKVw20mJmBMtzOX+ud/SlOxW2/yYGeWCFn7u2QlVu5ExVxvPyXEU7mSfkpPZnDwKHxHGH4V2VrE8a4gXrF1r6vyRNMfPjv5tLeJU/DdMebDf2v+DRgioGti2jfWozbCyYmFg1JCzhMpZPPsfaOGoqNLpjAOTjaZrcYOn+WhcNwM29+Nh0g5eF9BcZ84BNx2SIj47nq6h6WJ0Vc+PC85r5iK9M1vwoA8OMt01GYQhp1ZCCJ3rzNopN0OvyGvU4uHuHew7b31gxnGWJqnO34BEbFPbCLXOOyLzvpsxP1pcFGVyelmBmNDkH6YsrMRBcy79OSOyIwCKWFsYhegwwrCSYNKl4hyDhl+ueAYW2E1gzk9NXcNWKqQnppqKoV9alsxd/FAdF9ns1qhI8/qZHtXj0oVjQKfSrv0o72fGicVBBqFLfNYtJqdOHlT6sV5djoy1OW4F3f6zIitKZH1kBHrQb45df3Rt7NKmrbBrg12LZgw76M2jAGJ+ac1WSbtNgttp0sppBwy+wBCHtw2ExnlaP28v6jQ+m23VdvhxPOwCVRoCLo5JdjXmwf4VJh2cmTI3medLDmCOi3HlSenncgsZV+alrgdHefKT6YNKvdRW2WsY2jedLTkxcTqEjgu81hbY17BUfzcx/dK0W/Qq5kXrnlr5jUqjk2pgTD2FybN5vpnbTrv8g1qDOXImapjmbpShnvjRajNq21dxmY4sZVEXDNKZmZEySSs7QjK3byuRCrYWWysC0q2tZE8vhx/e1xC2ohgi1xIUBL0BXdHjelCULt7VNTQLrNJmZghtUO0o61g/he7Ihc0WI1R034J5mJpm/9xfmHMb8Lr9Q6srLCa45THS2Wigp6LK9KUp56TU2JyTXq8yNQKhENC4S0BHzT19USRmoY/OAUi3cy9AS6mRMZ6NfkvEJUKXTonn2OKgquxEuBMqIjGLwpATTigXbmJIxwRlMoooFkry0AYwv9vhfMphBWWV3i8nnL87w580dPYyttKeAGsqm7e1MjR54XIeeHIYPPxtXNRPXpWQcyEYo2pg/G1sUDJGcOZIio6On6GHf53oJyOw9CUkfUVZA7jeZqGTjh/Gzb0Ib20mM3SbmW8BsYtx5kzVFYhvMHxtayrEWkosgd5LJya5qtPOLVWmTHsz9w7wvHDx9dRbeZc/RVF3qyIb15Dggq7nbYJrScGwIwGnNyMIluG8WADw+KejNyfoL5vB4LsvobBoRG+jaXcEBj7LVLaBgXtlxI0Z6Pe73jyiIn898masNhDeEFCI5InTipshsz0axPA1+OTjIewgDhaau1RfZ+NFIdyE2QqwlqDqkA2FTzY2fPg6w3eTXCa1YQtcfp2jITTSfTBHDzWEq090r5dC2FNqJ2zYVmXtU229DxiuD+8t/BqB3wUf9+H9hsaomXm9MWrD2ZkagPNLUi8G6HXSdgK80H///R+E76uykM5UXcTm2kKkctoCrmwJ6zVu/vjzu/lP4shpRwocSSojBhmAwE0A0WGKh0qQUCiMuWS7cONrpPnsDkrTmpePCYxAhkQpxSPIySJX1LLs6kgShlJyIUPOQ2Ht+SeV6MntJerlabX2D5XoHu5ELISYUjFKGcGSkSbW5IxiIC5hVFBpnd6tJRoXw6HOE5S7iseLtCyLMpjVFDS2VEWaWPl6qY3r6QNv6f2QzUA30b7psiVUNxhw8slUZA4UsBBgHxSYCBZRgiMcOlAALCB3SKhkmLJIAN8/DyxHq2A52h6Wo/sMixQhxkxyokJJRasqioCfYxEWEdxnXH0eUI5XgXK8PSjH9xcU3gH7xCRlQhEuJY18sow7PIyoVOCAQ8xDm0LvGJSXRXbZL/KrqtJI0mFzPL2gCyZsAa7YVoR9+3i+vZ5eMMjJNsPUr6jFy4+8WIerB2l8lkP8vBiX25O/pUli8qmQtkn+tieC83yWCjS8QoVN8w683mNXpm+vZsJYRfHFrW0d0W+1t219/HiSZqkuL5dKizMFIB3GKCcCJqXgLlTkI+49MEsQdgoVEklDDP7E+wrZiaxXj6gCA4bB718t1G5OEOBwZkOLH3Jb0jSu3rdcBD0zZmSrzyf5q1LnlX39v5h1bA6h/mIhpIxGilBl3/TTCMsmTyA8DENIFjiF9TAVOgTB4AkiRMSUxJSDv7lHCHa/UAQhNBBSsAh+kggFQNI2YqOhAu2EVUQAm8TEYbgnO8x+OwLo0ogre/sugbjo6E4bENc4uuUgZLDeVV0lxmDXhZbrGEF99EF98CE/mhGJiYvS515tmrWUqLIQyGBr9VJK+xawqRp2QhIxzEDRIVuFaNHHikAfSL2wEphGtuAv17BB3kk2nHgeHC3xIN2OB+mflAfyC+HBygT+7XY8eHuvecCu8gBwVSK0KTvjkNGLNkTjHCulpCIywpiGrYMnBNIYFYWcEw7d7h0LGmuwKuM5244LZ//nwj3mwvH6OCHbjgvZveYCva78KmgIESNkY/adQNQUtAUwAW5REUGoLyVvo0YMuR7ch/YcY74uebubbGgsw/ESD4bb8WD45+TB/aPBiipZSwNfGVtiQ02agtivtCmGuRNbCPuVblcEg6F2UgG7hl53oKgF0tjxSj9FdryiRrWiRGWVAbgNDygOuRRhtM7r3bUSh74vkFxbc9pbKjr5t9OqY99vQA5jS05CqqXvO+8wIt17gciKCtKVApKIojZTtOiRSHHKMYHw8G4BsvQhT0aaj3j++caUtZn87F5O/PIv//n3IoBZWs1FfGSzauESejt61XStROl6iV55v8Tm7y59i7tOat80QjsdD3++Irhf0D5iwdTpubd5y6JcfM33ze1EKdjO3tqxjn1VShQJCSPWxvOG7UB0ThjDKoy4tN/Mr35tJ68X642v7dKlqDSbaUJ769uboox5uX77x8p1R5+C7OSrg08KynfbgPLdFwLKrb85+KSQfL8NJN9/EZDs4IuDTSHZn/9bCvd3Tc1fNz/9H1BLBwiZc/1o4goAAKs9AABQSwECFAAUAAgICAB9iVBZSzE+WgUFAACFJQAAFwAAAAAAAAAAAAAAAAAAAAAAZ2VvZ2VicmFfZGVmYXVsdHMyZC54bWxQSwECFAAUAAgICAB9iVBZsNgjfVsBAAB6AgAAFgAAAAAAAAAAAAAAAABKBQAAZ2VvZ2VicmFfamF2YXNjcmlwdC5qc1BLAQIUABQACAgIAH2JUFmZc/1o4goAAKs9AAAMAAAAAAAAAAAAAAAAAOkGAABnZW9nZWJyYS54bWxQSwUGAAAAAAMAAwDDAAAABRIAAAAA",
};
// is3D=is 3D applet using 3D view, AV=Algebra View, SV=Spreadsheet View, CV=CAS View, EV2=Graphics View 2, CP=Construction Protocol, PC=Probability Calculator DA=Data Analysis, FI=Function Inspector, macro=Macros
var views = {'is3D': 0,'AV': 0,'SV': 0,'CV': 0,'EV2': 0,'CP': 0,'PC': 0,'DA': 0,'FI': 0,'macro': 0};
var applet = new GGBApplet(parameters, '5.0', views);
applet.setHTML5Codebase('../activite/GeoGebra/HTML5/5.0/web/', 'true');
window.onload = function() {
	var width = Math.floor($('#page1').width());
	$('.containerGgb').width(width-2);
	$('.containerGgb').height(Math.floor(width*0.75)-2);
	parameters.width = width-2;
	parameters.height = Math.floor(width*0.75)-2;
	applet.inject('ggbApplet');
	window.ggb = true;
	
};

applet.setPreviewImage('data:image/gif;base64,R0lGODlhAQABAAAAADs=','https://www.geogebra.org/images/GeoGebra_loading.png','https://www.geogebra.org/images/applet_play.png');

window.applet = applet || {};

})();
