(function() {
	var
	// Obtain a reference to the canvas element
	// using its id.
	htmlCanvas = document.getElementById('c'),
  	// Obtain a graphics context on the
  	// canvas element for drawing.
  	context = htmlCanvas.getContext('2d'),
	container = $(c).parent();


window.canvasp = window.canvasp || {};
canvasp.radiustext = "5cm";
canvasp.rectxtext = "5cm";
canvasp.rectytext = "4cm";
canvasp.trixtext = "5cm";
canvasp.triytext = "4cm";
canvasp.trihyptext = "c";
canvasp.angb = "";
canvasp.angh = "";
canvasp.maisonxtext = "5cm";
canvasp.maisonytext = "4cm";
canvasp.tablextext = "5cm";
canvasp.tableytext = "4cm";
canvasp.index = 1;
canvasp.zoom = 0;

 
// Start listening to resize events and
// draw canvas.
initialize();

 
function initialize(el) {
	// Register an event listener to
	// call the resizeCanvas() function each time
	// the window is resized.
	if (el!=null){
		htmlCanvas = document.getElementById(el);
		context = htmlCanvas.getContext('2d');
		container = $('#'+el).parent();
		container.on('onresize', resizeCanvas);
	}
	window.addEventListener('resize', resizeCanvas, false);
	// Draw canvas border for the first time.
	resizeCanvas();
}
// Display custom canvas.
// In this case it's a blue, 5 pixel border that
// resizes along with the browser window.
function redraw(value) {
	context.strokeStyle = 'blue';
	context.lineWidth = '5';
	//context.strokeRect(0, 0, window.innerWidth, window.innerHeight);
	context.strokeRect(0, 0, htmlCanvas.width,htmlCanvas.height);	
	var centerX = htmlCanvas.width / 2;
	var centerY = htmlCanvas.height / 2;
	context.lineWidth = 5;
	switch (value) {
		case 1:
			var radius = Math.min(htmlCanvas.width,htmlCanvas.height) / 3;
			cercle(context,centerX,centerY,radius,canvasp.radiustext);	
			break; 
		case 2:			
			var width = htmlCanvas.width / 3 ;
			var height = htmlCanvas.height / 3;
			var xleft = centerX-width/2;
			var yleft = centerY-height/2;
			rectangle(context,xleft,yleft,width,height,canvasp.rectxtext,canvasp.rectytext );
			break;
		case 3:
			var width = htmlCanvas.width / 3   + canvasp.zoom;
			var height = htmlCanvas.height / 2 + canvasp.zoom;
			var xleft = centerX-width/2;
			var yleft = centerY-height/2;
			var texthyp = canvasp.trihyptext;
			if (texthyp === "c"){
				// compute 
				var x = Number(canvasp.trixtext.substring(0,canvasp.trixtext.length-2));
				var y = Number(canvasp.triytext.substring(0,canvasp.triytext.length-2));
				var unit = canvasp.trixtext.substring(canvasp.trixtext.length-2,canvasp.trixtext.length);
				var texthyp = (Math.sqrt(x*x+y*y).toFixed(2))+unit;
			}		
			triangle(context,xleft,yleft,width,height,canvasp.trixtext,canvasp.triytext,texthyp,canvasp.angb,canvasp.angh );
			break; 
		case 4:
			var width = htmlCanvas.width / 2 ;
			var height = 3*htmlCanvas.height / 4;
			var yleft = centerY-height/2;
			var xleft = centerX-width/2;
			var x = Number(canvasp.maisonxtext.substring(0,canvasp.maisonxtext.length-2));
			var y = Number(canvasp.maisonytext.substring(0,canvasp.maisonytext.length-2));
			var unit = canvasp.maisonxtext.substring(canvasp.maisonxtext.length-2,canvasp.maisonxtext.length);
			maison(context,xleft,yleft,width,height,canvasp.maisonxtext,canvasp.maisonytext,(Math.sqrt(x*x+y*y).toFixed(2))+unit);
			break; 
		case 5:
			var width = htmlCanvas.width / 2 ;
			var height = 3*htmlCanvas.height / 4;
			var yleft = centerY-height/2;
			var xleft = centerX-width/2;
			table(context,xleft,yleft,width,height,canvasp.tablextext,canvasp.tableytext);
			break; 
		default: 
			text = "Looking forward to the Weekend";
	}
}

// Runs each time the DOM window resize event fires.
// Resets the canvas dimensions to match window,
// then draws the new borders accordingly.
function resizeCanvas() {
	htmlCanvas.width = $(container).width();
	if (htmlCanvas.width/3<200){
		htmlCanvas.height = 200 ; 
	}else{
		htmlCanvas.height = $(container).width()/3;
	}
	redraw(canvasp.index);
}




function cercle(context,centerX,centerY,radius,textR)	{
	context.beginPath();
	context.lineWidth = 2;
	context.arc(centerX, centerY, radius, 0, 2 * Math.PI, false);
	context.fillStyle = '#c4f1f1';
	context.fill();	
	context.strokeStyle = '#003300';
	context.stroke();	
	context.lineWidth = 1;
	canvas_arrow(context, centerX-radius, centerY, centerX+radius, centerY,textR);
	context.stroke();	
}



function table (context,xleft,yleft,width,height,textx,texty){		
	context.beginPath();
	context.lineWidth = 2;
	context.arc(xleft, yleft+height/2, height/2, Math.PI/2, 3 * Math.PI/2, false);
	context.arc(xleft+width, yleft+height/2, height/2, 3*Math.PI/2, Math.PI/2, false);
	context.fillStyle = '#c4f1f1';
	context.fill();	
	context.strokeStyle = '#003300';
	context.stroke();	
	context.lineWidth = 1;
	canvas_arrow(context, xleft-5, yleft, xleft-5, yleft+height,texty);
	context.stroke();
	rectangle(context,xleft,yleft,width,height,textx,texty);
}

function maison(context,xleft,yleft,width,height,textx,texty,texthyp){
	triangle(context,xleft,yleft,width,height/2,textx,texty,texthyp);		
	rectangle(context,xleft,yleft+height/2,width,height/2,textx,texty);
}


function triangle (context,xleft,yleft,width,height,textx,texty,texthyp,angb,angh){
	context.beginPath();
	context.strokeStyle = '#003300';
	context.lineWidth = 2;
	//triangle 	
	context.moveTo(xleft,yleft);
	context.lineTo(xleft, yleft+height);
	context.lineTo(xleft+width, yleft+height);
	context.moveTo(xleft,yleft);
	context.lineTo(xleft+width, yleft+height);
	context.fillStyle = '#c4f1f1';
	context.fill();			
	context.stroke();
	//codage 4e
	context.moveTo(xleft, yleft + height-10);
	context.lineTo(xleft+10, yleft + height-10);
	context.lineTo(xleft+10, yleft + height);
	context.stroke();
	
	// dimensions
	context.beginPath();
	canvas_arrow(context, xleft, yleft+height+25, xleft + width, yleft+height+25,textx);
	canvas_arrow(context, xleft-5, yleft, xleft-5, yleft+height,texty);
	canvas_arrow(context,  xleft, yleft-5, xleft + width, yleft+height-5, texthyp);
	context.stroke();
	
	
	if (angb!=null &&angb!=="" ){	

		//angle bas droit
		context.moveTo(xleft+width,yleft+height);
		context.arc(xleft+width,yleft+height, 40, Math.PI, Math.PI + Math.atan(height/width),  false); 
		context.stroke();	
		
		context.save();
		context.lineWidth = 1; 
		
		// specify the font and colour of the text
		context.font = (30 + Math.floor(canvasp.zoom/2)).toString() + "px serif";
		context.fillStyle = "#ff0000"; // red
			 
		// set alignment of text at writing point (left-align)
		context.textAlign = "center";
			 
		// write the text
		context.fillText(angb, xleft+width -70, yleft+height - 2 );
			 
		// now restore the canvas flipping it back to its original orientation		
		context.restore();
	}
	
	if (angh!=null &&angh!=="" ){
		//angle haut gauche
		context.moveTo(xleft,yleft);
		context.arc(xleft,yleft, 30, 0.5*  Math.PI, 0.5* Math.PI - Math.atan(width/height),  true);
		context.stroke();
		
		context.save();
		context.lineWidth = 1; 
		
		// specify the font and colour of the text
		context.font = (30 + Math.floor(canvasp.zoom/2)).toString() + "px serif";
		context.fillStyle = "#ff0000"; // red
			 
		// set alignment of text at writing point (left-align)
		context.textAlign = "center";
			 
		// write the text
		context.fillText(angh, xleft + 30, yleft + 45);
			 
		// now restore the canvas flipping it back to its original orientation		
		context.restore();
	}
	
	
}


function rectangle (context,xleft,yleft,width,height,textx,texty){
	context.beginPath();
	context.strokeStyle = '#003300';
	context.lineWidth = 2;
	//rectangle 	
	context.rect(xleft,yleft,width,height);
	/*context.moveTo(xleft,yleft);
	context.lineTo(xleft, yleft+height);
	context.lineTo(xleft+width, yleft+height);
	context.lineTo(xleft+width,yleft);
	context.lineTo(xleft,yleft);*/
	
	context.fillStyle = '#c4f1f1';
	context.fill();	
	//codage 1er
	context.moveTo(xleft, yleft+10);
	context.lineTo(xleft+10, yleft+10);
	context.lineTo(xleft+10, yleft);
	//codage 2e
	context.moveTo(xleft + width, yleft+10);
	context.lineTo(xleft + width-10, yleft+10);
	context.lineTo(xleft + width-10, yleft);
	//codage 3e
	context.moveTo(xleft + width, yleft + height-10);
	context.lineTo(xleft + width-10, yleft + height-10);
	context.lineTo(xleft + width-10, yleft + height);
	//codage 4e
	context.moveTo(xleft, yleft + height-10);
	context.lineTo(xleft+10, yleft + height-10);
	context.lineTo(xleft+10, yleft + height);
	context.stroke();
	
	// dimensions
	context.beginPath();
	canvas_arrow(context, xleft, yleft-5, xleft + width, yleft-5,textx);
	canvas_arrow(context, xleft-5, yleft, xleft-5, yleft+height,texty);
	context.stroke();
}

function canvas_arrow(context, fromx, fromy, tox, toy,text){
    var headlen = 10;   // length of head in pixels
	
    var angle = Math.atan2(toy-fromy,tox-fromx);
	context.lineWidth = 1;
    context.moveTo(fromx, fromy);
    context.lineTo(tox, toy);
    context.lineTo(tox-headlen*Math.cos(angle-Math.PI/6),toy-headlen*Math.sin(angle-Math.PI/6));
    context.moveTo(tox, toy);
    context.lineTo(tox-headlen*Math.cos(angle+Math.PI/6),toy-headlen*Math.sin(angle+Math.PI/6));
	context.lineTo(tox, toy);
	
	context.moveTo(fromx, fromy);
    context.lineTo(fromx+headlen*Math.cos(angle-Math.PI/6),fromy+headlen*Math.sin(angle-Math.PI/6));
    context.moveTo(fromx, fromy);
    context.lineTo(fromx+headlen*Math.cos(angle+Math.PI/6),fromy+headlen*Math.sin(angle+Math.PI/6));
	context.stroke();
	if (text!=null){
	
		// start by saving the current context (current orientation, origin)
		context.save();
		context.lineWidth = 1; 
		// when we rotate we will be pinching the
		// top-left hand corner with our thumb and finger
		context.translate( (fromx+tox)/2,(fromy+toy)/2 );
		 
		// now rotate the canvas anti-clockwise by 90 degrees
		// holding onto the translate point
		if (angle>=Math.PI/2){
			context.rotate( -1*angle );
		}else{
			context.rotate( angle );
		}
		 
		// specify the font and colour of the text
		context.font = (30 + Math.floor(canvasp.zoom/2)).toString() + "px serif";
		context.fillStyle = "#ff0000"; // red
		 
		// set alignment of text at writing point (left-align)
		context.textAlign = "center";
		 
		// write the text
		context.fillText(text,0, -2 );
		 
		// now restore the canvas flipping it back to its original orientation		
		context.restore();
	}
}


canvasp.resizeCanvas = resizeCanvas;
canvasp.table = table;
canvasp.maison = maison;
canvasp.triangle = triangle;
canvasp.rectangle = rectangle;
canvasp.initialize = initialize;

})();
