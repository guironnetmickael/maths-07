---
author: Mickael Guironnet
title: 6e
---


<div class="box ch1">             
<div class="icon">
<div class="image">1</div>
<div class="info">
<h3 class="title">Nombres décimaux</h3>
<p markdown="1">
  <i class="ico"></i><a href="doc/Exercices/1_EX_Nombres_entiers.pdf" target="_blank">Exercices sur les nombres entiers</a>
  (<a href="https://coopmaths.fr/alea/?uuid=0688e&id=6N10&n=5&d=10&s=4&s2=0&s3=3&alea=k0kE&cd=1&uuid=7efdf&id=6N10-1&alea=Eu5P&uuid=34579&id=6N10-3&n=6&d=10&s=1&s2=2&alea=HZ3e&cd=1&uuid=86529&id=6N11-1&alea=UEr6&uuid=29b40&id=6N11-3&alea=R6RN&uuid=3bba9&id=6N11-4&alea=pO9E&uuid=a7aa7&id=6N11-5&alea=WIoT&uuid=dc348&id=6N10-4&n=4&d=10&alea=yDtt&cd=1&v=eleve" target="_blank">lien</a>)<br>
  <i class="ico"></i><a href="doc/Exercices/1_EX_Nombres_decimaux.pdf" target="_blank">Exercices sur les nombres décimaux </a> 
  (<a href="https://coopmaths.fr/alea/?uuid=0688e&id=6N10&n=5&d=10&s=4&s2=0&s3=3&alea=k0kE&cd=1&uuid=7efdf&id=6N10-1&alea=Eu5P&uuid=34579&id=6N10-3&n=6&d=10&s=1&s2=2&alea=HZ3e&cd=1&uuid=86529&id=6N11-1&alea=UEr6&uuid=29b40&id=6N11-3&alea=R6RN&uuid=3bba9&id=6N11-4&alea=pO9E&uuid=a7aa7&id=6N11-5&alea=WIoT&uuid=dc348&id=6N10-4&n=4&d=10&alea=yDtt&cd=1&v=eleve" target="_blank">lien</a>)<br>
   <i class="ico"></i><a href="doc/Exercices/1_EX_Comparaison.pdf" target="_blank">Exercices sur la comparaison de nombres </a> 
  (<a href="https://coopmaths.fr/alea/?uuid=be1e4&id=6N31&n=10&d=10&alea=8bMo&cd=1&cols=2&uuid=3e083&id=6N31-1&alea=Jkrs&uuid=3bba9&id=6N11-4&alea=T8Ax&uuid=a8c21&id=6N31-5&alea=k7fb&z=1.8&v=eleve" target="_blank">lien</a>)<br>
</p>
</div>
<div class="space notdone"><div style="width: 100%;" class="space done"></div></div>
</div>
</div>

<div class="box ch1 cache" markdown="1">
<p markdown="1">
<i class="tes"></i>Évaluation<a href="doc/Evaluations/1_EVA_nombres_decimaux.pdf" target="_blank"> sur les nombres décimaux</a><br>
<i class="tes"></i>Évaluation<a href="doc/Evaluations/2_EVA_nombres_decimaux.pdf" target="_blank"> sur les nombres décimaux</a><br>
<i class="tes"></i>Évaluation<a href="doc/Evaluations/3_EVA_comparer.pdf" target="_blank"> sur la comparaison de nombres décimaux</a><br>
{ data-search-exclude }
</p>
</div>

<div class="box ch2">             
<div class="icon">
<div class="image">2</div>
<div class="info">
<h3 class="title">Les bases de la géométrie</h3>
<p markdown="1">
  <i class="ico"></i><a href="doc/Exercices/2_EX_Bases_de_geomtrie.pdf" target="_blank">Exercices sur les bases de la géométrie</a> (<a href="https://coopmaths.fr/alea/?uuid=8f5d3&id=6G10&n=2&d=10&alea=oqne&cd=1&cols=2&uuid=3dbda&id=6G10-5&alea=fPaZ&uuid=9af23&id=6G10-6&n=1&d=10&s=1&s2=7&alea=HVvH&cd=1&uuid=7ff97&id=6G11&alea=zNJa&uuid=df825&id=6G11-1&alea=P3zE&v=latex" target="_blank">lien</a>)<br>
</p>
</div>
<div class="space notdone"><div style="width: 100%;" class="space done"></div></div>
</div>
</div>

<div class="box ch2 cache" markdown="1">
<p markdown="1">
<i class="tes"></i>Évaluation – Contrôle de cours <a href="doc/Evaluations/4_EVA_bases_géométrie.pdf" target="_blank">sur les bases de la géométrie</a><br>
{ data-search-exclude }
</p>
</div>

<div class="box ch3">             
<div class="icon">
<div class="image">3</div>
<div class="info">
<h3 class="title">Les trois opérations</h3>
<p markdown="1">
  <i class="ico"></i><a href="doc/Exercices/3_EX_3_Opérations.pdf" target="_blank">Exercices sur les 3 opérations</a> (<a href="https://coopmaths.fr/alea/?uuid=32e02&id=6C13-1&n=6&d=10&s=1-2-4-5-7-8&alea=hR9X&cd=1&uuid=01873&id=6C20&n=4&d=10&s=3&s2=2&s3=4&alea=Q2b9&cd=1&cols=2&uuid=52939&id=6C30&n=4&d=10&s=false&s2=2&alea=UZJu&cd=1&cols=2&uuid=2471d&id=6C30-1&n=8&d=10&s=1&s2=false&s3=true&s4=false&alea=vfoo&cd=1&cols=2&uuid=5df6e&id=6C30-5b&n=8&d=10&s=false&s2=3&alea=so61&cd=1&cols=2&uuid=4e89b&id=6C35&alea=6Om9&cols=2&v=" target="_blank">lien</a>)<br>
</p>
</div>
<div class="space notdone"><div style="width: 100%;" class="space done"></div></div>
</div>
</div>

<div class="box ch3 cache" markdown="1">
<p markdown="1">
<i class="tes"></i>Évaluation – Contrôle de cours <a href="doc/Evaluations/5_EVA_3_opérations.pdf" target="_blank">sur les 3 opérations</a><br>
<i class="tes"></i>Évaluation – Contrôle de cours2 <a href="doc/Evaluations/6_EVA_3_opérations-2.pdf" target="_blank">sur les 3 opérations</a><br>
<i class="tes"></i>Évaluation – Contrôle de cours3 <a href="doc/Evaluations/7_EVA_3_opérations-3.pdf" target="_blank">sur les 3 opérations</a><br>
{ data-search-exclude }
</p>
</div>

<div class="box ch4">             
<div class="icon">
<div class="image">4</div>
<div class="info">
<h3 class="title">Cercle et triangle</h3>
<p markdown="1">
  <i class="ico"></i><a href="doc/Exercices/4_EX_4_Cercles_et_triangles.pdf" target="_blank">Exercices sur les cercles et les triangles</a> (<a href="https://coopmaths.fr/alea/?uuid=03b49&id=6G10-4&n=1&d=10&s=3&s2=false&s3=6&alea=FUAD&cd=1&uuid=f4fdd&id=6G21-2&alea=rjLA&cols=2&uuid=b5eaf&id=6G20-2&n=4&d=10&s=1&s2=false&alea=3zmv&cd=1&uuid=e1e64&id=6G21-3&alea=JnCh&cols=2&v=" target="_blank">lien</a>)<br>
</p>
</div>
<div class="space notdone"><div style="width: 100%;" class="space done"></div></div>
</div>
</div>

<div class="box ch3 cache" markdown="1">
<p markdown="1">
<i class="tes"></i>Évaluation – Contrôle de cours <a href="doc/Evaluations/8_EVA_Cercles_triangles.pdf" target="_blank">sur les cercles et les triangles</a><br>
{ data-search-exclude }
</p>
</div>

<div class="box ch5">             
<div class="icon">
<div class="image">5</div>
<div class="info">
<h3 class="title">Périmètre et aire</h3>
<p markdown="1">
  <i class="ico"></i><a href="doc/Exercices/4_EX_Périmètre-et-aires.pdf" target="_blank">Exercices sur les périmètres et les aires</a> (<a href="https://coopmaths.fr/alea/?uuid=3cb1d&id=6M12&n=8&d=10&s=4&s2=true&s3=true&s4=1&alea=VCAn&cd=1&cols=2&uuid=6225c&id=6M23&n=8&d=10&s=4&s2=true&s3=1&s4=true&alea=qaSg&cd=1&cols=2&uuid=83be1&id=6M10&n=4&d=10&s=1-2-3&s2=false&s3=true&s4=false&alea=OQm2&cd=1&cols=2&uuid=5999f&id=6M11-2&alea=4DfO&cols=2&v=eleve&es=0211001" target="_blank">lien</a>)<br>
</p>
</div>
<div class="space notdone"><div style="width: 100%;" class="space done"></div></div>
</div>
</div>

<div class="box ch3 cache" markdown="1">
<p markdown="1">
<i class="tes"></i>Évaluation – Contrôle de cours <a href="doc/Evaluations/9_EVA_Périmètre_et_aire.pdf" target="_blank">sur périmètre et aire</a><br>
{ data-search-exclude }
</p>
</div>


