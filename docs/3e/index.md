---
author: Mickael Guironnet
title: 3e
---


<div class="box ch1">             
<div class="icon">
<div class="image">1</div>
<div class="info">
<h3 class="title">Calcul numérique</h3>
<p markdown="1">
  <i class="ico"></i><a href="doc/Exercices/1_RE_Calcul_numerique.pdf" target="_blank">Exercices sur le calcul numérique</a> - Fractions et puissances 
  (<a href="https://coopmaths.fr/alea/?uuid=0b020&id=4C10-7&n=10&d=10&s=true&s2=5&s3=10&alea=jNp5&cd=1&cols=2&uuid=f7f49&id=4C23-11&n=6&d=10&s=7&s2=false&alea=PuoE&cd=1&cols=2&uuid=55354&id=4C22-2&n=4&d=10&s=2&alea=RBdP&cd=1&cols=2&uuid=f5dcf&id=4C30&n=6&d=10&s=1&s2=4&s3=false&alea=lSdy&cd=1&cols=2&uuid=93df9&id=4C30-2&n=6&d=10&s=3&alea=tmSp&cd=1&cols=2&uuid=bae57&id=4C33-1&n=6&d=10&s=1-2-3&s2=1&alea=SUto&cd=1&cols=2&uuid=5d72b&id=4C32-0&alea=wsOq&cols=2&uuid=a0d16&id=4C32&n=4&d=10&s=1&s2=3&alea=j9eG&cd=1&cols=2&uuid=a0d16&id=4C32&n=4&d=10&s=2&s2=3&alea=mU4w&cd=1&cols=2&uuid=051c7&id=4C32-3&alea=d0ri&es=0211001" target="_blank">lien</a>) <br>
  <i class="ico"></i><a href="doc/Evaluations/3eme-EVA-Puissance-sujetABc.pdf" target="_blank">Contrôle d'entrainement sur les puissances</a> 
  (<a href=https://coopmaths.fr/alea/?uuid=f5dcf&id=4C30&n=10&d=10&s=1&s2=4&s3=true&alea=IHWm&cd=1&cols=2&uuid=93df9&id=4C30-2&n=6&d=10&s=3&alea=ItGS&cd=1&cols=2&uuid=bae57&id=4C33-1&n=6&d=10&s=1-2-3&s2=1&alea=QQpL&cd=1&cols=2&uuid=5d72b&id=4C32-0&alea=Dixn&cols=2&uuid=a0d16&id=4C32&n=4&d=10&s=1&s2=3&alea=RYnK&cd=1&cols=2&uuid=a0d16&id=4C32&n=4&d=10&s=2&s2=3&alea=fpKg&cd=1&cols=2&uuid=2d79c&id=4C34&alea=LT0F&uuid=051c7&id=4C32-3&alea=2tBY" target="_blank">lien</a>) <br>
  <i class="ico"></i><a href="doc/Cours/Conversions.pdf" target="_blank">Fiche sur les conversions et les préfixes</a><br>
</p>
</div>
<div class="space notdone"><div style="width: 100%;" class="space done"></div></div>
</div>
</div>

<div class="box ch1 cache" markdown="1">
<p markdown="1">
<i class="tes"></i>Évaluation – Contrôle de cours <a href="doc/Evaluations/3eme-EVA-Puissance-sujetAB.pdf" target="_blank"> sur les puissances</a><br>
{ data-search-exclude }
</p>
</div>

<div class="box ch2">             
<div class="icon">
<div class="image">2</div>
<div class="info">
<h3 class="title">Pythagore</h3>
<p markdown="1">
  <i class="ico"></i><a href="doc/Exercices/2_RE_Pythagore.pdf" target="_blank">Exercices sur Pythagore</a> (<a href="https://coopmaths.fr/alea/?uuid=bd660&id=4G20&alea=QhRf&cols=3&uuid=ab5d4&id=4G21&alea=lqTr&uuid=41187&id=4G20-4&alea=FOR1&uuid=b18e8&id=4G22&n=2&d=10&s=3&alea=aGhI&cd=1">lien</a>)<br>
  <i class="tes"></i><a href="doc/Evaluations/3eme-EVA-Pythagore.pdf" target="_blank">Contrôle d'entrainement sur Pythagore</a>
</p>
</div>
<div class="space notdone"><div style="width: 100%;" class="space done"></div></div>
</div>
</div>

<div class="box ch2 cache" markdown="1">
<p markdown="1">
<i class="tes"></i><a href="doc/Evaluations/3eme-EVA-Pythagore-sujetAB.pdf" target="_blank">Contrôle sur Pythagore</a>
{ data-search-exclude }
</p>
</div>

<div class="box ch3">             
<div class="icon">
<div class="image">3</div>
<div class="info">
<h3 class="title">Agrandissement</h3>
<p markdown="1">
  <i class="ico"></i><a href="doc/Exercices/3_EX_Agrandissement.pdf" target="_blank">Exercices sur les agrandissements</a> (<a href="https://coopmaths.fr/alea/?uuid=a0ad1&id=3G22-1&n=8&d=10&s=1-2-3-4-5-6&alea=QpY5&cd=1">lien</a>)<br>
</p>
</div>
<div class="space notdone"><div style="width: 100%;" class="space done"></div></div>
</div>
</div>

<div class="box ch3 cache" markdown="1">
<p markdown="1">
<i class="tes"></i><a href="doc/Evaluations/3eme-EVA-Agrandissement.pdf" target="_blank">Contrôle sur agrandissement</a>
{ data-search-exclude }
</p>
</div>

<div class="box ch4">             
<div class="icon">
<div class="image">4</div>
<div class="info">
<h3 class="title">Statistiques</h3>
<p markdown="1">
  <i class="ico"></i><a href="doc/Exercices/4_EX_Statistiques.pdf" target="_blank">Exercices sur les statistiques</a> (<a href="https://coopmaths.fr/alea/?uuid=b8afd&id=3S14&n=6&d=10&s=7&s2=4&s3=false&alea=fDpQ&cd=1">lien</a>)<br>
</p>
</div>
<div class="space notdone"><div style="width: 100%;" class="space done"></div></div>
</div>
</div>

<div class="box ch4 cache" markdown="1">
<p markdown="1">
<i class="tes"></i><a href="doc/Evaluations/3eme-EVA-Statistiques.pdf" target="_blank">Contrôle sur les statistiques</a><br>
<i class="tes"></i><a href="doc/Evaluations/3eme-EVA-suj-commun1.pdf" target="_blank">Contrôle commun 1</a><br>
{ data-search-exclude }
</p>
</div>

<div class="box ch5">             
<div class="icon">
<div class="image">5</div>
<div class="info">
<h3 class="title">Calcul littéral</h3>
<p markdown="1">
  <i class="ico"></i><a href="doc/Exercices/5_EX_Calcul_litteral.pdf" target="_blank">Exercices sur le calcul littéral</a> (<a href="https://coopmaths.fr/alea/?uuid=f6853&id=3L11-2&n=10&d=10&s=false&s2=true&s3=9&alea=bQHl&cd=1&cols=2&uuid=815eb&id=3L10-1&alea=TlKi&uuid=db2e0&id=3L11&n=6&d=10&s=3&s2=1&s3=7&s4=true&alea=Inw1&cd=1&uuid=82313&id=3L11-3&alea=htLJ&uuid=501f9&id=4L12&alea=EGDz&uuid=c88ba&id=3L10-2&alea=Ur4n">lien</a>)<br>
  <i class="ico"></i><a href="doc/Exercices/5_EX_Calcul_litteral2.pdf" target="_blank">Exercices sur le calcul littéral 2</a> (<a href="https://coopmaths.fr/alea/?uuid=5f5a6&id=3L11-4&n=8&d=10&s=3&alea=WjC2&cd=1&cols=2&uuid=501f9&id=4L12&alea=XE77&uuid=0a01e&id=2N40-1&alea=JEKX&uuid=51360&id=3L11-6&alea=2DZc&uuid=dnb_2024_07_antilles_4&alea=SaBe&uuid=dnb_2021_06_metropole_4&alea=8H2J&uuid=dnb_2021_09_metropole_3&alea=9hVj&v=eleve&es=0201001">lien</a>)<br>
  <i class="ico"></i><a href="doc/Exercices/5_EX_Calcul_litteral2.pdf" target="_blank">Exercices sur les identités remarquables</a> (<a href="https://coopmaths.fr/alea/?uuid=4197c&id=3L11-1&n=2&d=10&s=2&s2=true&s3=false&alea=VxEB&i=1&cd=1&uuid=51360&id=3L11-6&n=2&d=10&s=1&s2=3&alea=Fj64&i=1&cd=1&uuid=04b0a&id=2N41-6&n=5&d=10&s=2&s2=4&alea=m2mE&i=1&cd=1&uuid=0bd00&id=2N41-7a&n=5&d=10&s=2&alea=LySY&i=1&cd=1&v=eleve&es=0201001">lien</a>)<br>
  <i class="ico"></i><a href="doc/Exercices/6_EX_DNB_Programmes_de_calculs.pdf" target="_blank">Exercices sur les programmes de calculs</a> (<a href="https://coopmaths.fr/alea/?uuid=dnb_2024_07_metropole_2&uuid=dnb_2021_06_etrangers_3&uuid=dnb_2021_06_metropole_4&uuid=dnb_2023_06_etrangers_3&uuid=dnb_2023_06_polynesie_4&uuid=dnb_2019_09_metropole_5&v=eleve&es=0201001">lien</a>)<br>
</p>
</div>
<div class="space notdone"><div style="width: 100%;" class="space done"></div></div>
</div>
</div>

<div class="box ch5 cache" markdown="1">
<p markdown="1">
<i class="tes"></i><a href="doc/Evaluations/3eme-EVA-CalculLiteral.pdf" target="_blank">Contrôle sur le calcul littéral</a>
<i class="tes"></i><a href="doc/Evaluations/3eme-EVA-CalculLiteral2.pdf" target="_blank">Contrôle sur le calcul littéral2</a>
{ data-search-exclude }
</p>
</div>

<div class="box ch6">             
<div class="icon">
<div class="image">6</div>
<div class="info">
<h3 class="title">Thalès</h3>
<p markdown="1">
  <i class="ico"></i><a href="doc/Exercices/7_EX_Thalès.pdf" target="_blank">Exercices sur Thalès</a> (<a href="https://coopmaths.fr/alea/?uuid=74eac&id=3G20&alea=noOI&uuid=4beb8&id=3G20DNB0&alea=7uIy&uuid=eea67&id=3G20-1&alea=bimx&uuid=3451c&id=3G21&alea=3lOs&v=eleve&es=0211001">lien</a>)<br>
</p>
</div>
<div class="space notdone"><div style="width: 100%;" class="space done"></div></div>
</div>
</div>

<div class="box ch6 cache" markdown="1">
<p markdown="1">
<i class="tes"></i><a href="doc/Evaluations/3eme-EVA-Thalès.pdf" target="_blank">Contrôle sur Thalès</a>
<i class="tes"></i><a href="doc/Evaluations/3eme-EVA-Equations.pdf" target="_blank">Contrôle sur les équations</a>

{ data-search-exclude }
</p>
</div>


<div class="box ch7">             
<div class="icon">
<div class="image">7</div>
<div class="info">
<h3 class="title">Equations</h3>
<p markdown="1">
  <i class="ico"></i><a href="" target="_blank">Exercices sur les équations</a> (<a href="">lien</a>)<br>
</p>
</div>
<div class="space notdone"><div style="width: 100%;" class="space done"></div></div>
</div>
</div>

<div class="box ch7 cache" markdown="1">
<p markdown="1">

<i class="tes"></i><a href="doc/Evaluations/3eme-EVA-Equations.pdf" target="_blank">Contrôle sur les équations</a>

{ data-search-exclude }
</p>
</div>

<div class="box ch8">             
<div class="icon">
<div class="image">8</div>
<div class="info">
<h3 class="title">Trigonométrie</h3>
<p markdown="1">
  <i class="ico"></i><a href="" target="_blank">Exercices de trigonométrie</a> (<a href="">lien</a>)<br>
</p>
</div>
<div class="space notdone"><div style="width: 100%;" class="space done"></div></div>
</div>
</div>

<div class="box ch8 cache" markdown="1">
<p markdown="1">
<i class="tes"></i><a href="doc/Evaluations/3eme-EVA-Trigo.pdf" target="_blank">Contrôle sur la trigonométrie</a>

{ data-search-exclude }
</p>
</div>

<div class="box ch9">             
<div class="icon">
<div class="image">9</div>
<div class="info">
<h3 class="title">Fonctions</h3>
<p markdown="1">
  <i class="ico"></i><a href="" target="_blank">Exercices sur les fonctions</a> (<a href="">lien</a>)<br>
</p>
</div>
<div class="space notdone"><div style="width: 100%;" class="space done"></div></div>
</div>
</div>

<div class="box ch9 cache" markdown="1">
<p markdown="1">
<i class="tes"></i><a href="doc/Evaluations/3eme-EVA-Trigo.pdf" target="_blank">Contrôle sur la trigonométrie</a>

{ data-search-exclude }
</p>
</div>