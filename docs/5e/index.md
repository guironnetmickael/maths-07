---
author: Mickael Guironnet
title: 5e
---


<div class="box ch1">             
<div class="icon">
<div class="image">1</div>
<div class="info">
<h3 class="title">Les nombres relatifs</h3>
<p markdown="1">
  <i class="ico"></i><a href="https://coopmaths.fr/alea/?uuid=41030&id=can6N15&n=1&d=10&s=6&s2=0&s3=2&alea=Ot7t&i=1&uuid=22f41&id=can6N05&n=2&d=10&alea=0cF4&i=1&uuid=73d76&id=can6N08&n=1&d=10&alea=Hb14&i=1&uuid=79452&id=can6N11&n=1&d=10&s=1-2&s2=2&alea=Q76F&i=1&uuid=ce3da&id=can6N13&n=1&d=10&s=5-6&s2=1&alea=ZUvw&i=1&uuid=ca515&id=can6N04&n=1&d=10&alea=A8TC&i=1&v=eleve&z=1.4&es=3111001" target="_blank">Questions flash<br>
  <i class="ico"></i><a href="doc/Activites/5eme-C9-Nombres_relatifs_v2-presentation.pdf" target="_blank">Activité d'introduction sur les nombres relatifs</a><br>
  <i class="ico"></i><a href="doc/Cours/5eme-C1-Nombres_relatifs-vide.pdf" target="_blank">Cours sur les nombres relatifs</a><br>
  <i class="ico"></i><a href="https://coopmaths.fr/alea/?uuid=76343&id=5R10-1&alea=foHD&i=1&v=eleve&es=0210001" target="_blank">Deviner un nombre relatif</a><br>
  <i class="ico"></i><a href="https://coopmaths.fr/alea/?uuid=6d576&id=5R11-2&n=2&d=10&s=4&alea=9fIa&i=1&cd=1&v=eleve&es=0211001" target="_blank">Placer un point sur une droite graduée</a><br>
   <i class="ico"></i><a href="https://coopmaths.fr/alea/?uuid=4dadb&id=5R12-1&alea=14Ga&i=1&v=eleve&z=1.7&es=0211001" target="_blank">Placer un point sur un plan</a><br>
  <i class="ico"></i><a href="https://coopmaths.fr/alea/?uuid=19060&id=5R13&n=10&d=10&s=2&alea=btch&i=1&cd=1&v=eleve&z=1.2&es=0211001" target="_blank">Comparer des nombres relatifs</a><br>
  <i class="ico"></i><a href="https://coopmaths.fr/alea/?uuid=6ea89&id=6N10-2&n=6&d=10&s=5&alea=Ylkh&v=diaporama&z=1.7&ds=1000010" target="_blank">Numération des nombres décimaux (rappel)</a><br>
   <i class="ico"></i><a href="doc/Exercices/5_PDT_Nombres_relatifs_v3.pdf" target="_blank">PDT sur les nombres relatifs</a><br>
   <i class="ico"></i><a href="doc/Exercices/5_EX_1_nombres_relatifs.pdf" target="_blank">Exercices sur les nombres relatifs</a>   (<a href="https://coopmaths.fr/alea/?uuid=cab80&id=5R10-0&n=1&d=10&s=true&alea=hYXW&cd=1&uuid=6d576&id=5R11-2&n=2&d=10&s=4&alea=IhtV&cd=1&uuid=cd7ce&id=5R11&n=2&d=10&s=4&alea=masT&cd=1&uuid=19060&id=5R13&n=10&d=10&s=4&alea=yONa&cd=1&cols=2&uuid=76343&id=5R10-1&alea=hubK&uuid=ab968&id=5R12-2&alea=kTln&uuid=4dadb&id=5R12-1&alea=q94l" target="_blank">lien</a>)<br>
  <i class="ico"></i><a href="doc/Exercices/5eme-C9-Nombres_relatifs-tache-complexe.pdf" target="_blank">Tâche complexe</a> sur les coordonnées GPS<br>
</p>
</div>
<div class="space notdone"><div style="width: 100%;" class="space done"></div></div>
</div>
</div>

<div class="box ch1 cache" markdown="1">
<p markdown="1">
<i class="tes"></i>Évaluation – Contrôle de cours<a href="doc/Evaluations/1-EVAL_Nombres_relatifsv2.pdf" target="_blank"> sur les nombres relatifs</a><br>
{ data-search-exclude }
</p>
</div>


<div class="box ch2">             
<div class="icon">
<div class="image">2</div>
<div class="info">
<h3 class="title">La proportionnalité</h3>
<p markdown="1">
  <i class="ico"></i><a href="doc/Cours/5eme-C2-proportionnalité-vide.pdf" target="_blank">Cours sur la proportionnalité</a><br>
  <i class="ico"></i><a href="doc/Exercices/5_PDT_2_PDT_Proportionnalité.pdf" target="_blank">PDT sur la proportionnalité</a><br>
  <i class="ico"></i><a href="doc/Exercices/5_EX_2_proportionnalité.pdf" target="_blank">Exercices sur la proportionnalité</a>  (<a href="https://coopmaths.fr/alea/?uuid=2d5eb&id=6P15&n=4&d=10&s=1&s2=true&alea=8HKd&cd=1&cols=2&uuid=aa997&id=5P10&alea=vE5X&cols=2&uuid=65288&id=6P11-2&n=5&d=10&s=4&s2=false&s3=false&alea=CXmq&cd=1&uuid=b0f4e&id=6P12&alea=pEWC&v=latex" target="_blank">lien</a>)<br>
  <i class="ico"></i><a href="doc/Exercices/5_EX_3_pourcentages-echelles.pdf" target="_blank">Exercices sur les pourcentages et les échelles</a>  (<a href="https://coopmaths.fr/alea/?uuid=5199b&id=5P14-2&n=4&d=10&s=1&s2=1-2-4-5&alea=Dm61&cd=1&uuid=542be&id=5P14-1&alea=DJv3&uuid=4db23&id=5P14&alea=UxUf&uuid=edb61&id=5P13&n=2&d=10&s=1&s2=false&alea=W8rF&cd=1&uuid=edb61&id=5P13&n=2&d=10&s=2-3&s2=false&alea=RO10&cd=1&uuid=a29bd&id=5P11-1&n=3&d=10&s=4&s2=1&alea=kvxi&cd=1" target="_blank">lien</a>)<br>
  
  
</p>
</div>
<div class="space notdone"><div style="width: 100%;" class="space done"></div></div>
</div>
</div>

<div class="box ch2 cache" markdown="1">
<p markdown="1">
<i class="tes"></i>Évaluation – Contrôle de cours <a href="doc/Evaluations/2-EVAL_Proportionnalité.pdf" target="_blank">sur la proportionnalité</a><br>
<i class="tes"></i>Évaluation – Contrôle de cours <a href="doc/Evaluations/3-EVAL_Pourcentages.pdf" target="_blank">sur les pourcentages</a><br>
{ data-search-exclude }
</p>
</div>

<div class="box ch3">             
<div class="icon">
<div class="image">3</div>
<div class="info">
<h3 class="title">La symétrie</h3>
<p markdown="1">
  <i class="ico"></i><a href="doc/Cours/5eme-C3-Les-symétries-v3-vide.pdf" target="_blank">Cours sur la symétrie centrale</a><br>
  <i class="ico"></i><a href="https://maths07.zd.fr/activite/2020-2021/5eme/Activites/5eme-C14-Symetrie-centrale.html" target="_blank">Exercices en ligne sur la symétrie centrale</a><br>
  <i class="ico"></i><a href="doc/Exercices/5_EX_4_Symétrie.pdf" target="_blank">Exercices sur la symétrie</a> (<a href="https://coopmaths.fr/alea/?uuid=ce9ef&id=5G10-1&n=1&d=10&s=4&s2=1&s3=4&alea=ghM5&cd=1&uuid=8ea24&id=5G10-2&n=1&d=10&s=2&s2=1&s3=1&alea=gPNa&cd=1&uuid=8d4bf&id=5G11-1&alea=dLsu&uuid=49786&id=5G11-2&alea=GcEO&uuid=08f60&id=5G11-4&alea=wsLY&uuid=2d2bb&id=5G11-5&n=1&d=10&s=1&alea=FZmH&cd=1&uuid=d37ea&id=5G11-20&n=6&d=10&s=1&s2=6&alea=1bO5&cd=1" target="_blank">lien</a>)<br>
</p>
</div>
<div class="space notdone"><div style="width: 100%;" class="space done"></div></div>
</div>
</div>

<div class="box ch3 cache" markdown="1">
<p markdown="1">
<i class="tes"></i>Évaluation – Contrôle de cours <a href="doc/Evaluations/5eme-C14-Symétrie-Centrale-controle-v3.pdf" target="_blank">sur la symétrie v1</a><br>
<i class="tes"></i>Évaluation – Contrôle de cours <a href="doc/Evaluations/5eme-C14-Symétrie-Centrale-controle-v2.pdf" target="_blank">sur la symétrie v2</a><br>
{ data-search-exclude }
</p>
</div>

<div class="box ch4">             
<div class="icon">
<div class="image">4</div>
<div class="info">
<h3 class="title">Calculs avec priorité</h3>
<p markdown="1">
  <i class="ico"></i><a href="doc/Exercices/5_EX_5_CO_Développer_factoriser.pdf" target="_blank">Exercices sur développer/factoriser</a><br>
</p>
</div>
<div class="space notdone"><div style="width: 100%;" class="space done"></div></div>
</div>
</div>

<div class="box ch4 cache" markdown="1">
<p markdown="1">
<i class="tes"></i>Évaluation – Contrôle de cours <a href="doc/Evaluations/5-EVAL-Calculs-avec-priorité.pdf" target="_blank">sur les calculs avec priorité</a><br>
{ data-search-exclude }
</p>
</div>

<div class="box ch5">             
<div class="icon">
<div class="image">5</div>
<div class="info">
<h3 class="title">Angles et parallélisme</h3>
<p markdown="1">
  <i class="ico"></i><a href="doc/Cours/5eme-C6-Angles-et-parallélisme-v6-vide.pdf" target="_blank">Cours sur les angles et le parallélisme</a><br>
  <i class="ico"></i><a href="doc/Exercices/5_EX_6_Angles_parallélisme.pdf" target="_blank">Exercices sur les angles et le parallélisme</a><br>
  
</p>
</div>
<div class="space notdone"><div style="width: 100%;" class="space done"></div></div>
</div>
</div>

<div class="box ch5 cache" markdown="1">
<p markdown="1">
<i class="tes"></i>Évaluation – Contrôle de cours <a href="doc/Evaluations/5-EVAL_Angles.pdf" target="_blank">sur les angles v1</a><br>
<i class="tes"></i>Évaluation – Contrôle de cours <a href="doc/Evaluations/5eme-parallelisme-controle-V2.pdf" target="_blank">sur les angles v2</a><br>
<i class="tes"></i>Évaluation – Contrôle de cours <a href="doc/Evaluations/5eme-parallelisme-controle-v2bis.pdf" target="_blank">sur les angles v3</a><br>
{ data-search-exclude }
</p>
</div>

<div class="box ch6">             
<div class="icon">
<div class="image">6</div>
<div class="info">
<h3 class="title">Addition et soustraction de nombres relatifs</h3>
<p markdown="1">
  <i class="ico"></i><a href="doc/Cours/5eme-C13-Nombres_relatifs_opérations-v4-vide.pdf" target="_blank">Cours sur les additions et soustractions de nombes relatifs</a><br>
</p>
</div>
<div class="space notdone"><div style="width: 100%;" class="space done"></div></div>
</div>
</div>

<div class="box ch6 cache" markdown="1">
<p markdown="1">
<i class="tes"></i>Évaluation – Contrôle de cours <a href="doc/Evaluations/5-EVAL_Angles.pdf" target="_blank">sur les angles v1</a><br>
{ data-search-exclude }
</p>
</div>

<div class="box ch7">             
<div class="icon">
<div class="image">7</div>
<div class="info">
<h3 class="title">Triangles et angles</h3>
<p markdown="1">
  <i class="ico"></i><a href="doc/Cours/" target="_blank">Cours sur le triangle et les angles</a><br>
</p>
</div>
<div class="space notdone"><div style="width: 100%;" class="space done"></div></div>
</div>
</div>

<div class="box ch7 cache" markdown="1">
<p markdown="1">
<i class="tes"></i>Évaluation – Contrôle de cours <a href="doc/Evaluations/5-EVAL-triangles-angles.pdf" target="_blank">sur les angles v1</a><br>
{ data-search-exclude }
</p>
</div>